﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="LogInfo.aspx.cs" Inherits="WebApp.Log.LogInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form runat="server" id="ContentForm">

    <asp:Table runat="server" ID="InputTable" CssClass="nolineTable">
        <asp:TableRow>
            <asp:TableCell> <%= WebApp.DisplayString.LogId %></asp:TableCell>
            <asp:TableCell> <asp:TextBox runat="server" ID="LogTypeIdInput" /></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell> <asp:Button runat="server" ID="SubmitButton" OnClick="SubmitButton_Click" /> </asp:TableCell>
        </asp:TableRow>  
    </asp:Table>

    <asp:Panel runat="server" ID="ListPanel" Visible="true">
    <br />
    <table class="logTable">
        <tr>
            <th> <%= WebApp.DisplayString.LogId %></th>
            <th> <%= WebApp.DisplayString.LogName %></th>
            <th> <%= WebApp.DisplayString.LogDesc %></th>
            <th> </th>
        </tr>
        <asp:Repeater runat="server" ID="LogList">
        <ItemTemplate>
        <tr>
            <td> <%# DataBinder.Eval(Container.DataItem, "Id")%> </td>
            <td> <%# DataBinder.Eval(Container.DataItem, "Name")%> </td>
            <td> <%# DataBinder.Eval(Container.DataItem, "Desc")%> </td>
            <td> <a href="<%# DataBinder.Eval(Container.DataItem, "DetailURL")%>"> <font color="blue"> <%= WebApp.DisplayString.Log_DetailInfo %> </font> </a> </td>
        </tr>
        </ItemTemplate>
        </asp:Repeater>

        <asp:Repeater runat="server" ID="LogExList">
        <ItemTemplate>
        <tr>
            <td> <%# DataBinder.Eval(Container.DataItem, "Id")%> </td>
            <td> <%# DataBinder.Eval(Container.DataItem, "Name")%> </td>
            <td> <%# DataBinder.Eval(Container.DataItem, "Desc")%> </td>
            <td> <a href="<%# DataBinder.Eval(Container.DataItem, "DetailURL")%>"> <font color="blue"> <%= WebApp.DisplayString.Log_DetailInfo %> </font> </a> </td>
        </tr>
        </ItemTemplate>
        </asp:Repeater>
    </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="ResultPanel" Visible="false">
    <br />
    <table class="lineTable">
        <tr>
            <th colspan="1"> <%= WebApp.DisplayString.LogId %></td>
            <td> <asp:Literal runat="server" ID="LogTypeId" /> </td>
        </tr>
        <tr>
            <th colspan="1"> <%= WebApp.DisplayString.LogName %></td>
            <td> <asp:Literal runat="server" ID="LogName" /> </td>
        </tr>
        <tr>
            <th colspan="1"> <%= WebApp.DisplayString.LogDesc %></td>
            <td> <asp:Literal runat="server" ID="LogDesc" /> </td>
        </tr>
        <asp:Repeater runat="server" ID="LogFields">
        <ItemTemplate>            
            <tr>                
                <th> <%# DataBinder.Eval(Container.DataItem, "Name")%> </td>
                <td> <%# DataBinder.Eval(Container.DataItem, "Desc") %> </td>
            </tr>
        </ItemTemplate>
        </asp:Repeater>
    </table>
    </asp:Panel>

    </form>

</asp:Content>
