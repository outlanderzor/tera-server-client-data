﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_ChangeDungeonEnterCount.aspx.cs" Inherits="WebApp.Users.ATO_ChangeDungeonEnterCount" %>

<asp:Content ID="Content2" ContentPlaceHolderID="TERA_ATO" runat="server">

    <asp:Table runat="server" CssClass="lineTable">
        <asp:TableRow>
            <asp:TableHeaderCell><%# WebApp.DisplayString.ServerName %></asp:TableHeaderCell>
            <asp:TableHeaderCell><%# WebApp.DisplayString.UserDBID %></asp:TableHeaderCell>
            <asp:TableHeaderCell><%# WebApp.DisplayString.UserName %></asp:TableHeaderCell>
            <asp:TableHeaderCell><%# WebApp.DisplayString.AccountDBID %></asp:TableHeaderCell>
            <asp:TableHeaderCell><%# WebApp.DisplayString.AccountName %></asp:TableHeaderCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><asp:Label runat="server" ID="ServerName" /></asp:TableCell>
            <asp:TableCell><asp:Label runat="server" ID="UserDbId" /></asp:TableCell>
            <asp:TableCell><asp:Label runat="server" ID="UserName" /></asp:TableCell>
            <asp:TableCell><asp:Label runat="server" ID="AccountDbId" /></asp:TableCell>
            <asp:TableCell><asp:Label runat="server" ID="AccountName" /></asp:TableCell>
        </asp:TableRow>
    </asp:Table>

    <asp:Label runat="server" Text="<%# WebApp.DisplayString.ConfirmDungeonEnterCount %>" />

    <br />

    <asp:Label runat="server" Text="<%# WebApp.DisplayString.DungeonName %>" />
    <asp:Label runat="server" ID="DungeonName" />

    <br />

    <asp:Label runat="server" ID="DungeonEnterCount" />
    <asp:TextBox runat="server" ID="NewDungeonEnterCount" Text="0" />
    <asp:RegularExpressionValidator runat="server" ID="NewDungeonEnterCountValidator"
        ControlToValidate="NewDungeonEnterCount" ErrorMessage="숫자만 입력해주세요."
        ValidationExpression="\d+" />

    <br /> <br />

</asp:Content>
