﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="ClearClientSetting.aspx.cs" Inherits="WebApp.Users.ClearClientSetting" %>
<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form id="form" runat="server">

        <table class="outerlineTable">
            <tr>
                <td> <%= WebApp.DisplayString.ServerChoice %> </td>
                <td> <asp:DropDownList runat="server" ID="ServNoDropDownList" DataTextField="Text" DataValueField="No" /> </td>
            </tr>
            <tr>
                <td> <%= WebApp.DisplayString.UserDBID %> </td>
                <td> <asp:TextBox runat="server" ID="UserDbIdTextBox" /> </td>
            </tr>
            <tr>
                <td> <asp:Button runat="server" ID="SubmitButton" OnClick="OnSubmit" /> </td>
            </tr>
        </table>

    </form>

</asp:Content>
