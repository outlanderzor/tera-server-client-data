﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_SendNormalParcel.aspx.cs" Inherits="WebApp.Users.ATO_SendNormalParcel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
    <!-- 우편 받는사람 정보 -->
    <table style="margin-bottom: 10px; border: none;">
        <tr style="margin: 0px;">
            <td><%# WebApp.DisplayString.ServerName %></td>
            <td><asp:Label runat="server" ID="ServerName" Width="50"/></td>
        </tr>
        <tr style="margin: 0px;">
            <td><%# WebApp.DisplayString.UserName %></td>
            <td><asp:Label runat="server" ID="UserName" Width="150"/></td>
        </tr>
    </table>

    <br />
    
    <!-- 우편정보 입력-->
    <p style="font-weight:bold"><%# WebApp.DisplayString.SendParcelInfo %> </p>
    <table style="margin-bottom: 10px; border: none;">
        <tr>
            <td><%# WebApp.DisplayString.Sender %></td>
            <td><asp:TextBox runat="server" Rows="1" ID="Sender" Width="150" /></td>
        </tr>
        <tr>
            <td><%# WebApp.DisplayString.Title %></td>
            <td><asp:TextBox runat="server" Rows="1" ID="ParcelTitle" Width="150" /></td>
        </tr>
        <tr>
            <td><%# WebApp.DisplayString.Msg %></td>
            <td><asp:TextBox runat="server" TextMode="MultiLine" style="overflow:auto" ID="Msg" Height="300" Width="400"/></td>
        </tr>
    </table>
</asp:Content>
