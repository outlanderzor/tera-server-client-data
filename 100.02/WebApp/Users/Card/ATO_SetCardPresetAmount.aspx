﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_SetCardPresetAmount.aspx.cs" Inherits="WebApp.Users.Card.ATO_SetCardPresetAmount" %>

<asp:Content ID="Content2" ContentPlaceHolderID="TERA_ATO" runat="server">

    <asp:Table runat="server">
        <asp:TableRow>
            <asp:TableCell><%# WebApp.DisplayString.CurrentPresetAmount + ":" %></asp:TableCell>
            <asp:TableCell><asp:Label runat="server" ID="CurrentPresetAmount" /></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><%# WebApp.DisplayString.PresetAmount + ":" %></asp:TableCell>
            <asp:TableCell>
                <asp:TextBox runat="server" ID="PresetAmount" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>

</asp:Content>
