﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_SetCardAmount.aspx.cs" Inherits="WebApp.Users.Card.ATO_SetCardAmount" %>

<asp:Content ID="Content2" ContentPlaceHolderID="TERA_ATO" runat="server">

    <asp:Table runat="server">
        <asp:TableRow>
            <asp:TableCell>
                <%# WebApp.DisplayString.CardName + ":" %> &nbsp;
                <asp:Label runat="server" ID="CardName" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <%# WebApp.DisplayString.CardCurrentCollectionStatus + ":" %> &nbsp;
                <asp:Label runat="server" ID="CardCollectionStatus" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <%# WebApp.DisplayString.CardCollectionStatus + ":" %> &nbsp;
                <asp:TextBox runat="server" ID="Amount" />
                <asp:RegularExpressionValidator runat="server" ControlToValidate="Amount" ErrorMessage='<%# WebApp.DisplayString.ErrorMsg_OnlyNumbers %>' ValidationExpression="\d+" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>

</asp:Content>
