﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="CharacterCreateRestriction.aspx.cs" Inherits="WebApp.Users.CreateRestriction.CharacterCreateRestriction" %>
<asp:Content ID="Content3" ContentPlaceHolderID="headerContentHolder" runat="server">
	<style type="text/css">
		div#controlButtons input[type=submit] {
			float: right;
		}
		a.underline {
			text-decoration: underline;
		}
	</style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="mainContentHolder" runat="server">
    <form id="Form1" runat="server">
		<table class="lineTable">
			<tr>
				<th><%# WebApp.DisplayString.ServerChoice %></th>
				<th><%# WebApp.DisplayString.Search %></th>
			</tr>
			<tr>
				<td><asp:DropDownList runat="server" ID="ServerSelect" DataSource="<%# WebApp.WebAdminDB.ServerListWithAll.Select(x => new { ServerName = x.mName, ServerNo = x.mNo }) %>" DataTextField="ServerName" DataValueField="ServerNo"/></td>
				<td><asp:Button runat="server" ID="SelectOption" Text="Search" OnClick="SelectOption_Click" /></td>
			</tr>
		</table>
		<div id="controlButtons">
            <asp:Button runat="server" ID="DeleteRestriction" CommandName="DeleteRestriction" OnCommand="DeleteRestriction_Command" Text="<%# WebApp.DisplayString.DeleteSetting %>"/>
            <asp:Button runat="server" ID="AddRestriction" CommandName="AddRestriction" OnCommand="AddRestriction_Command" Text="<%# WebApp.DisplayString.InsertItem %>"/>			
		</div>
		<table class="lineTable width-full">
			<asp:Repeater runat="server" ID="RestrictionList" >
				<HeaderTemplate>
					<tr>
						<th><asp:CheckBox runat="server" ID="ServerCheckAll" OnCheckedChanged="ServerCheckAll_CheckedChanged" AutoPostBack="true" /></th>
						<th><%# WebApp.DisplayString.ServerName %></th>
						<th><%# WebApp.DisplayString.Class %></th>
						<th><%# WebApp.DisplayString.Race %></th>
						<th><%# WebApp.DisplayString.StartTime %></th>
					</tr>
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
						<td><asp:CheckBox runat="server" ID="ServerCheck" /><asp:HiddenField runat="server" ID="InfoId" Value='<%# Eval("ServerNo") + " " + Eval("InfoId") %>'/></td>
						<td><%# Eval("ServerName") %></td>
						<td><%# Eval("Class") %></td>
						<td><%# Eval("Race") %></td>
						<td><%# Eval("StartTime") %></td>
					</tr>
				</ItemTemplate>
			</asp:Repeater>
		</table>
	</form>
</asp:Content>