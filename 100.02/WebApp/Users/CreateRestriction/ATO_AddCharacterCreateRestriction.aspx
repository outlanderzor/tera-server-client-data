﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_AddCharacterCreateRestriction.aspx.cs" Inherits="WebApp.Users.CreateRestriction.ATO_AddCharacterCreateRestriction" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
	<div class="ato-serverlist">
		<label><%# WebApp.DisplayString.CheckServer %></label>
		<ul>
			<asp:Repeater runat="server" ID="ServerCheckList">
				<ItemTemplate>
					<li><asp:CheckBox runat="server" ID="ServerName" Text='<%# Eval("ServerName") %>' /><asp:HiddenField runat="server" ID="ServerNo" Value='<%# Eval("ServerNo") %>' /></li>
				</ItemTemplate>
			</asp:Repeater>
		</ul>
		<label><%# WebApp.DisplayString.CheckAllServer %></label>
	</div>
	<div class="ato-main">
		<ul class="form-style">
			<li><label class="width100"><%# WebApp.DisplayString.RestrictionStartTime %></label><asp:TextBox runat="server" ID="StartTime" type="date" /></li>
			<li><label class="width100"><%# WebApp.DisplayString.ClassSetting %></label><asp:DropDownList runat="server" ID="ClassType"/></li>
			<li><label class="width100"><%# WebApp.DisplayString.RaceSetting %></label><asp:DropDownList runat="server" ID="Race"/></li>
            <li><label class="width100"><%# WebApp.DisplayString.GenderSetting %></label><asp:DropDownList runat="server" ID="Gender"/></li>
		</ul>
	</div>
	<script type="text/javascript">
	    $(function () {
	        $('[type=date]').each(function () {
	            $(this).appendDtpicker({ "dateOnly": false, "minuteInterval": 5 });
	        })
	        $('[type=date]').keypress(function () { return false; })
	        $('[type=date]').keydown(function () { return false; })
	    });
	</script>
</asp:Content>
