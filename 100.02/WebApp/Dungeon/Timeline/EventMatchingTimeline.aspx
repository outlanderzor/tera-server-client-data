﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="EventMatchingTimeline.aspx.cs" Inherits="WebApp.Dungeon.Timeline.EventMatchingTimeline" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">
    <form runat="server" id="form">
        <div style="margin-bottom: .5em;">
            <label for="ServerNoDDL"><%# WebApp.DisplayString.ServerChoice %></label>
            <asp:DropDownList ID="ServerNoDDL" runat="server" DataTextField="Text" DataValueField="Value" />
            <asp:Button runat="server" ID="SearchButton" OnClick="SearchButton_Click" />
        </div>
        <table class="lineTable">
            <asp:Repeater runat="server" ID="TimelineList" OnItemCommand="TimelineList_OnItemCommand">
                <HeaderTemplate>
                    <tr>
                        <th><%# WebApp.DisplayString.ServerName %></th>
                        <th><%# WebApp.DisplayString.EventMatchingTaskName %></th>
                        <th><%# WebApp.DisplayString.Timeline_IsOpen %></th>
                        <th><%# WebApp.DisplayString.DayOfWeek %></th>
                        <th><%# WebApp.DisplayString.Timeline_Start %></th>
                        <th><%# WebApp.DisplayString.Timeline_End %></th>
                        <th><%# WebApp.DisplayString.DoEditOrDelete %></th>
                    </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><%# Eval("ServerName") %></td>
                        <td><%# Eval("TargetName") %></td>
                        <td><%# Eval("IsOpen") %></td>
                        <td><%# Eval("DayOfWeek") %></td>
                        <td><%# Eval("StartTime") %></td>
                        <td><%# Eval("EndTime") %></td>
                        <td><asp:Button runat="server" ID="EditTimelineButton" Text="<%# WebApp.DisplayString.DoEditOrDelete %>"
                            CommandName="ShowEditTimeline" CommandArgument='<%# Eval("Index") %>'/></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
		<div id="controlButtons">
			<asp:Button runat="server" ID="AddTimeline" CommandName="AddTimeline" OnCommand="AddTimeline_Command" Text="<%# WebApp.DisplayString.Add_Timeline %>"/>	
        </div>
    </form>
</asp:Content>
