﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.Master" AutoEventWireup="true" CodeBehind="ATO_EditTimeline.aspx.cs" Inherits="WebApp.Dungeon.Timeline.ATO_EditTimeline" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
    <asp:HiddenField runat="server" ID="ServerNo" />
    <asp:HiddenField runat="server" ID="TargetId" />
    <asp:HiddenField runat="server" ID="TaskType" />
	<ul class="form-style">
        <li><label class="width130"><%= WebApp.DisplayString.ServerName %></label><asp:TextBox runat="server" ID="ServerName" ReadOnly="True" /></li>
        <li><label class="width130"><%= TimelineType == WebApp.Dungeon.Timeline.TimelineType.DUNGEON ? WebApp.DisplayString.DungeonName : WebApp.DisplayString.EventMatchingTaskName %></label><asp:TextBox runat="server" ID="Target" ReadOnly="True" /></li>
    </ul>
    <p style="font-weight: bold; margin: 1em 0;"><%= WebApp.DisplayString.Edit_Timeline %></p>
    <ul class="form-style" style="border: solid 1px black; padding: .5em;">
	    <li><label class="width130"><%= WebApp.DisplayString.Timeline_IsOpen %></label><asp:DropDownList runat="server" ID="IsAdd" DataTextField="Text" DataValueField="Value" /></li>
        <li><label class="width130"><%= WebApp.DisplayString.DayOfWeek %></label><asp:DropDownList runat="server" ID="DayOfWeek" DataTextField="Text" DataValueField="Value" /></li>
        <li><label class="width130"><%= WebApp.DisplayString.Timeline_Start %></label><asp:TextBox runat="server" ID="StartHour" Width="2em"/>:<asp:TextBox runat="server" ID="StartMinute" Width="2em"/></li>
        <li><label class="width130"><%= WebApp.DisplayString.Timeline_End %></label><asp:TextBox runat="server" ID="EndHour" Width="2em"/>:<asp:TextBox runat="server" ID="EndMinute" Width="2em"/></li>
        <li><asp:Button runat="server" ID="AddEntryButton" OnClick="AddEntryButton_Click" Text="<%# WebApp.DisplayString.Add %>" /></li>
    </ul>
    <p style="font-weight: bold;"><%= WebApp.DisplayString.DataSheet_Timeline %></p>
    <table style="margin: 1em 0;">
        <asp:Repeater runat="server" ID="DatasheetTimelineList">
            <HeaderTemplate>
                <tr>
                    <th><%= WebApp.DisplayString.DayOfWeek %></th>
                    <th><%= WebApp.DisplayString.Timeline_Start %></th>
                    <th><%= WebApp.DisplayString.Timeline_End %></th>
                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><%# Eval("DayOfWeek") %></td>
                    <td><%# Eval("StartTime") %></td>
                    <td><%# Eval("EndTime") %></td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
    <p style="font-weight: bold;"><%= WebApp.DisplayString.WebAdmin_Timeline %></p>
    <table style="margin: 1em 0;">
        <asp:Repeater runat="server" ID="TimelineList" OnItemCommand="TimelineList_OnItemCommand">
            <HeaderTemplate>
                <tr>
                    <th><%= WebApp.DisplayString.Timeline_IsOpen %></th>
                    <th><%= WebApp.DisplayString.DayOfWeek %></th>
                    <th><%= WebApp.DisplayString.Timeline_Start %></th>
                    <th><%= WebApp.DisplayString.Timeline_End %></th>
                    <th><%= WebApp.DisplayString.DoDelete %></th>
                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><%# Eval("IsAdd") %></td>
                    <td><%# Eval("DayOfWeek") %></td>
                    <td><%# Eval("StartTime") %></td>
                    <td><%# Eval("EndTime") %></td>
                    <td><asp:Button runat="server" ID="DeleteIntervalButton" Text="<%# WebApp.DisplayString.DoDelete %>"
                            CommandName="DeleteInterval" CommandArgument='<%# Eval("Index") %>'/></td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
    <p style="margin-top:0; margin-bottom:1em;">
        <asp:Button runat="server" ID="DeleteAllButton" OnClick="DeleteAllButton_Click" Text="<%# WebApp.DisplayString.DeleteAllData %>" />
        <asp:Button runat="server" ID="ResetButton" OnClick="ResetButton_Click" Text="<%# WebApp.DisplayString.Reset %>" />
    </p>
</asp:Content>