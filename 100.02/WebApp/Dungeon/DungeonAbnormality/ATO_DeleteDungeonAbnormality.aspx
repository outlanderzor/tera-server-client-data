﻿<%@ Import Namespace="WebApp" %>
<%@ Import Namespace="WebApp.CommonEnum" %>
<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_DeleteDungeonAbnormality.aspx.cs" Inherits="WebApp.Dungeon.ATO_DeleteDungeonAbnormality" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TERA_ATO" runat="server">
	<table class="lineTable" style="width: 100%;">
		<asp:Repeater runat="server" ID="ContinentAbnormalityList">
			<HeaderTemplate>
				<tr>
					<th><%# DisplayString.ServerName %></th>
					<th><%# DisplayString.DungeonName %></th>
					<th><%# DisplayString.Abnormality %></th>
					<th><%# DisplayString.StartTime %></th>
					<th><%# DisplayString.EndTime %></th>
				</tr>
			</HeaderTemplate>
			<ItemTemplate>
				<tr>
					<td><%# Eval("ServerName") %></td>
					<td><%# (int)Eval("ContinentId") == ContinentAbnormalityEnum.DungeonAllId ? "" : "[" + Eval("ContinentId") + "] "  %> <%# Eval("ContinentName") %></td>
					<td>[<%# Eval("AbnormalityId") %>] <%# Eval("AbnormalityName") %></td>
					<td><%# Eval("StartTimeStr") %></td>
					<td><%# Eval("EndTimeStr") %></td>
				</tr>
			</ItemTemplate>
		</asp:Repeater>
	</table>
</asp:Content>
