﻿<%@ Import Namespace="WebApp" %>
<%@ Import Namespace="WebApp.CommonEnum" %>
<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/ServerSelectAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_InsertDungeonAbnormality.aspx.cs" Inherits="WebApp.Dungeon.ATO_InsertDungeonAbnormality" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TERA_ATO" runat="server">
	<div class="ato-main">
		<ul class="form-style">
			<li>
				<label class="width100"><%# DisplayString.EventPeriod %></label>
				<asp:TextBox runat="server" ID="StartTime" class="date width120" />
				<asp:TextBox runat="server" ID="EndTime" class="date width120" />
			</li>
			<li>
				<label class="width100"><%# DisplayString.AbnormalityID %></label>
				<asp:TextBox runat="server" ID="AbnormalityId" class="abnormalityId width120" />
				<asp:Button runat="server" ID="ShowAbnormalityName" OnClick="ShowAbnormalityName_Click" Text="<%# DisplayString.OK %>"/>
				<asp:Button runat="server" ID="AddAbnormality" OnClick="AddAbnormality_Click" Text="<%# DisplayString.Add %>" class="multiple"/>
				<asp:Label runat="server" ID="AbnormalityNameLabel"></asp:Label>
				<asp:HiddenField runat="server" ID="AbnormalityListStr" />
			</li>
			<li id="warning-li">
				<label class="width100"></label>
				<label style="color: red; font-weight: bold;"><%# DisplayString.ErrorMsg_WarningAbnormalityNot950600 %></label>
			</li>
			<li>
				<label class="width100"><%# DisplayString.DungeonName %></label>
				<asp:DropDownList runat="server" ID="ContinentId" />
				<asp:Button runat="server" ID="AddContinent" OnClick="AddContinent_Click" Text="<%# DisplayString.Add %>" class="multiple"/>
				<asp:HiddenField runat="server" ID="ContinentListStr" />
			</li>
		</ul>
		<div class="multiple" id="div-list">
			<asp:Panel runat="server" ID="PanelAbnormalityList" style="display: inline-block; vertical-align: top;">
				<table class="lineTable">
					<asp:Repeater runat="server" ID="InsertedAbnormalityList">
						<HeaderTemplate>
							<tr>
								<th><%# DisplayString.AbnormalityName %></th>
								<th><%# DisplayString.DeleteFromList %></th>
							</tr>
						</HeaderTemplate>
						<ItemTemplate>
							<tr>
								<td><%# "[" + Eval("AbnormalityId") + "] " + Eval("AbnormalityName") %></td>
								<td><asp:Button runat="server" ID="DeleteAbnormality" Text="<%# DisplayString.DeleteFromList %>" CommandArgument='<%# Eval("AbnormalityId") %>' OnCommand="DeleteAbnormality_Command"/></td>
							</tr>
						</ItemTemplate>
					</asp:Repeater>
				</table>
			</asp:Panel>
			<asp:Panel runat="server" ID="PanelContinentList" style="display: inline-block; vertical-align: top;">
				<table class="lineTable">
					<asp:Repeater runat="server" ID="InsertedContinentList">
						<HeaderTemplate>
							<tr>
								<th><%# DisplayString.DungeonName %></th>
								<th><%# DisplayString.DeleteFromList %></th>
							</tr>
						</HeaderTemplate>
						<ItemTemplate>
							<tr>
								<td><%# (int)Eval("ContinentId") == ContinentAbnormalityEnum.DungeonAllId ? "" : "[" + Eval("ContinentId") + "] "  %> <%# Eval("ContinentName") %></td>
								<td><asp:Button runat="server" ID="DeleteContinent" Text="<%# DisplayString.DeleteFromList %>" CommandArgument='<%# Eval("ContinentId") %>' OnCommand="DeleteContinent_Command"/></td>
							</tr>
						</ItemTemplate>
					</asp:Repeater>
				</table>
			</asp:Panel>
		</div>
	</div>
	<script type="text/javascript">
		$(function () {
			$('li#warning-li').hide();
			var abnormalityId = $('input.abnormalityId').val();
			if (abnormalityId != '' && abnormalityId != '0' && abnormalityId != '950600') {
				$('li#warning-li').show();
			}
		});
	</script>

</asp:Content>
