﻿<%@ Import Namespace="WebApp" %>
<%@ Import Namespace="WebApp.CommonEnum" %>
<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="DungeonAbnormality.aspx.cs" Inherits="WebApp.Dungeon.DungeonAbnormality" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headerContentHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">
	<form id="PageForm" runat="server">
		<table class="lineTable">
			<tr>
				<th><%# DisplayString.ServerChoice %></th>
				<th style="display: none;"><%# DisplayString.DungeonName %></th>
				<th><%# DisplayString.Search %></th>
			</tr>
			<tr>
				<td><asp:DropDownList runat="server" ID="ServerSelect" /></td>
				<td style="display: none;"><asp:DropDownList runat="server" ID="DungeonSelect" /></td>
				<td><asp:Button runat="server" ID="SelectOption" OnClick="SelectOption_Click" Text="<%# DisplayString.Search %>" /></td>
			</tr>
		</table>

		<div id="controlButtons" style="text-align: right; margin-bottom: 10px;">
			<asp:Button runat="server" ID="InsertData" OnClick="InsertData_Click" Text="<%# DisplayString.AddAbnormality %>" />
			<asp:Button runat="server" ID="DeleteData" OnClick="DeleteData_Click" Text="<%# DisplayString.DeleteAbnormality %>"/>
			<asp:Button runat="server" ID="DeleteAllData" OnClick="DeleteAllData_Click" Text="<%# DisplayString.DeleteAllData %>"/>
		</div>
		<table class="lineTable" style="width: 100%;">
			<asp:Repeater runat="server" ID="ContinentAbnormalityList">
				<HeaderTemplate>
					<tr>
						<th>
							<asp:CheckBox runat="server" ID="CheckAll" AutoPostBack="true" OnCheckedChanged="CheckAll_CheckedChanged" />
						</th>
						<th><%# DisplayString.ServerName %></th>
						<th><%# DisplayString.DungeonName %></th>
						<th><%# DisplayString.Abnormality %></th>
						<th><%# DisplayString.StartTime %></th>
						<th><%# DisplayString.EndTime %></th>
					</tr>
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
						<td>
							<asp:CheckBox runat="server" ID="Check" />
							<asp:HiddenField runat="server" ID="ItemValue" Value='<%# Eval("ServerNo") + " " + Eval("DataId") %>'/>
						</td>
						<td><%# Eval("ServerName") %></td>
						<td><%# (int)Eval("ContinentId") == ContinentAbnormalityEnum.DungeonAllId ? "" : "[" + Eval("ContinentId") + "] "  %> <%# Eval("ContinentName") %></td>
						<td>[<%# Eval("AbnormalityId") %>] <%# Eval("AbnormalityName") %></td>
						<td><%# Eval("StartTimeStr") %></td>
						<td><%# Eval("EndTimeStr") %></td>
					</tr>
				</ItemTemplate>
			</asp:Repeater>
		</table>
	</form>
</asp:Content>
