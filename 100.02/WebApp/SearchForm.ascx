﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchForm.ascx.cs" Inherits="WebApp.SearchForm" %>

<asp:Panel runat="server" ID="searchPannel" DefaultButton="submitButton">
<table  >
    <tr>
        <td colspan="3">
            <asp:Label ID="title"  runat="server" Font-Bold="true"/>
        </td>    
    </tr>
    <tr>        
        <td colspan="2">        
        <%= WebApp.DisplayString.ServerChoice %>
        </td>
        <td>
            <asp:DropDownList ID="serverSelect" runat="server" DataTextField = "Name" DataValueField = "Number">
            </asp:DropDownList>
        </td>
    </tr>
    <asp:Repeater runat="server" ID="searchRepeater" >
    <ItemTemplate>
     <tr>
        <td>b
        <asp:RadioButton runat="server" Visible="<%# UseRadio %>" Checked="<%# ExistParam(Container.DataItem) %>" />
        </td>
        <td>        
        <%# DataBinder.GetPropertyValue(Container.DataItem,"Text") %>
        </td>        
        <td>
            <asp:HiddenField runat="server" Value='<%# DataBinder.GetPropertyValue(Container.DataItem,"Param") %>' />
            <asp:TextBox runat="server" text="<%# GetParam(Container.DataItem) %>" ToolTip="<%# WebApp.DisplayString.CommaComment %>"/>            
        </td>        
    </tr>
    </ItemTemplate>
    </asp:Repeater>
    <tr>
        <td colspan="3">            
            <asp:Button ID="submitButton" runat="server" oncommand="submitButton_Command"/>
        </td>
    </tr>
</table>
</asp:Panel>