﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="RestrictBulkDetail.aspx.cs" Inherits="WebApp.Restrict.RestrictBulkDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">
    <form runat="server" id="form">
    <table class="outerlineTable">
        <tr>
            <td style="padding-top: 5px;">
                <strong>[ <%= WebApp.DisplayString.RestrictBulkFile %> ]</strong>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel runat="server" ID="DetailPanel">
                </asp:Panel>
            </td>
        </tr>
    </table>
    &nbsp;
    <table class="outerlineTable">
        <tr>
            <td style="padding-top: 5px;">
                <strong>[ <%= WebApp.DisplayString.RestrictBulkDetail %> ]</strong>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel runat="server" ID="ResultPanel">
                </asp:Panel>
            </td>
        </tr>
    </table>
    </form>
</asp:Content>
