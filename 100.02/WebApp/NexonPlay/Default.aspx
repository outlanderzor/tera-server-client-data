﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApp.NexonPlay.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headerContentHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">
    <form id="Form1" runat="server">
        <div id="controlButtons">
            <asp:Button runat="server" ID="NotiOnButton" Text="<%# WebApp.DisplayString.TradeBrokerNoti_ON %>" OnClick="NotiOnButton_OnClick"/>
            <asp:Button runat="server" ID="NotiOffButton" Text="<%# WebApp.DisplayString.TradeBrokerNoti_OFF %>" OnClick="NotiOffButton_OnClick" />
        </div>
        <br />

	    <table class="lineTable ">
		    <asp:Repeater runat="server" ID="ServerOnOffList">
			    <HeaderTemplate>
				    <tr>
					    <th><%# WebApp.DisplayString.ServerName %></th>
                        <th><%# WebApp.DisplayString.Status %></th>
				    </tr>
			    </HeaderTemplate>
			    <ItemTemplate>
				    <tr>
					    <td><%# Eval("ServerName") %></td>
					    <td><%# (bool)Eval("IsConverterOn") ? "On" : "Off" %></td>
				    </tr>
			    </ItemTemplate>
		    </asp:Repeater>
	    </table>
    </form>
</asp:Content>