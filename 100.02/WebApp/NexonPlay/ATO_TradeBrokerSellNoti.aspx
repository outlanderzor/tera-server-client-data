﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_TradeBrokerSellNoti.aspx.cs" Inherits="WebApp.NexonPlay.ATO_TradeBrokerSellNoti" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
	<table class="lineTable" style="margin-bottom:20px;">
		<tr>
			<th><%# WebApp.DisplayString.ServerName %></th>
			<th><%# WebApp.DisplayString.ChangeState %></th>
		</tr>
		<tr>
			<td><asp:Label runat="server" ID="ServerName"/></td>
			<td><asp:Label runat="server" ID="OnOff"/></td>
		</tr>
	</table>
</asp:Content>
