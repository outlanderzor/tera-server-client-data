﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="IngameAnnounce.aspx.cs" Inherits="WebApp.Announce.IngameAnnounce" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form runat="server" id="form">
        <table>
        <tr>
            <td> <strong><%= WebApp.DisplayString.IngameAnnounceTitle%></strong></td>
            <td>
                <table>
                <tr>
                    <td> <%= WebApp.DisplayString.ServerChoice %> </td>
                    <td> <asp:DropDownList runat="server" ID="ServNoDDLForView" DataTextField="Text" DataValueField="Value" /> </td>
                    <td> <asp:Button runat="server" ID="SearchButton" OnClick="SearchButton_OnClick" /> </td>
                </tr>
                </table>
            </td>
        </tr>
        </table>
        <br />
        <br />
        <table>
            <tr> 
                <td> <strong><%= WebApp.DisplayString.InstantAnnounce %> </strong></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Panel runat="server" ID="InstantAnnounceResult">
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td> <asp:Button runat="server" ID="DelAllInstantAnnounceButton" OnClick="DelAllInstantAnnounceButton_OnClick" /> </td>
                <td> <asp:Panel runat="server" ID="DelInstantButtonPannel"> </asp:Panel> </td>
                <td> <asp:Panel runat="server" ID="EditInstantButtonPannel"> </asp:Panel> </td>
                <td> <asp:Button runat="server" ID="AddInstantAnnounceButton" OnClick="AddInstantAnnounceButton_OnClick" /> </td>
            </tr>
        </table>
        <table>
            <tr>
                <td> <asp:Panel runat="server" ID="instantPageNo"> </asp:Panel> </td>
                <td> <asp:Panel runat="server" ID="preInstantButton"> </asp:Panel> </td>
                <td> <asp:Panel runat="server" ID="nextInstantButton"> </asp:Panel> </td>
            </tr>
        </table>
        <br />
        <br />
        <table>
            <tr> 
                <td> <strong><%= WebApp.DisplayString.ReserveAnnounce %> </strong></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Panel runat="server" ID="ReserveAnnounceResult">
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td> <asp:Button runat="server" ID="DelAllReserveAnnounceButton" OnClick="DelAllReserveAnnounceButton_OnClick" /> </td>
                <td> <asp:Panel runat="server" ID="DelReserveButtonPannel"> </asp:Panel> </td>
                <td> <asp:Panel runat="server" ID="EditReserveButtonPannel"> </asp:Panel> </td>
                <td> <asp:Button runat="server" ID="AddReserveAnnounceButton" OnClick="AddReserveAnnounceButton_OnClick" /> </td>
                
            </tr>
        </table>
        <table>
            <tr>
                <td> <asp:Panel runat="server" ID="reservePageNo"> </asp:Panel> </td>
                <td> <asp:Panel runat="server" ID="preReserveButton"> </asp:Panel> </td>
                <td> <asp:Panel runat="server" ID="nextReserveButton"> </asp:Panel> </td>
            </tr>
        </table>
    </form>

</asp:Content>