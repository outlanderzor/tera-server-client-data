﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApp.Politics.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form runat="server" id="form">

        <table class="outerlineTable">
        <tr>
            <td><table>
                <tr>
                    <td> <%= WebApp.DisplayString.ServerChoice %> </td>
                    <td> <asp:DropDownList runat="server" ID="ServNoDropDownList" DataTextField="Text" DataValueField="Value" /> </td>
                </tr>
                <tr>
                    <td> <asp:Button runat="server" ID="EnableButton" OnClick="OnEnable" /> </td>
                    <td>
                        <asp:DropDownList runat="server" ID="YearDropDownList" /> <%= WebApp.DisplayString.Year %>
                        <asp:DropDownList runat="server" ID="MonthDropDownList" /> <%= WebApp.DisplayString.Month %>
                        <asp:DropDownList runat="server" ID="DayDropDownList" /> <%= WebApp.DisplayString.Day %>
                        <asp:DropDownList runat="server" ID="HourDropDownList" /> <%= WebApp.DisplayString.Hour %>
                        <asp:DropDownList runat="server" ID="MinuteDropDownList" /> <%= WebApp.DisplayString.Minute %>
                    </td>
                </tr>
                <tr>
                    <td> <asp:Button runat="server" ID="DisableButton" OnClick="OnDisable" /> </td>
                    <td> <%= WebApp.DisplayString.Immediate %> </td>
                </tr>
            </table></td>
        </tr>
        </table>

    </form>

</asp:Content>
