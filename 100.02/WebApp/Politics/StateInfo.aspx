﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="StateInfo.aspx.cs" Inherits="WebApp.Politics.StateInfo" %>
<%@ Register TagPrefix="webApp" TagName="SearchForm" Src="~/SearchForm.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form runat="server" id="resultForm" >

        <table class="outerlineTable">
            <tr><td>
                <webApp:SearchForm  runat="server" ID="searchForm"/>        
            </td></tr>
        </table>

        <asp:Table runat="server" ID="resultTable" CssClass="nolineTable">
            <asp:TableRow> <asp:TableCell> &nbsp; </asp:TableCell> </asp:TableRow>
            <asp:TableRow>                
                <asp:TableCell> <%= WebApp.DisplayString.SystemState%> </asp:TableCell>
                <asp:TableCell> : <asp:Literal runat="server" ID="onoff" /> </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>                
                <asp:TableCell> <%= WebApp.DisplayString.Generation%> </asp:TableCell>
                <asp:TableCell> : <asp:Literal runat="server" ID="generation" /> </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell> <%= WebApp.DisplayString.CurrentStep %> </asp:TableCell>
                <asp:TableCell> : <asp:Literal runat="server" ID="currentStep" />  </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell> <%= WebApp.DisplayString.EndTime%> </asp:TableCell>
                <asp:TableCell> : <asp:Literal runat="server" ID="endTime" /> </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell> <%= WebApp.DisplayString.NextStep%> </asp:TableCell>
                <asp:TableCell> : <asp:Literal runat="server" ID="nextStep" /> </asp:TableCell>
            </asp:TableRow>
        </asp:Table>

    </form>

</asp:Content>
