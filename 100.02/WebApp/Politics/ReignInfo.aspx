﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="ReignInfo.aspx.cs" Inherits="WebApp.Politics.ReignInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form runat="server" id="resultForm" >

        <table class="nolineTable">
            <tr>
                <td> <%= WebApp.DisplayString.ServerChoice %> </td>
                <td> <asp:DropDownList runat="server" ID="ServNoDropDownList" DataTextField="Text" DataValueField="Value" AutoPostBack="true" /> </td>
            </tr>
            <tr>
                <td> <%= WebApp.DisplayString.GenerationSelect %></td>
                <td> <asp:DropDownList runat="server" ID="GenDropDownList" /> </td>
            </tr>
            <tr>
                <td> <%= WebApp.DisplayString.GuardSelect %> </td>
                <td> <asp:DropDownList runat="server" ID="GuardDropDownList" DataTextField="Text" DataValueField="Value" /> </td>
            </tr>
            <tr>
                <td> <asp:Button runat="server" ID="SubmitButton" OnClick="SubmitButton_Click" /> </td>
            </tr>       
        </table>

        <asp:Panel runat="server" ID="resultPanel">
        
        <table class="nolineTable">            
            <tr> <td> &nbsp; </td> </tr>
            <tr>                
                <td> <b> <asp:Literal runat="server" ID="guardName" /> </b> </td>                                
            </tr>
            <tr> <td> &nbsp; </td> </tr>
            <tr>                
                <td> <b><%= WebApp.DisplayString.LordInfo%></b> </td>                
            </tr>
            <tr>
                <td>
                    <table class="lineTable">
                        <tr>
                            <td> <%= WebApp.DisplayString.Generation %> </td> 
                            <td> <asp:Literal runat="server" ID="generationTxt" /> </td>
                        </tr>
                        <tr>
                            <td>
                                <%= WebApp.DisplayString.LordName %>
                                <asp:HyperLink runat="server" ID="editLordNameLink"> <font color="red">[<%=WebApp.DisplayString.Dismiss%>]</font> </asp:HyperLink>
                            </td> 
                            <td> <asp:Literal runat="server" ID="lordName" /> </td>
                        </tr>
                        <tr>
                            <td>  <%= WebApp.DisplayString.LordGuild %>  </td> 
                            <td> <asp:Literal runat="server" ID="lordGuild" /> </td>
                        </tr>
                        <tr>
                            <td>  <%= WebApp.DisplayString.CompeteType %>  </td> 
                            <td> <asp:Literal runat="server" ID="competeType" /> </td>
                        </tr>
                        <tr>
                            <td>  <%= WebApp.DisplayString.GuardHuntingZone %>  </td> 
                            <td> <asp:Literal runat="server" ID="guardHuntingZone" /> </td>
                        </tr>
                        <tr>
                            <td>  <%= WebApp.DisplayString.GuardTown %>  </td> 
                            <td> <asp:Literal runat="server" ID="guardTown" /> </td>
                        </tr>
                        <tr>
                            <td>  <%= WebApp.DisplayString.LordPeriod %>  </td> 
                            <td> <asp:Literal runat="server" ID="lordPeriod" /> </td>
                        </tr>
                        <tr>
                            <td>  
                                <%= WebApp.DisplayString.GuardNotice %>  
                                <asp:HyperLink runat="server" ID="editGuardNoticeLink"> <font color="red">[<%=WebApp.DisplayString.Modify%>]</font> </asp:HyperLink>
                            </td> 
                            <td> <asp:Literal runat="server" ID="guardNotice" /> </td>
                        </tr>
                        <tr>
                            <td>  
                                <%= WebApp.DisplayString.TotalEarnedTax%>  
                                <asp:HyperLink runat="server" ID="editGuardTaxLink"> <font color="red">[<%=WebApp.DisplayString.Modify%>]</font> </asp:HyperLink>
                            </td> 
                            <td> <asp:Literal runat="server" ID="totalEarnedTax" /> </td>
                        </tr>
                        <tr>
                            <td>  <%= WebApp.DisplayString.TaxForLord%>  </td> 
                            <td> <asp:Literal runat="server" ID="taxForLord" /> </td>
                        </tr>
                        <tr>
                            <td>  <%= WebApp.DisplayString.LordTaxProfitRecord%>  </td> 
                            <td> <asp:Literal runat="server" ID="lordTaxProfitRecord" /> </td>
                        </tr>
                        <tr>
                            <td>  
                                <%= WebApp.DisplayString.PolicyPoint%>  
                                <asp:HyperLink runat="server" ID="editPolicyPointLink"> <font color="red">[<%=WebApp.DisplayString.Modify%>]</font> </asp:HyperLink>
                            </td> 
                            <td> <asp:Literal runat="server" ID="policyPoint" /> </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <table>
            <tr> <td> &nbsp; </td> </tr>
            <tr> 
                <td>
                    <b><%= WebApp.DisplayString.TaxRateList%></b>                     
                </td>                
                <td />
                <td> 
                    <b><%= WebApp.DisplayString.PolicyList%></b>                    
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <table class="lineTable">
                        <asp:Repeater runat="server" ID="taxItems">
                        <ItemTemplate>
                            <tr>
                                <td> 
                                    <%# DataBinder.GetPropertyValue(Container.DataItem, "Item1")%>  
                                    <a href="<%# DataBinder.GetPropertyValue(Container.DataItem, "Item3")%>">
                                        <font color="red">
                                            <%# DataBinder.GetPropertyValue(Container.DataItem, "Item4") %>                                            
                                        </font> 
                                    </a>                                   
                                </td> 
                                <td> <%# DataBinder.GetPropertyValue(Container.DataItem, "Item2")%> % </td>
                            </tr>
                        </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </td>
                <td />
                <td valign="top">
                    <table class="lineTable">
                        <asp:Repeater runat="server" ID="policyItems">
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <%# DataBinder.GetPropertyValue(Container.DataItem, "Item1")%>                                                                                                        
                                    <a href="<%# DataBinder.GetPropertyValue(Container.DataItem, "Item3")%>"> <font color="red">[<%=WebApp.DisplayString.Modify%>]</font> </a>                                   
                                </td> 
                                <td> <%# DataBinder.GetPropertyValue(Container.DataItem, "Item2")%> </td>
                            </tr>
                        </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </td>
            </tr>
        </table>

        </asp:Panel>

    </form>

</asp:Content>
