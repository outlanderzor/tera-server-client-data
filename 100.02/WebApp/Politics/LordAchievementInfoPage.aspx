﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="LordAchievementInfoPage.aspx.cs" Inherits="WebApp.Politics.LordAchievementInfoPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form id="Form1" runat="server" class="nolineTable">
    
        <table class="nolineTable">
            <tr>
                <td> <%= WebApp.DisplayString.ServerChoice %> </td>
                <td> <asp:DropDownList runat="server" ID="ServNoDDL" DataTextField="Text" DataValueField="Value" /> </td>
            </tr>
            <tr>
                <td> <%= WebApp.DisplayString.GuildDBID %> </td>
                <td> <asp:TextBox runat="server" ID="TextGuildDbId" /> </td>
            </tr>
            <tr>
                <td> <%= WebApp.DisplayString.GuildName %> </td>
                <td> <asp:TextBox runat="server" ID="TextGuildName" /> </td>
            </tr>
            <tr>
                <td> <asp:Button runat="server" ID="SearchButton" OnClick="SearchButton_Click" /> </td>
            </tr>
        </table>
        
        <asp:Panel runat="server" ID="DataPanel" Visible="true">
        </asp:Panel>
        
        <asp:Panel runat="server" ID="ListPanel" Visible="true">
        </asp:Panel>
    </form>
</asp:Content>
