﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApp.TroubleShoot.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form id="Form1" runat="server" class="nolineTable">

        <table class="nolineTable">
            <tr>
                <td> <%= WebApp.DisplayString.ServerChoice %> </td>
                <td> <asp:DropDownList runat="server" ID="ServNoDDL" DataTextField="Text" DataValueField="Value" /> </td>
            </tr>
            <tr>
                <td> <%= WebApp.DisplayString.UserDBID %> </td>
                <td> <asp:TextBox runat="server" ID="UserDbIdTextBox" /> </td>
            </tr>
            <tr>
                <td> <asp:Button runat="server" ID="SubmitButton" OnClick="SubmitButton_Click" /> </td>
            </tr>     
        </table>

        &nbsp;

        <asp:Panel runat="server" ID="resultPanel" Visible="false">

        <!-- user info -->
        <table class="nolineTable""><tr><td><b><%= WebApp.DisplayString.UserInfo %></b></td></tr></table>
        <table class="lineTable">
            <tr>
                <th> <%= WebApp.DisplayString.User %> </th>
                <td> <asp:Literal runat="server" ID="UserNameLiteral"/> </td>
            </tr>
            <tr>
                <th> <%= WebApp.DisplayString.UserDBID %> </th>
                <td> <asp:Literal runat="server" ID="UserDbIdLiteral"/> </td>
            </tr>
        </table>

        <!-- rewarded info -->
        &nbsp;
        <table class="nolineTable""><tr><td><b><%= WebApp.DisplayString.AdminRewardedItemsInfo%></b></td></tr></table>
        <table class="lineTable">
            <tr>
                <th style="width:250px"> <%= WebApp.DisplayString.Item %></th>                
                <th> <%= WebApp.DisplayString.AdminRewardedItemAmount%></th>
                <th> <%= WebApp.DisplayString.CurrentRelatedItemAmount%></th>
            </tr>
            <asp:Repeater runat="server" ID="RewardedInfoRepeater">
            <ItemTemplate>
            <tr>
                <td> <%# Eval("ItemName") %> (<%# Eval("ItemTemplateId") %>) </td>
                <td> <%# Eval("RewardedAmount") %> </td>
                <td> <%# Eval("CurrentAmount") %> </td>
            </tr>
            </ItemTemplate>
            </asp:Repeater>
        </table>

        <!-- inventory check -->
        &nbsp;
        <table class="nolineTable""><tr><td><b><%= WebApp.DisplayString.Inven %></b></td></tr></table>
        <table class="lineTable">
            <tr>
                <th style="width:250px"> <%= WebApp.DisplayString.Item %></th>                
                <th> <%= WebApp.DisplayString.ItemAmount %></th>
                <th> </th>
            </tr>
            <asp:Repeater runat="server" ID="InventoryRepeater">
            <ItemTemplate>
            <tr>
                <td> <%# Eval("ItemName") %> (<%# Eval("ItemTemplateId") %>) </td>
                <td> <%# Eval("ItemAmount") %> </td>
                <td> <a href="<%# Eval("DeleteLink") %>"> <font color="red"> <%=WebApp.DisplayString.DeleteItem %> </font> </a> </td>
            </tr>
            </ItemTemplate>
            </asp:Repeater>
        </table>

        <!-- warehouse check -->
        &nbsp;
        <table class="nolineTable""><tr><td><b><%= WebApp.DisplayString.WareHouse %></b></td></tr></table>
        <table class="lineTable">
            <tr>
                <th style="width:250px"> <%= WebApp.DisplayString.Item %></th>                
                <th> <%= WebApp.DisplayString.ItemAmount %></th>
                <th> </th>
            </tr>
            <asp:Repeater runat="server" ID="WarehouseRepeater">
            <ItemTemplate>
            <tr>
                <td> <%# Eval("ItemName") %> (<%# Eval("ItemTemplateId") %>) </td>
                <td> <%# Eval("ItemAmount") %> </td>
                <td> <a href="<%# Eval("DeleteLink") %>"> <font color="red"> <%=WebApp.DisplayString.DeleteItem %> </font> </a> </td>
            </tr>
            </ItemTemplate>
            </asp:Repeater>
        </table>

        <!-- parcel check -->
        &nbsp;
        <table class="nolineTable""><tr><td><b><%= WebApp.DisplayString.Parcel %></b></td></tr></table>
        <table class="lineTable">
            <tr>
                <th> <%= WebApp.DisplayString.Parcel_Type %> </th>
                <th> <%= WebApp.DisplayString.Item %></th>                
                <th> <%= WebApp.DisplayString.ItemAmount %></th>
                <th> <%= WebApp.DisplayString.Item %></th>                
                <th> <%= WebApp.DisplayString.ItemAmount %></th>
                <th> <%= WebApp.DisplayString.Item %></th>                
                <th> <%= WebApp.DisplayString.ItemAmount %></th>
                <th> <%= WebApp.DisplayString.Item %></th>                
                <th> <%= WebApp.DisplayString.ItemAmount %></th>
                <th> <%= WebApp.DisplayString.Item %></th>                
                <th> <%= WebApp.DisplayString.ItemAmount %></th>
                <th> </th>
            </tr>
            <asp:Repeater runat="server" ID="ParcelRepeater">
            <ItemTemplate>
            <tr>
                <td> <%# Eval("ParcelType")%> </td>
                <td> <%# Eval("ItemName0") %> (<%# Eval("ItemTemplateId0") %>) </td>
                <td> <%# Eval("ItemAmount0") %> </td>
                <td> <%# Eval("ItemName1") %> (<%# Eval("ItemTemplateId1") %>) </td>
                <td> <%# Eval("ItemAmount1") %> </td>
                <td> <%# Eval("ItemName2") %> (<%# Eval("ItemTemplateId2") %>) </td>
                <td> <%# Eval("ItemAmount2") %> </td>
                <td> <%# Eval("ItemName3") %> (<%# Eval("ItemTemplateId3") %>) </td>
                <td> <%# Eval("ItemAmount3") %> </td>
                <td> <%# Eval("ItemName4") %> (<%# Eval("ItemTemplateId4") %>) </td>
                <td> <%# Eval("ItemAmount4") %> </td>
                <td> <a href="<%# Eval("DeleteLink") %>"> <font color="red"> <%=WebApp.DisplayString.IT_SEIZURE %> </font> </a> </td>
            </tr>
            </ItemTemplate>
            </asp:Repeater>
        </table>

        <!-- tradebroker check -->
        &nbsp;
        <table class="nolineTable""><tr><td><b><%= WebApp.DisplayString.IT_TRADE_BROKER %></b></td></tr></table>
        <table class="lineTable">
            <tr>
                <th style="width:250px"> <%= WebApp.DisplayString.Item %></th>                
                <th> <%= WebApp.DisplayString.ItemAmount %></th>
                <th> </th>
            </tr>
            <asp:Repeater runat="server" ID="TradeBrokerRepeater">
            <ItemTemplate>
            <tr>
                <td> <%# Eval("ItemName") %> (<%# Eval("ItemTemplateId") %>) </td>
                <td> <%# Eval("ItemAmount") %> </td>
                <td> <a href="<%# Eval("DeleteLink") %>"> <font color="red"> <%=WebApp.DisplayString.DeleteItem %> </font> </a> </td>
            </tr>
            </ItemTemplate>
            </asp:Repeater>
        </table>

        </asp:Panel>

    </form>

</asp:Content>
