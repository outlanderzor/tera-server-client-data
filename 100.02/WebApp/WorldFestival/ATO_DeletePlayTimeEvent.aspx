﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_DeletePlayTimeEvent.aspx.cs" Inherits="WebApp.WorldFestival.ATO_DeletePlayTimeEvent" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
    <table style="margin-bottom: 10px; border: none;">

                <tr>
                    <th><%# WebApp.DisplayString.ServerName %></th>
                    <th><%# WebApp.DisplayString.EventId %></th>
                    <th><%# WebApp.DisplayString.AccountTrait %></th>
                    <th><%# WebApp.DisplayString.EventPeriod %></th>
                    <th><%# WebApp.DisplayString.RewardInfo %></th>
                </tr>

                <tr>
                    <td><asp:Label runat="server" ID="ServerName" /></td>
                    <td><asp:Label runat="server" ID="EventId" /></td>
                    <td><asp:Label runat="server" ID="AccountTrait" /></td>
                    <td><asp:Label runat="server" ID="EventPeriod" /></td>
                    <td><asp:Label runat="server" ID="RewardInfoStr" /></td>
                </tr>

    </table>
</asp:Content>
