﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="EventMatchingOnOff.aspx.cs" Inherits="WebApp.WorldFestival.EventMatchingOnOff" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">
    <form runat="server" id="form">
    <table class="outerlineTable">
        <tr>
            <td style="padding-top: 5px;" colspan="2">
                <strong>[ <%= WebApp.DisplayString.EventMatchingOnOff %> ]</strong>
            </td>
        </tr>
        <tr>
            <td>
                <%= WebApp.DisplayString.ServerChoice %>
            </td>
            <td>
                <asp:DropDownList ID="ServerNoDDL" runat="server" DataTextField="Text" DataValueField="Value" />
            </td>
        </tr>
        <tr>
            <td>
                <%= WebApp.DisplayString.EventChoice %>
            </td>
            <td>
                <asp:DropDownList ID="EventDDL" runat="server" DataTextField="Text" DataValueField="Value" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Button ID="SearchButton" runat="server" OnClick="SearchButton_OnClick" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Panel runat="server" ID="ResultPanel">
                </asp:Panel>
            </td>
        </tr>
    </table>
    </form>
</asp:Content>
