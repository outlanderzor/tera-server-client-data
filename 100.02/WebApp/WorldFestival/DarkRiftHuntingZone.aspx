﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="DarkRiftHuntingZone.aspx.cs" Inherits="WebApp.WorldFestival.DarkRiftHuntingZone" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form id="Form1" runat="server" class="nolineTable">
    
        <table class="nolineTable">
            <tr>
                <td> <%= WebApp.DisplayString.ServerChoice %> </td>
                <td> <asp:DropDownList runat="server" ID="ServNoDDL" DataTextField="Text" DataValueField="Value" /> </td>
            </tr>
            <tr>
                <td> <asp:Button runat="server" ID="SearchButton" OnClick="SearchButton_Click" /> </td>
            </tr>
        </table>
        
        <asp:Panel runat="server" ID="HuntingZoneListPanel" Visible="false">
        </asp:Panel>
        <asp:Panel runat="server" ID="ResultPanel">
        </asp:Panel>
    </form>
</asp:Content>
