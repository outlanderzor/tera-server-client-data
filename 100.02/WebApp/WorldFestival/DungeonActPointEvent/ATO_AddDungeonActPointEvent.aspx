﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_AddDungeonActPointEvent.aspx.cs" Inherits="WebApp.WorldFestival.ATO_AddDungeonActPointEvent" %>

<asp:Content ID="Content3" ContentPlaceHolderID="TERA_ATO" runat="server">

    <div class="ato-serverlist">
		<label><%# WebApp.DisplayString.CheckServer %></label>
		<ul>
			<asp:Repeater runat="server" ID="ServerCheckList">
				<ItemTemplate>
					<li><asp:CheckBox runat="server" ID="ServerName" Text='<%# Eval("ServerName") %>' /><asp:HiddenField runat="server" ID="ServerNo" Value='<%# Eval("ServerNo") %>' /></li>
				</ItemTemplate>
			</asp:Repeater>
		</ul>
	</div>

    <div class="ato-main">

        <asp:Label runat="server" Text="<%# WebApp.DisplayString.EventTime %>" />
        <asp:TextBox runat="server" type="date" ID="StartTime" /> ~ 
        <asp:TextBox runat="server" type="date" ID="EndTime" />

        <br /> <br />

        <asp:Label runat="server" Text="<%# WebApp.DisplayString.DungeonName %>" />
        <asp:DropDownList runat="server" ID="DungeonListDDL" DataTextField="Text" DataValueField="Value" />
        &nbsp;

        <asp:Label runat="server" Text="<%# WebApp.DisplayString.DungeonActPoint %>" />
        <asp:TextBox runat="server" ID="DungeonActPointTextBox" /> &nbsp;
        <asp:Button runat="server" ID="AddDungeonActPointEventButton" Text="<%# WebApp.DisplayString.Add %>" OnClick="AddDungeonActPointEventButton_OnClick" />

        <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator1" ControlToValidate="DungeonActPointTextBox" ErrorMessage="숫자만 입력 가능합니다." ValidationExpression="\d+" />

        <br />

        <table class="lineTable">
            <asp:Repeater runat="server" ID="DungeonActPointEventList">
                <HeaderTemplate>
                    <tr>
                        <th><%# WebApp.DisplayString.DungeonName %></th>
                        <th><%# WebApp.DisplayString.DungeonActPoint %></th>
                        <th><%# WebApp.DisplayString.DoControl %></th>
                    </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><%# WebApp.DungeonDatasheet.GetDungeonData((int)Eval("DungeonContinentId")).name %></td>
                        <td><%# Eval("RequiredActPoint") %></td>
                        <td>
                            <asp:Button runat="server" ID="DeleteEventButton" Text="<%# WebApp.DisplayString.DeleteEvent %>" OnClick="DeleteEventButton_OnClick" />
                            <asp:HiddenField runat="server" ID="DungeonContinentIdField" Value='<%# Eval("DungeonContinentId") %>' />
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </div>
    <script type="text/javascript">
        $(function () {
            $('[type=date]').each(function () {
                $(this).appendDtpicker({ "dateOnly": false, "minuteInterval": 1 });
            })
            $('[type=date]').keypress(function () { return false; })
            $('[type=date]').keydown(function () { return false; })
        });
	</script>
</asp:Content>
