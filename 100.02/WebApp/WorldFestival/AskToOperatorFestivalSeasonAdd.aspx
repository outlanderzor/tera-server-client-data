﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/CustomAskDialog.Master" AutoEventWireup="true" CodeBehind="AskToOperatorFestivalSeasonAdd.aspx.cs" Inherits="WebApp.WorldFestival.AskToOperatorFestivalSeasonAdd" %>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentBody" runat="server">
    <asp:Panel runat="server" ID="RequestPanel" Visible="true"> 
        <p style="font-weight:bold; margin-bottom:10px; "><%# WebApp.DisplayString.AddFestivalWarning %><br /><asp:Label runat="server" ID="ErrorInfo" style="color:red;" /></p>
    
        <form id="Form1" runat="server" >
            <table style="margin-bottom: 10px; border: none;">

                <tr style="margin: 0px;">
                    <td><%# WebApp.DisplayString.SelectFestival %></td>
                    <td><asp:DropDownList runat="server" ID="FestivalDDL" DataTextField="Text" DataValueField="Value" /></td>
                </tr>
            
                <tr style="margin: 0px;">
                    <td><%# WebApp.DisplayString.FestivalPeriod %></td>
                    <td><asp:TextBox runat="server" Rows="1" ID="StartDate" type="date" onkeypress="return false;" onkeydown="return false;" /> ~ <asp:TextBox runat="server" Rows="1" ID="EndDate" type="date"  onkeypress="return false;" onkeydown="return false;" />
                        <script type="text/javascript">
                            $(function() {
                                $('*[type=date]').appendDtpicker({ "dateOnly" : true });
                            });
                        </script>
                    </td>
                </tr>
            </table>

            <asp:TextBox runat="server" TextMode="MultiLine" style="overflow:auto" Height="100px" Width="250px" ID="Memo" placeholder="<%# WebApp.DisplayString.MemoStr %>" /> <br /><br />
        
            <asp:Button runat="server" ID="Submit" Text="<%# WebApp.DisplayString.AddFestival %>" OnClick="Submit_OnClick" /> 
            <asp:Button runat="server" ID="Cancel" Text="<%# WebApp.DisplayString.Cancel %>" OnClick="Cancel_OnClick" />

        </form>
    </asp:Panel>

    <asp:Panel runat="server" ID="ResultPanel" Visible="false">
        <p style="font-weight:bold; margin-bottom:10px; "><asp:Label runat="server" ID="Result" /></p>
        <form id="Form2" runat="server" >
        <asp:GridView CssClass="lineTable" ID="ServerResult" runat="server" AutoGenerateColumns="False" ShowHeader="true" ShowHeaderWhenEmpty="true">
          <Columns>
            <asp:TemplateField>
                <HeaderTemplate><%=WebApp.DisplayString.ServerName%></HeaderTemplate>
                <ItemTemplate><%# Eval("serverName").ToString()%></ItemTemplate>            
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate><%=WebApp.DisplayString.Result%></HeaderTemplate>
                <ItemTemplate><%# Eval("result").ToString()%></ItemTemplate>
            </asp:TemplateField>
          </Columns>
        </asp:GridView>
        <asp:Button runat="server" ID="Close" Text="<%# WebApp.DisplayString.OK %>" OnClick="Cancel_OnClick" />
            </form>
    </asp:Panel>
</asp:Content>
