﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="WorldFestivalScheduling.aspx.cs" Inherits="WebApp.WorldFestival.WorldFestivalScheduling" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form runat="server" id="form" style="height: 602px">

        <table class="outerlineTable">
        <tr>
            <td colspan="5">&nbsp;<br /></td>
        </tr>
         <tr>
            <td width="80"> <%= WebApp.DisplayString.EventChoice%> </td>
            <td width="180" colspan="4"> <asp:DropDownList runat="server" ID="EventNoDDL" DataTextField="Text" DataValueField="Value" /> </td>
        </tr>
        <tr>
            <td colspan="5">&nbsp;<br /></td>
        </tr>
        <tr>        
            <td width="80"> <%= WebApp.DisplayString.ServerChoice%> </td>
            <td width="480" colspan="4"> <asp:CheckBoxList runat="server" ID="ServerNoCBL" DataTextField="Text" DataValueField="Value" RepeatColumns="4" RepeatDirection="Horizontal" /> </td>
        </tr>
        <tr>
            <td colspan="5">&nbsp;<br /></td>
        </tr>   
        <tr>
            <td width="80"> <%= WebApp.DisplayString.EventPeriod%> </td>
            <td width="180"> <asp:TextBox runat="server" ID="StartTimeText" /> </td>
            <td width="20" style="text-align:center;">~</td>
            <td width="180"> <asp:TextBox runat="server" ID="EndTimeText" /> </td>
            <td> <asp:Button runat="server" ID="AddEventButton" OnClick="AddEventButton_OnClick" /> </td>
        </tr>
        <tr>
            <td colspan="5">&nbsp;<br /></td>
        </tr>        
        </table>
        <br />
        <br />
        <br />
        <table>
        <tr>
            <td> <strong><%= WebApp.DisplayString.ScheduledWorldFestivalViewTitle%></strong> </td>
            <td>
                <table>
                <tr>
                    <td> </td>
                    <td> <asp:DropDownList runat="server" ID="EventNoDDLForView" DataTextField="Text" DataValueField="Value" /> </td>
                    <td> <asp:Button runat="server" ID="SearchButton" OnClick="SearchButton_OnClick" /> </td>
                </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Panel runat="server" ID="ResultPanel">
                </asp:Panel>
            </td>
        </tr>
        </table>
    </form>

</asp:Content>
