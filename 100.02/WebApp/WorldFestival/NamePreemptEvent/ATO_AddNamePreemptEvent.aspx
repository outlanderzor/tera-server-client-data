﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_AddNamePreemptEvent.aspx.cs" Inherits="WebApp.WorldFestival.NamePreemptEvent.ATO_AddNamePreemptEvent" %>


<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
    <div class="ato-serverlist">
		<label><%# WebApp.DisplayString.CheckServer %></label>
		<ul>
			<asp:Repeater runat="server" ID="ServerCheckList">
				<ItemTemplate>
					<li><asp:CheckBox runat="server" ID="ServerName" Text='<%# Eval("ServerName") %>' /><asp:HiddenField runat="server" ID="ServerNo" Value='<%# Eval("ServerNo") %>' /></li>
				</ItemTemplate>
			</asp:Repeater>
		</ul>
		<label><%# WebApp.DisplayString.CheckAllServer %></label>
	</div>
    <div class="ato-main">
		<ul class="form-style">
			<li>
            <label class="width150"><%# WebApp.DisplayString.SetNamePreemptEventPeriod %></label>
                <asp:TextBox runat="server" ID="PreemptStartTime" type="date" />~
			    <asp:TextBox runat="server" ID="PreemptEndTime" type="date" />
			</li>
			<li>
            <label class="width150"><%# WebApp.DisplayString.SetNameCreationEventPeriod %></label>
                <asp:TextBox runat="server" ID="CreationStartTime" type="date" />~
			    <asp:TextBox runat="server" ID="CreationEndTime" type="date" />
            </li>            
		</ul>
	</div>
    <script type="text/javascript">
    	    $(function () {
    	        $('[type=date]').each(function () {
    	            $(this).appendDtpicker({ "dateOnly": false, "minuteInterval": 1 });
    	        })
    	        $('[type=date]').keypress(function () { return false; })
    	        $('[type=date]').keydown(function () { return false; })
    	    });
	</script>

</asp:Content>
