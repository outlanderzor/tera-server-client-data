﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="NamePreemptEvent.aspx.cs" Inherits="WebApp.WorldFestival.NamePreemptEvent.NamePreemptEvent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headerContentHolder" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">
    <br />
   	<strong><%# WebApp.DisplayString.SelectServer %></strong>
    <form id="Form1" runat="server">
		<table class="lineTable">
			<tr>
				<th><%# WebApp.DisplayString.ServerChoice %></th>
				<th><%# WebApp.DisplayString.Search %></th>
			</tr>
            <tr>
				<td><asp:DropDownList runat="server" ID="ServerSelect" DataSource="<%# WebApp.WebAdminDB.ServerListWithAll.Select(x => new { ServerName = x.mName, ServerNo = x.mNo }) %>" DataTextField="ServerName" DataValueField="ServerNo"/></td>
				<td><asp:Button runat="server" ID="SelectOption" Text="Search" OnClick="SelectOption_Click" /></td>
			</tr>
        </table>
        <br />
      	<strong><%# WebApp.DisplayString.ReserveEvent %></strong>
        <div id="controlButtons" style="float:right">
            <asp:Button runat="server" ID="AddEvent" CommandName="AddEvent" OnCommand="Add_Event_Command"  Text="<%# WebApp.DisplayString.AddEvent %>"/>
			<asp:Button runat="server" ID="DeleteEvent" CommandName="DeleteEvent" OnCommand="Delete_Event_Command" Text="<%# WebApp.DisplayString.DeleteEvent %>"/>
		</div>
        <table class="lineTable width-full">
     		<asp:Repeater runat="server" ID="NamePreemptEventList">
                 <HeaderTemplate>
                   <tr>
                        <th> <asp:CheckBox runat="server" ID="ServerCheckAll" AutoPostBack="true" OnCheckedChanged="ServerCheckAll_CheckedChanged" /> </th>
                        <th><%# WebApp.DisplayString.ServerName %> </th>
                        <th><%# WebApp.DisplayString.PreemptStartTime %> </th>
                        <th><%# WebApp.DisplayString.PreemptEndTime %> </th>
                        <th><%# WebApp.DisplayString.CreationStartTime %> </th>
                        <th><%# WebApp.DisplayString.CreationEndTime %> </th>
                   </tr>
                 </HeaderTemplate>
                 <ItemTemplate>
					<tr>
						<td><asp:CheckBox runat="server" ID="ServerCheck" /><asp:HiddenField runat="server" ID="ServerID" Value='<%# Eval("ServerNo") + " " + Eval("EventId") %>'/></td>
						<td><%# Eval("ServerName") %></td>
                        <td><%# Eval("PreemptStartTime") %></td>
                        <td><%# Eval("PreemptEndTime") %> </td>
					    <td><%# Eval("CreationtStartTime") %> </td>
                        <td><%# Eval("CreationEndTime") %> </td>
					</tr>
				</ItemTemplate>
            </asp:Repeater>
        </table>
    </form>

</asp:Content>
