﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_DelNamePreemptEvent.aspx.cs" Inherits="WebApp.WorldFestival.NamePreemptEvent.ATO_DelNamePreemptEvent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
      <table class="lineTable width-full">
		<asp:Repeater runat="server" ID="NamePreemptEventList">
			<HeaderTemplate>
				<tr>
				    <th><%# WebApp.DisplayString.ServerName %> </th>
                    <th><%# WebApp.DisplayString.PreemptStartTime %> </th>
                    <th><%# WebApp.DisplayString.PreemptEndTime %> </th>
                    <th><%# WebApp.DisplayString.CreationStartTime %> </th>
                    <th><%# WebApp.DisplayString.CreationEndTime %> </th>
				</tr>
			</HeaderTemplate>
			<ItemTemplate>
				<tr>
						<td><%# Eval("ServerName") %></td>
                        <td><%# Eval("PreemptStartTime") %></td>
                        <td><%# Eval("PreemptEndTime") %> </td>
					    <td><%# Eval("CreationtStartTime") %> </td>
                        <td><%# Eval("CreationEndTime") %> </td>
				</tr>
			</ItemTemplate>
		</asp:Repeater>
	</table>

</asp:Content>
