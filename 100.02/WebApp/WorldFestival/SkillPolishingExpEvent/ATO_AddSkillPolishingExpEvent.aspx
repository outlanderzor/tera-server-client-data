﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_AddSkillPolishingExpEvent.aspx.cs" Inherits="WebApp.WorldFestival.SkillPolishingExpEvent.ATO_AddSkillPolishingExpEvent" %>


<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
    <div class="ato-serverlist">
		<label><%# WebApp.DisplayString.CheckServer %></label>
		<ul>
			<asp:Repeater runat="server" ID="ServerCheckList">
				<ItemTemplate>
					<li><asp:CheckBox runat="server" ID="ServerName" Text='<%# Eval("ServerName") %>' /><asp:HiddenField runat="server" ID="ServerNo" Value='<%# Eval("ServerNo") %>' /></li>
				</ItemTemplate>
			</asp:Repeater>
		</ul>
	</div>
     <div class="ato-main">
		<ul class="form-style">
            <li>
                <label class="width150"><%# WebApp.DisplayString.EventList %></label>
                <asp:DropDownList runat="server" ID="EventTypeDDL" class="eventTypeList" DataValueField="TypeId" DataTextField="TypeName" OnSelectedIndexChanged="DropDownEventType_SelectedIndexChanged"/>
            </li>
			<li>
                <label class="width150"><%# WebApp.DisplayString.EventPeriod %></label>
                <asp:TextBox runat="server" ID="StartTime" type="date" /> ~
			    <asp:TextBox runat="server" ID="EndTime" type="date" />
            </li>
            <li>
                <label class="width150"><%# WebApp.DisplayString.EventValue %></label>
                <asp:TextBox runat="server" ID="EventValue" />
            </li>
            <li>
                <table class="lineTable">
                    <th><%# WebApp.DisplayString.AddSkillPolishingExpEvent %></th>
                </table>
            </li>
		</ul>
	</div>
    <script type="text/javascript">
        $(function () {
            $('[type=date]').each(function () {
                $(this).appendDtpicker({ "dateOnly": false, "minuteInterval": 1 });
            })
            $('[type=date]').keypress(function () { return false; })
            $('[type=date]').keydown(function () { return false; })
        });
	</script>

</asp:Content>
