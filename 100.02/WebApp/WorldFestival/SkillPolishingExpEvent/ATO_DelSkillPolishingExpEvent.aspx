﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_DelSkillPolishingExpEvent.aspx.cs" Inherits="WebApp.WorldFestival.SkillPolishingExpEvent.ATO_DelSkillPolishingExpEvent" %>

<asp:Content ID="Content2" ContentPlaceHolderID="TERA_ATO" runat="server">

    <div class="ato-serverlist">
		<label><%# WebApp.DisplayString.CheckServer %></label>
		<ul>
			<asp:Repeater runat="server" ID="ServerCheckList">
				<ItemTemplate>
					<li><asp:CheckBox runat="server" ID="ServerName" Text='<%# Eval("ServerName") %>' /><asp:HiddenField runat="server" ID="ServerNo" Value='<%# Eval("ServerNo") %>' /></li>
				</ItemTemplate>
			</asp:Repeater>
		</ul>
	</div>

    <div class="ato-main">
        <table class="lineTable">
            <asp:Repeater runat="server" ID="SkillPolishingExpEventList">
                <HeaderTemplate>
                    <tr>
                        <th><%# WebApp.DisplayString.ServerName %></th>
                        <th><%# WebApp.DisplayString.EventId %></th>
                    </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><%# Eval("ServerName") %></td>
                        <td><%# Eval("EventId") %></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </div>

</asp:Content>
