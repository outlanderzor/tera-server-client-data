﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="MasterpieceEvent.aspx.cs" Inherits="WebApp.WorldFestival.MasterpieceEventPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">
    <form runat="server" id="form">
        <table>
        <tr>
            <td>
                <table>
                <tr>
                    <td> <%= WebApp.DisplayString.ServerChoice %> </td>
                    <td> <asp:DropDownList runat="server" ID="ServNoDDLForView" DataTextField="Text" DataValueField="Value" /> </td>
                    <td> <asp:Button runat="server" ID="SearchButton" OnClick="SearchButton_OnClick" /> </td>
                </tr>
                </table>
            </td>
        </tr>
        </table>
        <br />
        <br />
        <table>
            <tr>
                <td> <asp:Button runat="server" ID="DelAllEventButton" OnClick="DelAllEventButton_OnClick" /> </td>
                <td> <asp:Button runat="server" ID="DeleteEventButton" OnClick="DelEventButton_OnClick" /> </td>
                <td> <asp:Button runat="server" ID="AddEventButton" OnClick="AddEventButton_OnClick" /> </td>
            </tr>
        </table>
        <table>
            <tr>
                <td colspan="2">
                    <asp:Panel runat="server" ID="ResultPanel">
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </form>

</asp:Content>