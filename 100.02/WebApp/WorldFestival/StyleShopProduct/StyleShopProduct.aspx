﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="StyleShopProduct.aspx.cs" Inherits="WebApp.WorldFestival.StyleShopProduct.StyleShopProduct" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headerContentHolder" runat="server">
    <style type="text/css">
	</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">
    <br />
   	<strong><%# WebApp.DisplayString.SelectServer %></strong>

    <form id="Form1" runat="server">
		<table class="lineTable" style="margin-bottom:10px;">
			<tr>
				<th><%# WebApp.DisplayString.ServerChoice %></th>
                <th><%# WebApp.DisplayString.MarkId %></th>
                <th><%# WebApp.DisplayString.ItemTemplateId_For_SearchLog %>(<%# WebApp.DisplayString.All %> = 0)</th>
				<th><%# WebApp.DisplayString.Search %></th>
                <th><%# WebApp.DisplayString.ExcelDownload %></th>
			</tr>
			<tr>
				<td><asp:DropDownList runat="server" ID="ServerSelect" AutoPostBack="true" DataSource="<%# WebApp.WebAdminDB.ServerListWithAll.Select(x => new { ServerName = x.mName, ServerNo = x.mNo }) %>" DataTextField="ServerName" DataValueField="ServerNo"/></td>
                <td><asp:DropDownList runat="server" ID="MarkId" DataValueField="Key" DataTextField="Value" /></td>
                <td><asp:TextBox runat="server" ID="ItemTemplateId"/></td>
				<td><asp:Button runat="server" ID="SelectOption" Text="Search" OnClick="SelectOption_Click" /></td>
                <td><asp:Button runat="server" ID="ExcelDownload" Text='<%# WebApp.DisplayString.ExcelDownload %>' OnClick="ExcelDownload_Click" /></td>
			</tr>
		</table>
      	
        <div id="controlButtons">
		    <asp:Button runat="server" ID="AddProduct" CommandName="AddProduct" OnCommand="OnAddProduct" Text="<%# WebApp.DisplayString.ADD_STYLESHOP_PRODUCT %>"/>
            <asp:Button runat="server" ID="AddProductByExcel" CommandName="AddProductByExcel" OnCommand="OnAddProductByExcel" Text="<%# WebApp.DisplayString.ADD_STYLESHOP_PRODUCT_BY_EXCEL %>"/>
            <asp:Button runat="server" ID="DeleteAllStyleShopProduct" CommandName="DeleteAllStyleShopProduct" OnCommand="OnDeleteAllStyleShopProduct" Text="<%# WebApp.DisplayString.DELETE_ALL_STYLESHOP_PRODUCT %>"/>
            <asp:Button runat="server" ID="ResetAllProductMarkEvent" CommandName="ResetAllProductMarkEvent" OnCommand="OnResetAllProductMarkEvent" Text="<%# WebApp.DisplayString.RESET_ALL_PRODUCT_MARK_EVENT %>"/>
	    </div>
        <br />

		<table class="lineTable">
			<asp:Repeater runat="server" ID="ProductList">
				<HeaderTemplate>
					<tr>
                        <th><%# WebApp.DisplayString.ServerName %></th>
                        <th><%# WebApp.DisplayString.Item %></th>                        
                        <th><%# WebApp.DisplayString.SalePeriod %></th>
                        <th><%# WebApp.DisplayString.MarkId %></th>
                        <th><%# WebApp.DisplayString.ApplyMarkPeriod %></th>
                        <th><%# WebApp.DisplayString.DiscountRate %></th>
                        <th><%# WebApp.DisplayString.TCatPrice %></th>               
                        <th><%# WebApp.DisplayString.PriceAfterSale %></th>                
                        <th><%# WebApp.DisplayString.PreviewString %></th>
                        <th><%# WebApp.DisplayString.PreviewStartDate %></th>
                        <th><%# WebApp.DisplayString.SaleType %></th>
                        <th><%# WebApp.DisplayString.Control %></th>
					</tr>
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
                        <td><%# Eval("ServerName") %></td>
                        <td><%# WebApp.DataSheetManager.GetItemNameWithTID((int)Eval("TemplateId")) %></td>
                        <td><%# Eval("SaleStartTime")%> ~<br/><%#Eval("SaleEndTime") %></td>
                        <td><%# Eval("MarkType") %></td>
                        <td><%# Eval("MarkTypeStartTime")%> ~<br/><%#Eval("MarkTypeEndTime") %></td>
                        <td><%# (int)Eval("DiscountPercent") == 0 ? "" : Eval("DiscountPercent") + "%" %></td>
                        <td><%# (int)Eval("Price") == 0 ? "" : Eval("Price") %></td>     
                        <th><%# (int)Eval("DiscountPrice") == 0 ? "" : Eval("DiscountPrice") %></th>                        
                        <td><%# Eval("PreviewDetail") %></td>
                        <td><%# Eval("PreviewStartTime") %></td>
                        <td><%# Eval("SaleType") %></td>
                        <td>
							<asp:button runat="server" ID="DeleteBtn" Text='<%# WebApp.DisplayString.DeleteItem %>' CommandName="DeleteItem" CommandArgument='<%# Eval("ServerNo") + " " + Eval("EventId") + " " + Eval("TemplateId") %>' OnCommand="OnDeleteBtn"/>
							<asp:button runat="server" ID="UpdateBtn" Text='<%# WebApp.DisplayString.Update %>' CommandName="UpdateItem" CommandArgument='<%# Eval("ServerNo") + " " + Eval("EventId") + " " + Eval("TemplateId")%>' OnCommand="OnUpdateBtn"/>
						</td>
					</tr>
				</ItemTemplate>
			</asp:Repeater>
		</table>
    </form>

</asp:Content>
