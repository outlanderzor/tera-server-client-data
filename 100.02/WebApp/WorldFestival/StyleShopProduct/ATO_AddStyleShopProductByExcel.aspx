﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/AppCode/DataSetAskDialog.master" CodeBehind="ATO_AddStyleShopProductByExcel.aspx.cs" Inherits="WebApp.WorldFestival.StyleShopProduct.ATO_AddStyleShopProductByExcel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
    
	<div class="ato-serverlist" style="width:100%;">
		<label><%# WebApp.DisplayString.CheckServer %></label>
        <asp:CheckBox runat="server" Text='<%# WebApp.DisplayString.SelectAll %>' AutoPostback="true" OnCheckedChanged="OnSelectAllServers"/>
		<ul>
			<asp:Repeater runat="server" ID="ServerCheckList">
				<ItemTemplate>
					<li>
                        <asp:CheckBox runat="server" ID="ServerName" Text='<%# Eval("ServerName") %>' />
                        <asp:HiddenField runat="server" ID="ServerNo" Value='<%# Eval("ServerNo") %>' />
					</li>
				</ItemTemplate>
			</asp:Repeater>
		</ul>
		<label><%# WebApp.DisplayString.CheckAllServer %></label>
	</div>
    
    <asp:ScriptManager ID="ScriptManager" runat="server"></asp:ScriptManager>

    <div class="ato-editpanel" style="width:100%;">
        <asp:UpdatePanel ID="FileUpload" runat="server">
            <ContentTemplate>
                <asp:FileUpload runat="server" ID="StyleShopFile"/>
                <asp:Button runat="server" ID="UploadBtn" OnClick="OnUploadStyleShopProduct" Text="<%# WebApp.DisplayString.Upload %>"/>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="UploadBtn" />
            </Triggers>
        </asp:UpdatePanel>
        <a href="StyleShopTemplate.xlsx" target="_blank" style="display: block; margin-bottom: 10px; text-decoration: underline;">Download template file</a>
    </div>

    <div class="ato-main">
		<table class="lineTable" style="width:100%;">
			<asp:Repeater runat="server" ID="StyleShopProductList">
				<HeaderTemplate>
					<tr>
						<th><%# WebApp.DisplayString.Item %></th>
                        <th><%# WebApp.DisplayString.SalePeriod %></th>
                        <th><%# WebApp.DisplayString.MarkId %></th>
                        <th><%# WebApp.DisplayString.ApplyMarkPeriod %></th>
                        <th><%# WebApp.DisplayString.DiscountRate %></th>
                        <th><%# WebApp.DisplayString.TCatPrice %></th>               
                        <th><%# WebApp.DisplayString.PriceAfterSale %></th>     
                        <th><%# WebApp.DisplayString.PreviewString %></th>
                        <th><%# WebApp.DisplayString.PreviewStartDate %></th>
                        <th><%# WebApp.DisplayString.SaleType %></th>
					</tr>
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
						<td><%# WebApp.DataSheetManager.GetItemNameWithTID((int)Eval("TemplateId")) %></td>
                        <td><%# Eval("SaleStartTime")%> ~<br/><%#Eval("SaleEndTime") %></td>
                        <td><%# Eval("MarkType") %></td>
                        <td><%# Eval("MarkTypeStartTime")%> ~<br/><%#Eval("MarkTypeEndTime") %></td>
                        <td><%# Eval("DiscountPercent") %></td>
                        <td><%# Eval("Price")  %></td>     
                        <td><%# Eval("PriceAfterSale") %></td>
                        <td><%# Eval("PreviewDetail") %></td>
                        <td><%# Eval("PreviewStartTime") %></td>
                        <td><%# Eval("SaleType") %></td>
					</tr>
				</ItemTemplate>
			</asp:Repeater>
		</table>
	</div>
</asp:Content>