﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/AppCode/DataSetAskDialog.master" CodeBehind="ATO_DeleteStyleShopProduct.aspx.cs" Inherits="WebApp.WorldFestival.StyleShopProduct.ATO_DeleteStyleShopProduct" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
	<div class="ato-main">
		<ul class="form-style">
            <li>
                <label class="width150"><%# WebApp.DisplayString.Item %></label>
                <asp:Label runat="server" ID="ItemName" Enabled="false"/>                
            </li>      
            <li>
                <br />
                <label class="width150"><%# WebApp.DisplayString.SalePeriod %></label>
                <asp:TextBox runat="server" ID="SaleStartDate" type="date" Enabled="false"/> ~
                <asp:TextBox runat="server" ID="SaleEndDate" type="date" Enabled="false"/>
            </li>
            <li>
                <label class="width150"><%# WebApp.DisplayString.MarkId %></label>
                <asp:DropDownList runat="server" ID="MarkId" DataValueField="Key" DataTextField="Value" Enabled="false"/>
			</li>
            <li>
                <label class="width150"><%# WebApp.DisplayString.ApplyMarkPeriod %></label>
                <asp:TextBox runat="server" ID="MarkStartDate" type="date" Enabled="false"/> ~
                <asp:TextBox runat="server" ID="MarkEndDate" type="date" Enabled="false"/>
            </li>
             <li>
                <label class="width150"><%# WebApp.DisplayString.TCatPrice %></label>
                <asp:TextBox runat="server" ID="Price" Enabled="false"/>
			</li>

            <li>
                <asp:Label runat="server" width="150" ID="DiscountLabel"><%# WebApp.DisplayString.DiscountRate %></asp:Label>
                <asp:TextBox runat="server" ID="DiscountPercent" Enabled="false"/>
            </li>

            <li>
                <asp:Label runat="server" width="150" ID="PriceAfterSaleLabel"><%# WebApp.DisplayString.PriceAfterSale %></asp:Label>
                <asp:TextBox runat="server" ID="PriceAfterSale" Enabled="false"/>
			</li>

            <li>
                <br />
                <asp:label runat="server" width="150"><%# WebApp.DisplayString.PreviewStartDate %></asp:label>
                <asp:TextBox runat="server" ID="PreviewStartDate" type="date" Enabled="false"/>
             </li>
             <li>
                <asp:label runat="server" width="150"><%# WebApp.DisplayString.PreviewString %></asp:label>
                <asp:TextBox runat="server" ID="PreviewDetail" Enabled="false"/>                
			</li>

             <li>
                <asp:label runat="server" width="150"><%# WebApp.DisplayString.SaleType %></asp:label>
                <asp:TextBox runat="server" ID="SaleType" Enabled="false"/>                
			</li>
		</ul>
	</div>
     <script type="text/javascript">
         $(function () {
             $('[type=date]').each(function () {
                 $(this).appendDtpicker({ "dateOnly": false, "minuteInterval": 1 });
             })
             $('[type=date]').keypress(function () { return false; })
             $('[type=date]').keydown(function () { return false; })
         });
	</script>
</asp:Content>