﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="EventMatchingReward.aspx.cs" Inherits="WebApp.WorldFestival.EventMatchingReward"%>
<asp:Content ID="Content3" ContentPlaceHolderID="headerContentHolder" runat="server">
	<style type="text/css">
		body {
		}
		ul {
			list-style-type: none;
			padding: 0;
		}
		ul.submenus {
			margin: 10px 0;
		}
		ul.submenus > li {
			display: inline-block;
			margin-right: 20px;
		}
		ul.conditions > li {
			margin-bottom: 10px;
		}
	</style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="mainContentHolder" runat="server">
	<form id="Form1" runat="server">
		<ul class="conditions">
			<li> <!-- 서버 목록 -->
				<span>서버</span>
				<asp:DropDownList runat="server" ID="ServerList" DataSource="<%# WebApp.WebAdminDB.ServerListWithAll.Select(x => new { ServerName = x.mName, ServerNo = x.mNo }) %>" DataTextField="ServerName" DataValueField="ServerNo"/>
				<asp:Button runat="server" ID="SelectServer" Text="Search" OnClick="SelectServer_Click"/>
			</li>
		</ul>
		<table class="lineTable">
			<asp:Repeater runat="server" ID="BonusSettingsList">
				<HeaderTemplate>
					<tr>
						<th><%# WebApp.DisplayString.ServerName %></th>
						<th><%# WebApp.DisplayString.DailyBonus %></th>
						<th><%# WebApp.DisplayString.Modify %></th>
                        <th><%# WebApp.DisplayString.WeeklyBonus %></th>
						<th><%# WebApp.DisplayString.Modify %></th>
					</tr>
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
						<td><%# Eval("ServerName") %></td>
						<td><%# Eval("GetDailyFlagStr") %></td>
						<td><asp:button runat="server" ID="ToggleDailyBtn"
								Text='<%# (Eval("GetDailyFlagStr") == "On" ? "Off" : "On") %>' 
								CommandArgument='<%# Eval("ServerNo") + " " + (Eval("GetDailyFlagStr") == "On" ? "Off" : "On") %>' 
								OnCommand="ToggleDailySettings_Command"/></td>
                        <td><%# Eval("GetWeeklyFlagStr") %></td>
						<td><asp:button runat="server" ID="ToggleWeeklyBtn"
								Text='<%# (Eval("GetWeeklyFlagStr") == "On" ? "Off" : "On") %>' 
								CommandArgument='<%# Eval("ServerNo") + " " + (Eval("GetWeeklyFlagStr") == "On" ? "Off" : "On") %>' 
								OnCommand="ToggleWeeklySettings_Command"/></td>
					</tr>
				</ItemTemplate>
			</asp:Repeater>
		</table>
	</form>
</asp:Content>
