﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_PlayGuideExtraDailyRewardUploadExcel.aspx.cs" Inherits="WebApp.WorldFestival.EventMatching.ATO_PlayGuideExtraDailyRewardUploadExcel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
    <div class="ato-serverlist" style="width: 100%;">
        <label><%# WebApp.DisplayString.CheckServer %></label>
        <asp:CheckBox runat="server" Text='<%# WebApp.DisplayString.SelectAll %>' ID="CheckAllServer" AutoPostBack="true" OnCheckedChanged="OnSelectAllServers" />
        <ul>
            <asp:Repeater runat="server" ID="ServerCheckList">
                <ItemTemplate>
                    <li>
                        <asp:CheckBox runat="server" ID="ServerName" Text='<%# Eval("ServerName") %>' AutoPostBack="true" OnCheckedChanged="OnSelectServer" />
                        <asp:HiddenField runat="server" ID="ServerNo" Value='<%# Eval("ServerNo") %>' />
                    </li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
        <label><%# WebApp.DisplayString.CheckAllServer %></label>
    </div>

    <div class="ato-editpanel" style="width: 100%;">
        <asp:FileUpload runat="server" ID="StyleShopFile" />
        <asp:Button runat="server" ID="UploadBtn" AutoPostBack="true" OnClick="OnUploadExcelFile" Text="<%# WebApp.DisplayString.Upload %>" />

        <a href="PlayGuideExtraDailyRewardTemplate.xlsx" target="_blank" style="display: block; margin-bottom: 10px; text-decoration: underline;">Download template file</a>
    </div>

    <div class="ato-main">
        <table class="lineTable" style="width: 100%;">
            <asp:Repeater runat="server" ID="ShowExtraDailyRewardList">
                <HeaderTemplate>
                    <tr>
                        <th><%# WebApp.DisplayString.OrderID %></th>
                        <th><%# WebApp.DisplayString.OrderName %></th>
                        <th><%# WebApp.DisplayString.DateStart %></th>
                        <th><%# WebApp.DisplayString.DateEnd %></th>
                        <th><%# WebApp.DisplayString.Item %></th>
                        <th><%# WebApp.DisplayString.Amount %></th>
                        <th><%# WebApp.DisplayString.PlayGuideAddRewardDays %></th>
                        <th><%# WebApp.DisplayString.PlayGuideAddRewardTime %></th>

                    </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><%# Eval("EventMatchingId") %></td>
                        <td><%# Eval("EventMatchingName") %></td>
                        <td><%# Eval("StartTime")%> </td>
                        <td><%# Eval("EndTime") %></td>
                        <td><%# Eval("Item") %></td>
                        <td><%# Eval("ItemAmount") %></td>
                        <td><%# Eval("DaysOfWeek") %></td>
                        <td><%# Eval("PossibleTime") %></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </div>
</asp:Content>