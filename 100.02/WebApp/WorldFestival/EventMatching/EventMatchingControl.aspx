﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="EventMatchingControl.aspx.cs" Inherits="WebApp.WorldFestival.EventMatchingControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">
    <form runat="server" id="form">
    <table class="outerlineTable">
        <tr>
            <td width="80"> <%= WebApp.DisplayString.ServerChoice %> </td>
            <td width="400"> <asp:DropDownList runat="server" ID="ServerNoDDL" DataTextField="Text" DataValueField="Value" /> </td>
            <td width="20">&nbsp;</td>
            <td> <asp:Button runat="server" ID="AddControlButton" OnClick="AddControlButton_OnClick" /> </td>
        </tr>
        <tr>
            <td colspan="4">&nbsp;<br /></td>
        </tr>
        <tr>
            <td width="80"> <%= WebApp.DisplayString.SelectOrder %> </td>
            <td width="400"> <asp:DropDownList runat="server" ID="EventDDL" DataTextField="Text" DataValueField="Value" /> </td>
            <td width="20">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">&nbsp;<br /></td>
        </tr>
        <tr>
            <td width="80"> <%= WebApp.DisplayString.EventMatchingControlType %> </td>
            <td width="400">
                <asp:RadioButtonList runat="server" ID="EventMatchingControlTypeList" RepeatColumns="2" DataTextField="Text" DataValueField="Value"/>
            </td>
            <td width="20">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <table>
    <tr>
        <td> <strong><%= WebApp.DisplayString.EventMatchingControlList %></strong> </td>
        <td>
            <table>
            <tr>
                <td> <%= WebApp.DisplayString.ServerChoice %> </td>
                <td> <asp:DropDownList runat="server" ID="ServerNoDDLForView" DataTextField="Text" DataValueField="Value" /> </td>
                <td> <asp:Button runat="server" ID="SearchButton" OnClick="SearchButton_OnClick" /> </td>
            </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Panel runat="server" ID="ResultPanel">
            </asp:Panel>
        </td>
    </tr>
    </table>
    </form>
</asp:Content>
