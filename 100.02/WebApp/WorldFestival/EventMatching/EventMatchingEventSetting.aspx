﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="EventMatchingEventSetting.aspx.cs" Inherits="WebApp.WorldFestival.EventMatchingEventSetting" %>
<asp:Content ID="Content3" ContentPlaceHolderID="headerContentHolder" runat="server">
	<style type="text/css">
		body {
		}
		ul {
			list-style-type: none;
			padding: 0;
		}
		ul.submenus {
			margin: 10px 0;
		}
		ul.submenus > li {
			display: inline-block;
			margin-right: 20px;
		}
		ul.conditions > li {
			margin-bottom: 10px;
		}
	</style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">
    <form runat="server" id="form">
        <table>
        <tr>
            <td>
                <table>
                <tr>
                    <td> <%= WebApp.DisplayString.ServerChoice %> </td>
                    <td> <asp:DropDownList runat="server" ID="ServNoDDLForView" DataTextField="Text" DataValueField="Value" /> </td>
                    <td> <asp:Button runat="server" Text="<%# WebApp.DisplayString.ServerChoice %>" ID="SearchButton" OnClick="SearchButton_OnClick" /> </td>
                </tr>
                </table>
            </td>
        </tr>
        </table>
        <br />
        <br />
        <table>
            <tr> 
                <td> <strong><%= WebApp.DisplayString.PlayGuideEventList %> </strong></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Panel runat="server" ID="PlayGuideEventResult">
                        <asp:Table runat="server" ID="PlayGuideEventTable" CssClass="lineTable">
                            <asp:TableHeaderRow>
                                <asp:TableHeaderCell> <input type="checkbox" id="__allEvent" name="<%= WebApp.ParamDefine.Common.Ck %>" onclick="ClickCheckboxInput('__ckevent')" /> </asp:TableHeaderCell>
                                <asp:TableHeaderCell> <%# WebApp.DisplayString.ServerName %> </asp:TableHeaderCell>
                                <asp:TableHeaderCell> <%# WebApp.DisplayString.EventType %> </asp:TableHeaderCell>
                                <asp:TableHeaderCell> <%# WebApp.DisplayString.Magnification %></asp:TableHeaderCell>
                                <asp:TableHeaderCell> <%# WebApp.DisplayString.DateStart %></asp:TableHeaderCell>
                                <asp:TableHeaderCell> <%# WebApp.DisplayString.DateEnd %></asp:TableHeaderCell>
                            </asp:TableHeaderRow>
                        </asp:Table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <br />
        <table>
            <tr>
                <td> <asp:Button runat="server" ID="AddEventButton" Text="<%# WebApp.DisplayString.AddEvent %>" OnClick="AddEventButton_OnClick" /> </td>
                <td> <asp:Button runat="server" ID="DelEventButton" Text="<%# WebApp.DisplayString.DeleteEvent %>" onclick="<%# MakeAskDelete(WebApp.TASK_TYPE.PLAYGUIDE_EVENT_DELETE.Code) %>" /> </td>
                <td> <asp:Button runat="server" ID="DelAllEventButton" Text="<%# WebApp.DisplayString.DelAllEvent %>" OnClick="DelAllEventButton_OnClick" /> </td>
            </tr>
        </table>
        <table>
            <tr>
                <td> 
                    <asp:Panel runat="server" ID="EventPageNo"> 
                        <asp:Label runat="server" ID="EventPageNoHeader" Text="<%# WebApp.DisplayString.SearchPage %>" /> 
                        <asp:Label runat="server" ID="EventPageNoName" /> 
                     </asp:Panel> 
                </td>
                <td> <asp:Button runat="server" ID="EventPrevPageButton" Text="<%# WebApp.DisplayString.PrevPage %>" Visible="false" OnClick="EventPrevPageButton_OnClick" /> </td>
                <td> <asp:Button runat="server" ID="EventNextPageButton" Text="<%# WebApp.DisplayString.NextPage %>" Visible="false" OnClick="EventNextPageButton_OnClick" /> </td>
            </tr>
        </table>

        <!-- 기본 보상에 추가되는 보상-->
        <br />
        <br />
        <table>
            <tr> 
                <td> <strong><%= WebApp.DisplayString.PlayGuideExtraBaseReward %> </strong></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Panel runat="server" ID="PlayGuideExtraBaseRewardResult">
                        <asp:Table runat="server" ID="PlayGuideExtraBaseRewardTable" CssClass="lineTable">
                            <asp:TableHeaderRow>
                                <asp:TableHeaderCell> <input type="checkbox" id="__ckbasereward_all" name="<%# WebApp.ParamDefine.Common.Ck %>" onclick="ClickCheckboxInputAndUncheckOthers('__ckbasereward')" /> </asp:TableHeaderCell>
                                <asp:TableHeaderCell> <%# WebApp.DisplayString.ServerName %> </asp:TableHeaderCell>
                                <asp:TableHeaderCell> <%# WebApp.DisplayString.OrderName %> </asp:TableHeaderCell>
                                <asp:TableHeaderCell> <%# WebApp.DisplayString.Item %></asp:TableHeaderCell>
                                <asp:TableHeaderCell> <%# WebApp.DisplayString.DateStart %></asp:TableHeaderCell>
                                <asp:TableHeaderCell> <%# WebApp.DisplayString.DateEnd %></asp:TableHeaderCell>
                            </asp:TableHeaderRow>
                        </asp:Table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <br />
        <table>
            <tr>
                <td> <asp:Button runat="server" ID="AddBaseRewardButton" Text="<%# WebApp.DisplayString.AddReward %>" OnClick="AddExtraBaseRewardButton_OnClick" /> </td>
                <td> <asp:Button runat="server" ID="DelBaseRewardButton" Text="<%# WebApp.DisplayString.DeleteReward %>" onclick="<%# MakeAskDelete(WebApp.TASK_TYPE.PLAYGUIDE_EXTRA_BASE_REWARD_DELETE.Code) %>" /> </td>
                <td> <asp:Button runat="server" ID="DelAllBaseRewardButton" Text="<%# WebApp.DisplayString.DelAllExtraBaseReward %>" OnClick="DelAllBaseRewardButton_OnClick" /> </td>
                <td> <asp:Button runat="server" ID="BaseRewardListToExcelButton" Text="<%# WebApp.DisplayString.ExcelDownload %>" OnClick="BaseRewardListToExcelButton_OnClick" /> </td>
                <td> <asp:Button runat="server" ID="AddExtraRewardByExcel"  Text="<%# WebApp.DisplayString.UploadEventMatchingExcel %>" OnClick="AddExtraRewardByExcel_OnClick"/> </td>
            </tr>
        </table>
        <table>
            <tr>
                <td> 
                    <asp:Panel runat="server" ID="BaseRewardPageNo"> 
                        <asp:Label ID="BaseRewardPageNoHeader" runat="server" Text="<%# WebApp.DisplayString.SearchPage %>" />
                        <asp:Label ID="BaseRewardPageNoName" runat="server" Text="" /> 
                    </asp:Panel> 
                </td>
                <td> <asp:Button runat="server" ID="BaseRewardPrevPageButton" Text="<%# WebApp.DisplayString.PrevPage %>" Visible="false" OnClick="BaseRewardPrevPageButton_OnClick" /> </td>
                <td> <asp:Button runat="server" ID="BaseRewardNextPageButton" Text="<%# WebApp.DisplayString.NextPage %>" Visible="false" OnClick="BaseRewardNextPageButton_OnClick" /> </td>
            </tr>
        </table>

        <!-- 추가 보상 설정-->
        <br />
        <br />
        <table>
            <tr> 
                <td> <strong><%= WebApp.DisplayString.PlayGuideExtraReward %> </strong></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Panel runat="server" ID="PlayGuideExtraRewardResult">
                        <asp:Table runat="server" ID="PlayGuideExtraRewardTable" CssClass="lineTable">
                            <asp:TableHeaderRow>
                                <asp:TableHeaderCell> <input type="checkbox" id="__ckdailyreward_all" name="<%= WebApp.ParamDefine.Common.Ck %>" onclick="ClickCheckboxInputAndUncheckOthers('__ckdailyreward')" /> </asp:TableHeaderCell>
                                <asp:TableHeaderCell> <%# WebApp.DisplayString.ServerName %> </asp:TableHeaderCell>
                                <asp:TableHeaderCell> <%# WebApp.DisplayString.OrderName %> </asp:TableHeaderCell>
                                <asp:TableHeaderCell> <%# WebApp.DisplayString.Item %></asp:TableHeaderCell>
                                <asp:TableHeaderCell> <%# WebApp.DisplayString.DateStart %></asp:TableHeaderCell>
                                <asp:TableHeaderCell> <%# WebApp.DisplayString.DateEnd %></asp:TableHeaderCell>
                                <asp:TableHeaderCell> <%# WebApp.DisplayString.PlayGuideAddRewardDays %></asp:TableHeaderCell>
                                <asp:TableHeaderCell> <%# WebApp.DisplayString.PlayGuideAddRewardTime %></asp:TableHeaderCell>
                            </asp:TableHeaderRow>
                        </asp:Table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <br />
        <table>
            <tr>
                <td> <asp:Button runat="server" ID="AddRewardButton" Text="<%# WebApp.DisplayString.AddReward %>" OnClick="AddExtraRewardButton_OnClick" /> </td>
                <td> <asp:Button runat="server" ID="DelRewardButton" Text="<%# WebApp.DisplayString.DeleteReward %>" onclick="<%# MakeAskDelete(WebApp.TASK_TYPE.PLAYGUIDE_EXTRA_REWARD_DELETE.Code) %>" /> </td>
                <td> <asp:Button runat="server" ID="DelAllRewardButton" Text="<%# WebApp.DisplayString.DelAllExtraReward %>" OnClick="DelAllRewardButton_OnClick" /> </td>
                <td> <asp:Button runat="server" ID="RewardListToExcelButton" Text="<%# WebApp.DisplayString.ExcelDownload %>" OnClick="RewardListToExcelButton_OnClick" /> </td>
                <td> <asp:Button runat="server" ID="AddExtraDailyRewardByExcel" Text="<%# WebApp.DisplayString.UploadEventMatchingExcel %>" OnClick="AddExtraDailyRewardByExcel_OnClick" /> </td>
            </tr>
        </table>
        <table>
            <tr>
                <td> 
                    <asp:Panel runat="server" ID="RewardPageNo">
                        <asp:Label runat="server" ID="RewardPageNoHeader" Text="<%# WebApp.DisplayString.SearchPage %>" />
                        <asp:Label runat="server" ID="RewardPageNoName" />
                    </asp:Panel> 
                </td>
                <td> <asp:Button runat="server" ID="RewardPrevPageButton" Text="<%# WebApp.DisplayString.PrevPage %>" Visible="false" OnClick="RewardPrevPageButton_OnClick" /> </td>
                <td> <asp:Button runat="server" ID="RewardNextPageButton" Text="<%# WebApp.DisplayString.NextPage %>" Visible="false" OnClick="RewardNextPageButton_OnClick" /> </td>
            </tr>
        </table>
    </form>

</asp:Content>