﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/CustomAskDialog.Master" AutoEventWireup="true" CodeBehind="ATO_EventMatchingReward.aspx.cs" Inherits="WebApp.WorldFestival.ATO_EventMatchingReward" %>
<%@ Import Namespace="WebApp" %>

<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentBody" runat="server">
	<form id="Form2" runat="server" class="nolineTable">   
		<!-- 잘못 입력 된 값에 대한 안내 메세지  -->
		<p style="color:black; font-weight:bold"><asp:Label ID="ErrorInfo" runat="server" visible="false" ForeColor="Red" Font-Bold="true"/></p><br />

		<asp:Panel id="EditPanel" runat="server" visible="true">
			<table>
				<tr>
					<th><%# WebApp.DisplayString.ServerName %></th>
					<th><%# WebApp.DisplayString.ChangeTarget %></th>
                    <th><%# WebApp.DisplayString.ChangeState %></th>
				</tr>
				<tr>
					<td><asp:label runat="server" ID="ServerName" /></td>
                    <td><asp:label runat="server" ID="DailyOrWeekly" /></td>
					<td><asp:label runat="server" ID="SettingsFlagStr"/></td>
				</tr>
			</table>

			<!-- 메모 입력란 -->
			<p style="font-weight:bold">[<%= WebApp.DisplayString.MemoStr %>]</p><br />
			<asp:TextBox runat="server" width="250px" height="100px" ID="MemoBox" TextMode="MultiLine" style="overflow:auto"/><br /><br />    

			<!-- 확인 버튼 -->
			<asp:Button runat="server" ID="ConfirmButton" text='<%# WebApp.DisplayString.OK %>' OnClick="ConfirmButton_Click" OnClientClick="if(!SubmitConfirm()) return false;" />
		</asp:Panel>

		<asp:Button runat="server" ID="CloseButton" text="<%# WebApp.DisplayString.Close%>" Visible="false" OnClick="CloseButton_Click"/><br />
	</form>
    <script type="text/javascript">
        function SubmitConfirm()
        {
            if (confirm("<%#DisplayString.EditAsk%>"))
                return true;
            else
                return false;
        }
    </script>
</asp:Content>
