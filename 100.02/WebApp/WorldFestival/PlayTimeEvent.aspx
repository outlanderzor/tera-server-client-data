﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="PlayTimeEvent.aspx.cs" Inherits="WebApp.WorldFestival.PlayTimeEvent" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headerContentHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">
    <form id="Form1" runat="server">
        <asp:DropDownList runat="server" ID="ServerDDL" DataSource="<%# WebApp.WebAdminDB.ServerListWithAll.Select(x => new {Text=x.mName, Value=x.mNo}) %>" DataTextField="Text" DataValueField="Value" />
        <asp:Button runat="server" ID="ServerChoice" Text="<%# WebApp.DisplayString.Search %>" OnClick="ServerChoice_Click" />
        <br /><br />
        <asp:Button runat="server" Text="<%# WebApp.DisplayString.AddEvent %>" ID="AddEvent" OnClick="AddEvent_Click" /> 
        <asp:Button runat="server" Text="<%# WebApp.DisplayString.DeleteEvent %>" ID="DeleteEvent" OnClick="DeleteEvent_Click" /> 
        <asp:Button runat="server" Text="<%# WebApp.DisplayString.ModifyEvent %>" ID="ModifyEvent" OnClick="ModifyEvent_Click" />
    <br /><br />
    <table class="lineTable">
    <asp:Repeater runat="server" ID="PlayTimeEventRepeater">
        <HeaderTemplate>
            <tr>
                <th></th>
                <th><%# WebApp.DisplayString.ServerName %></th>
                <th><%# WebApp.DisplayString.EventId %></th>
                <th><%# WebApp.DisplayString.AccountTrait %></th>
                <th><%# WebApp.DisplayString.EventPeriod %></th>
                <th><%# WebApp.DisplayString.RewardInfo %></th>
            </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td><asp:CheckBox runat="server" ID="EventCB" AutoPostBack="true" OnCheckedChanged="EventCB_CheckedChanged" /></td>
                <td><%# Eval("ServerName") %> <asp:HiddenField runat="server" ID="ServerNo" Value='<%# Eval("ServerNo") %>' /></td>
                <td><asp:Label runat="server" ID="EventId" Text='<%# Eval("EventId") %>' /> </td>
                <td><%# Eval("AccountTrait") %> </td>
                <td><%# Eval("EventPeriod") %></td>
                <td><%# Eval("RewardInfo") %></td>
            </tr>
        </ItemTemplate>
    </asp:Repeater>
    </table>
        </form>
</asp:Content>
