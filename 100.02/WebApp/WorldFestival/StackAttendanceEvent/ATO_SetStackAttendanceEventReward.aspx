﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_SetStackAttendanceEventReward.aspx.cs" Inherits="WebApp.WorldFestival.StackAttendanceEvent.ATO_SetStackAttendanceEventReward" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
    <div class="ato-serverlist">
		<label><%# WebApp.DisplayString.CheckServer %></label>
		<ul>
			<asp:Repeater runat="server" ID="ServerCheckList">
				<ItemTemplate>
					<li><asp:CheckBox runat="server" ID="ServerName" Text='<%# Eval("ServerName") %>' /><asp:HiddenField runat="server" ID="ServerNo" Value='<%# Eval("ServerNo") %>' /></li>
				</ItemTemplate>
			</asp:Repeater>
		</ul>
		<label><%# WebApp.DisplayString.CheckAllServer %></label>
	</div>

    <div class="ato-main">
		<ul class="form-style">
			<table class="lineTable width-full">
     		<asp:Repeater runat="server" ID="StackAttendanceEventList">
                 <HeaderTemplate>
                   <tr>
                       <th><%# WebApp.DisplayString.ServerName %> </th>
                       <th><%# WebApp.DisplayString.StartTime %> </th>
                       <th><%# WebApp.DisplayString.EndTime %> </th>
                       <th><%# WebApp.DisplayString.ResetHour %> </th>
                   </tr>
                 </HeaderTemplate>
                 <ItemTemplate>
					<tr>
						<td><%# Eval("ServerName") %></td>
                        <td><%# Eval("StartTime") %></td>
                        <td><%# Eval("EndTime") %> </td>
                        <th><%# Eval("ResetHour") %> </th>
					</tr>
				</ItemTemplate>
            </asp:Repeater>
        </table>
	</div>
    <div class="ato-rewardlist">
        <table class="lineTableAlignCenter">
            <li>
                <label class="width150"><%# WebApp.DisplayString.RewardType %></label>
                <asp:DropDownList runat="server" ID="RewardType" class="rewardTypeList" DataValueField="TypeId" DataTextField="TypeName" OnSelectedIndexChanged="DropDownRewardType_SelectedIndexChanged" AutoPostBack="true"/>
                <asp:Button runat="server" Text="<%# WebApp.DisplayString.Refresh %>" OnClick="ChangedEventInfo" AutoPostBack="true" />
            </li>
            <tr>
                <td><%# WebApp.DisplayString.Day %></td>
                <td><%# WebApp.DisplayString.ItemTemplateId %></td>
                <td><%# WebApp.DisplayString.ItemAmount %></td>
                <td><%# WebApp.DisplayString.Highlight %></td>
                <td><%# WebApp.DisplayString.ItemName %></td>
                <td><%# WebApp.DisplayString.ItemMaxAmount %></td>
            </tr>
            <asp:Repeater runat="server" ID="RewardList">
				<ItemTemplate>
					<tr>
                        <td><asp:Label runat="server" ID="Day" Text='<%# Eval("Day") %>'/></td>
                        <td><asp:TextBox runat="server" ID="ItemTemplateId" Text='<%# Eval("ItemTemplateId") %>'/></td>
                        <td><asp:TextBox runat="server" ID="ItemAmount" Text='<%# Eval("ItemAmount") %>'/></td>
                        <td><asp:CheckBox runat="server" ID="Highlight" checked='<%# Eval("Highlight") %>'/></td>
                        <td><%# Eval("ItemName") %></td>
                        <td><%# Eval("ItemMaxAmount") %></td>
					</tr>
				</ItemTemplate>
			</asp:Repeater>
        </table>
	</div>
    <script type="text/javascript">
    	    $(function () {
    	        $('[type=date]').each(function () {
    	            $(this).appendDtpicker({ "dateOnly": false, "minuteInterval": 1 });
    	        })
    	        $('[type=date]').keypress(function () { return false; })
    	        $('[type=date]').keydown(function () { return false; })
    	    });
	</script>

</asp:Content>
