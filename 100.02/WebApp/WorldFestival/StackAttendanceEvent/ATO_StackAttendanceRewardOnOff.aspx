﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_StackAttendanceRewardOnOff.aspx.cs" Inherits="WebApp.WorldFestival.StackAttendanceEvent.ATO_StackAttendanceRewardOnOff" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
    <div class="ato-serverlist">
		<label><%# WebApp.DisplayString.CheckServer %></label>
		<ul>
			<asp:Repeater runat="server" ID="ServerCheckList">
				<ItemTemplate>
					<li><asp:CheckBox runat="server" ID="ServerName" Text='<%# Eval("ServerName") %>' /><asp:HiddenField runat="server" ID="ServerNo" Value='<%# Eval("ServerNo") %>' /></li>
				</ItemTemplate>
			</asp:Repeater>
		</ul>
		<label><%# WebApp.DisplayString.CheckAllServer %></label>
	</div>
    <div class="ato-main">
		<ul class="form-style">
			<li>
                <label class="width150" style="display:inline"><%# WebApp.DisplayString.OnOff %></label>
                <asp:RadioButtonList class="radio-group" style="display:inline" runat="server" ID="OnOff" DataValueField="OnOff" DataTextField="OnOffStr" AutoPostBack="true" OnSelectedIndexChanged="OnOff_SelectedIndexChanged" RepeatLayout="UnorderedList"/>
            </li>
		</ul>
	</div>
</asp:Content>
