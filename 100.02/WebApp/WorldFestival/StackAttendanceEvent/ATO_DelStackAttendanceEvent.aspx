﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_DelStackAttendanceEvent.aspx.cs" Inherits="WebApp.WorldFestival.StackAttendanceEvent.ATO_DelStackAttendanceEvent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
    <table class="lineTable width-full">
     	<asp:Repeater runat="server" ID="StackAttendanceEventList">
                <HeaderTemplate>
                <tr>
                    <th> <asp:CheckBox runat="server" ID="ServerCheckAll" AutoPostBack="true" Checked="true" OnCheckedChanged="ServerCheckAll_CheckedChanged" /> </th>
                    <th><%# WebApp.DisplayString.ServerName %> </th>
                    <th><%# WebApp.DisplayString.StartTime %> </th>
                    <th><%# WebApp.DisplayString.EndTime %> </th>
                    <th><%# WebApp.DisplayString.AttendanceCondition%> </th>
                    <th><%# WebApp.DisplayString.ResetHour %> </th>
                    <th><%# WebApp.DisplayString.RewardInfo %> </th>
                </tr>
                </HeaderTemplate>
                <ItemTemplate>
				<tr>
					<td><asp:CheckBox runat="server" ID="ServerCheck" Checked="true"/><asp:HiddenField runat="server" ID="ServerID" Value='<%# Eval("ServerNo") + " " + Eval("EventId") %>'/></td>
					<td><%# Eval("ServerName") %></td>
                    <td><%# Eval("StartTime") %></td>
                    <td><%# Eval("EndTime") %> </td>
					<td><%# Eval("ConditionType") %> </td>
                    <td><%# Eval("ResetHour") %> </td>
                    <td><%# Eval("RewardInfoStr") %> </td>
				</tr>
			</ItemTemplate>
        </asp:Repeater>
    </table>
</asp:Content>
