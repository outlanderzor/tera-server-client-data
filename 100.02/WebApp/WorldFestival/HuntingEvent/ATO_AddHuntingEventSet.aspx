﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_AddHuntingEventSet.aspx.cs" Inherits="WebApp.WorldFestival.HuntingEvent.ATO_AddHuntingEventSet" %>

<asp:Content ID="Content2" ContentPlaceHolderID="TERA_ATO" runat="server">

    <div class="ato-serverlist">
		<label><%# WebApp.DisplayString.CheckServer %></label>
		<ul>
			<asp:Repeater runat="server" ID="ServerCheckList">
				<ItemTemplate>
					<li><asp:CheckBox runat="server" ID="ServerName" Text='<%# Eval("ServerName") %>' /><asp:HiddenField runat="server" ID="ServerNo" Value='<%# Eval("ServerNo") %>' /></li>
				</ItemTemplate>
			</asp:Repeater>
		</ul>
	</div>

    <div class="ato-main">

        <asp:Table runat="server">
            <asp:TableRow>
                <asp:TableCell><asp:Label runat="server" Text="<%# WebApp.DisplayString.EventName %>" /></asp:TableCell>
                <asp:TableCell><asp:TextBox runat="server" ID="EventName" /></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell><asp:Label runat="server" Text="<%# WebApp.DisplayString.StartTime %>" /></asp:TableCell>
                <asp:TableCell><asp:TextBox runat="server" ID="StartTime" type="date" /></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell><asp:Label runat="server" Text="<%# WebApp.DisplayString.EndTime %>" /></asp:TableCell>
                <asp:TableCell><asp:TextBox runat="server" ID="EndTime" type="date" /></asp:TableCell>
            </asp:TableRow>
        </asp:Table>

    </div>

    <script type="text/javascript">
        $(function () {
            $('[type=date]').each(function () {
                $(this).appendDtpicker({ "dateOnly": false, "minuteInterval": 1 });
            })
            $('[type=date]').keypress(function () { return false; })
            $('[type=date]').keydown(function () { return false; })
        });
	</script>

</asp:Content>
