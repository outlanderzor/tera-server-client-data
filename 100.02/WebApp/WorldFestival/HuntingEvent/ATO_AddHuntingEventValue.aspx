﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_AddHuntingEventValue.aspx.cs" Inherits="WebApp.WorldFestival.HuntingEvent.ATO_AddHuntingEventValue" %>

<asp:Content ID="Content2" ContentPlaceHolderID="TERA_ATO" runat="server">

    <%# WebApp.DisplayString.AlertCheckAllServer %>
    <br />
    <%# WebApp.DisplayString.DungeonClearCountEventIsCountable %>
    <br />

    <asp:Table runat="server">
        <asp:TableRow>
            <asp:TableCell><%# WebApp.DisplayString.ServerName %></asp:TableCell>
            <asp:TableCell><asp:Label runat="server" ID="ServerName" /></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><%# WebApp.DisplayString.EventChoice %></asp:TableCell>
            <asp:TableCell><asp:DropDownList runat="server" ID="EventListDDL" DataTextField="Text" DataValueField="Value" /></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><%# WebApp.DisplayString.HuntingzoneEventList %></asp:TableCell>
            <asp:TableCell><asp:DropDownList runat="server" ID="HuntingEventTypeDDL" AutoPostBack="true" DataTextField="Text" DataValueField="Value" OnSelectedIndexChanged="HuntingEventTypeDDL_OnSelectedIndexChanged" /></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><%# WebApp.DisplayString.HuntingzoneEventValue %></asp:TableCell>
            <asp:TableCell>
                <asp:TextBox runat="server" ID="EventValue" />
                <asp:RegularExpressionValidator runat="server" ControlToValidate="EventValue" ValidationExpression="([0-9])[0-9]*[.]?[0-9]*" ErrorMessage="<%# WebApp.DisplayString.ErrorMsg_OnlyNumbers %>" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><%# WebApp.DisplayString.WorldFestivalAddHuntingZone %></asp:TableCell>
            <asp:TableCell>
                <!-- 사냥터 선택 -->
                <asp:Repeater runat="server" ID="SelectedHuntingZoneList">
                    <ItemTemplate>
                        <%# string.Format("[{0}][({1}) {2}]", Eval("HuntingZoneTypeName"), Eval("HuntingZoneId"), Eval("HuntingZoneName")) %> &nbsp;
                        <asp:Button runat="server" Text="<%# WebApp.DisplayString.DeleteFromList %>"
                            CommandName="HuntingZoneDeleteFromList" CommandArgument='<%# Eval("HuntingZoneType") + "|" + Eval("HuntingZoneId") %>' OnCommand="HuntingZoneDeleteFromList_OnCommand" />
                        <br />
                    </ItemTemplate>
                </asp:Repeater>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><%# WebApp.DisplayString.AddHuntingZone %></asp:TableCell>
            <asp:TableCell>
                <%# WebApp.DisplayString.Type %> &nbsp;
                <asp:DropDownList runat="server" ID="HuntingZoneTypeDDL" AutoPostBack="true" DataTextField="Text" DataValueField="Value" OnSelectedIndexChanged="HuntingZoneTypeDDL_OnSelectedIndexChanged" /> &nbsp;

                <%# WebApp.DisplayString.HuntingZone %> &nbsp;
                <asp:DropDownList runat="server" ID="HuntingZoneDDL" AutoPostBack="true" DataTextField="Text" DataValueField="Value" OnSelectedIndexChanged="HuntingZoneDDL_OnSelectedIndexChanged" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>

    <br />

</asp:Content>
