﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="VipStoreEvent.aspx.cs" Inherits="WebApp.WorldFestival.VipStoreEvent" %>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">
    <form id="Form1" runat="server" class="nolineTable">
    
        <table class="nolineTable">
            <tr>
                <td> <%= WebApp.DisplayString.ServerChoice %> </td>
                <td> <asp:DropDownList runat="server" ID="ServNoDDL" DataTextField="Text" DataValueField="Value" /> </td>
                <td> <asp:Button runat="server" ID="SearchButton" OnClick="SearchButton_Click" /> </td>
                <td> &nbsp; </td>
            </tr>
        </table><br />
        
        <asp:Button runat="server" ID="AddEventButton" OnClick="AddSaleButton_Click" /><br /><br /><br />

        <asp:GridView CssClass="lineTable" ID="GVSMLList" runat="server" AutoGenerateColumns="False" ShowHeader="true" ShowHeaderWhenEmpty="true" OnRowCommand="OnRowCommand">
        <Columns>
            <asp:TemplateField   HeaderStyle-Width="100">
                <HeaderTemplate><%=WebApp.DisplayString.ServerName%></HeaderTemplate>
                <ItemTemplate><%# Eval("serverName").ToString()%></ItemTemplate>
            </asp:TemplateField>
                <asp:TemplateField >
                <HeaderTemplate><%=WebApp.DisplayString.SlotId%></HeaderTemplate>
                <ItemTemplate><%# Eval("slotId").ToString()%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField >
                <HeaderTemplate><%=WebApp.DisplayString.DiscountRate%></HeaderTemplate>
                <ItemTemplate><%# Eval("discountRate", "{0:#.##}") %>%</ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField >
                <HeaderTemplate><%=WebApp.DisplayString.StartTime%></HeaderTemplate>
                <ItemTemplate><%# Eval("startTime").ToString()%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField >
                <HeaderTemplate><%=WebApp.DisplayString.EndTime%></HeaderTemplate>
                <ItemTemplate><%# Eval("endTime").ToString()%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField >
                <HeaderTemplate></HeaderTemplate>
                <ItemTemplate><asp:Button runat="server" Text='<%# WebApp.DisplayString.DeleteEvent%>' ID="DelButton" CommandArgument='<%# Eval("serverNo").ToString() +"," + Eval("eventId").ToString()%>'/></ItemTemplate>
            </asp:TemplateField>
        </Columns>
        </asp:GridView>
    </form>

</asp:Content>
