﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_AddPlayTimeEvent.aspx.cs" Inherits="WebApp.WorldFestival.ATO_AddPlayTimeEvent" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">

    <table style="margin-bottom: 10px; border: none;">
        <tr style="margin: 0px;">
            <td><%# WebApp.DisplayString.AccountTrait %></td>
            <td><asp:RadioButtonList ID="AccountTraitId" runat="server" DataSource="<%#WebApp.DataSheetManager.GetAccountTraits(WebApp.Global.Publisher).Select(x => new { Text = x.Name, Value = x.Id })%>" DataTextField="Text" DataValueField="Value" RepeatColumns="5" RepeatLayout="Table" /></td>
        </tr>

        <tr style="margin: 0px;">
            <td><%# WebApp.DisplayString.ServerChoice %></td>
            <td><asp:CheckBoxList ID="Servers" runat="server" DataSource="<%#WebApp.WebAdminDB.ServerListWithAll.Select(x => new { Text = x.mName, Value = x.mNo })%>" DataTextField="Text" DataValueField="Value" RepeatColumns="5" RepeatLayout="Table" /></td>
        </tr>
            
        <tr style="margin: 0px;">
            <td><%# WebApp.DisplayString.EventPeriod %></td>
            <td><asp:TextBox runat="server" Rows="1" ID="StartTime" type="date" onkeypress="return false;" onkeydown="return false;" /> ~ <asp:TextBox runat="server" Rows="1" ID="EndTime" type="date"  onkeypress="return false;" onkeydown="return false;" />
                <script type="text/javascript">
                    $(function () {
                        $('*[type=date]').appendDtpicker();
                    });
                </script>
            </td>
        </tr>
    </table>
    <br />
    <p style="font-weight:bold"><%# WebApp.DisplayString.DailyPlayTimeReward %> </p>
    <table style="margin-bottom: 10px; border: none;">
        <asp:Repeater runat="server" ID="DailyRewardData" DataSource="<%# Enumerable.Range(1, 6) %>">
            <HeaderTemplate>
                <tr>
                    <th><%# WebApp.DisplayString.AdminRewardId %></th>
                    <th><%# WebApp.DisplayString.PlayTime %></th>
                    <th><%# WebApp.DisplayString.GiveItemTemplateId %></th>
                    <th><%# WebApp.DisplayString.GiveItemAmount %></th>
                    <th><%# WebApp.DisplayString.LinkToAwsomium %></th>
                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><asp:Label runat="server" ID="RewardId" Width="50" Text="<%# Container.DataItem.ToString() %>" /></td>
                    <td><asp:TextBox runat="server" Rows="1" ID="PlayTime" Width="150" /></td>
                    <td><asp:TextBox runat="server" Rows="1" ID="ItemTemplateId"  Width="150" /></td>
                    <td><asp:TextBox runat="server" Rows="1" ID="ItemCount"  Width="150" /></td>
                    <td><asp:TextBox runat="server" Rows="1" ID="Awsomium"  Width="150" /></td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>

    <p style="font-weight:bold"><%# WebApp.DisplayString.TotalPlayTimeReward %> </p>
    <table style="margin-bottom: 10px; border: none;">
        <asp:Repeater runat="server" ID="TotalRewardData" DataSource="<%# Enumerable.Range(1, 6) %>">
            <HeaderTemplate>
                <tr>
                    <th><%# WebApp.DisplayString.AdminRewardId %></th>
                    <th><%# WebApp.DisplayString.PlayTime %></th>
                    <th><%# WebApp.DisplayString.GiveItemTemplateId %></th>
                    <th><%# WebApp.DisplayString.GiveItemAmount %></th>
                    <th><%# WebApp.DisplayString.LinkToAwsomium %></th>
                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><asp:Label runat="server" ID="RewardId" Width="50" Text="<%# Container.DataItem.ToString() %>" /></td>
                    <td><asp:TextBox runat="server" Rows="1" ID="PlayTime" Width="150" /></td>
                    <td><asp:TextBox runat="server" Rows="1" ID="ItemTemplateId"  Width="150" /></td>
                    <td><asp:TextBox runat="server" Rows="1" ID="ItemCount"  Width="150" /></td>
                    <td><asp:TextBox runat="server" Rows="1" ID="Awsomium"  Width="150" /></td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
</asp:Content>
