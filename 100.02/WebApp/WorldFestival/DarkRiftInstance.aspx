﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="DarkRiftInstance.aspx.cs" Inherits="WebApp.WorldFestival.DarkRiftInstance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form id="Form2" runat="server" class="nolineTable">
    
        <table class="nolineTable">
            <tr>
                <td> <%= WebApp.DisplayString.ServerChoice %> </td>
                <td> <asp:DropDownList runat="server" ID="ServNoDDL" DataTextField="Text" DataValueField="Value" /> </td>
                <td> &nbsp; </td>
            </tr>
        </table>
        
        <table class="nolineTable">
            <tr>
                <td> <%= WebApp.DisplayString.ViewType %> </td>
                <td> <asp:DropDownList runat="server" ID="ViewTypeDDL" DataTextField="Text" DataValueField="Value" /> </td>
                <td> <asp:Button runat="server" ID="SearchButton" OnClick="SearchButton_Click" /> </td>
            </tr>
        </table>
        
        <asp:Panel runat="server" ID="ResultPanel" Visible="false">
        </asp:Panel>
    </form>
</asp:Content>
