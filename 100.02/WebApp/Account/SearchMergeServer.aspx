﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="SearchMergeServer.aspx.cs" Inherits="WebApp.Account.SearchMergeServer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form runat="server" id ="resultForm" >

        <table class="nolineTable" >
        <tr>
            <td> <%= WebApp.DisplayString.NewServerName %> </td>
            <td> <asp:DropDownList runat="server" ID="NewServerDDL" DataTextField="ServerName" DataValueField="ServerNo" />
        </tr>
        <tr>
            <td> <%= WebApp.DisplayString.OldUserName %> </td>
            <td> <asp:TextBox runat="server" ID="OldNameTB" />
        </tr>
        <tr>
             <td> <asp:Button runat="server" ID="SubmitButton" OnClick="SubmitButton_Click" /> </td>
        </tr>
        </table>
    
        <asp:GridView CssClass="lineTable" ID="ChangedUserNameListGV" runat="server" AutoGenerateColumns="false" ShowHeader="true">
        <Columns>
            <asp:TemplateField>
                <HeaderTemplate> <%= WebApp.DisplayString.OldServerName %></HeaderTemplate>
                <ItemTemplate> <%# Eval("OldServerName") %></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate> <%= WebApp.DisplayString.OldUserName%></HeaderTemplate>
                <ItemTemplate> <%# Eval("OldUserName") %> ( <%# Eval("OldUserDbId") %> )</ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate> <%= WebApp.DisplayString.NewUserName%></HeaderTemplate>
                <ItemTemplate> <a href="<%# Eval("NameURL")%>" /><%# Eval("NewUserName")%></a> </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        </asp:GridView>

        <asp:Label runat="server" ID="MessageLable" Visible="false" />

    </form>
    

</asp:Content>
