﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_GuildQuestControl.aspx.cs" Inherits="WebApp.ContentsControl.GuildQuest.ATO_GuildQuestControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
    <table class="lineTable" style="margin-bottom:20px;">
		<tr>
			<th><%# WebApp.DisplayString.ServerName %></th>
			<th><%# WebApp.DisplayString.State %></th>
		</tr>
		<tr>
			<td><asp:Label runat="server" ID="ServerName"/></td>
			<td><asp:Label runat="server" ID="OnOff"/></td>
		</tr>
	</table>
	<script type="text/javascript">
	    $(document).ready(function () {
	        $('input#ContentBody_Submit').val('<%=this.Request["isOn"].ToLower()=="true" ? WebApp.DisplayString.Off : WebApp.DisplayString.On %>')
	    })
	</script>
</asp:Content>
