﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="GmNote.aspx.cs" Inherits="WebApp.ContentsControl.GmNote.GmNote" %>

<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">
    <form runat="server">
        
        <asp:Button runat="server" ID="EnableContentsButton" Text="<%# WebApp.DisplayString.EnableMenu %>" OnClick="EnableContentsButton_Click" />
        <asp:Button runat="server" ID="DisableContentButton" Text="<%# WebApp.DisplayString.DisableMenu %>" OnClick="DisableContentButton_Click" />

        <br />

        <div>
            <table class="lineTable ">
                <asp:Repeater runat="server" ID="OnOffStateList">
                    <HeaderTemplate>
                        <tr>
                            <th><asp:CheckBox runat="server" ID="AllCheckBox" AutoPostBack="true" OnCheckedChanged="AllCheckBox_CheckedChanged" /></th>
                            <th><%# WebApp.DisplayString.ServerName %></th>
                            <th><%# WebApp.DisplayString.MenuStatus %></th>
                        </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:CheckBox runat="server" ID="ServerCheck" />
                                <asp:HiddenField runat="server" ID="CheckHiddenField" Value='<%# Eval("ServerNo") + "|" + ((bool)Eval("IsEnabled") ? "true" : "false") %>' />
                            </td>
                            <td><%# Eval("ServerName") %></td>
                            <td><%# (bool)Eval("IsEnabled") ? WebApp.DisplayString.On : WebApp.DisplayString.Off %></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
        </div>
    </form>
</asp:Content>
