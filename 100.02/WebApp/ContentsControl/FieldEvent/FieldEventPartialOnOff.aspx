﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="FieldEventPartialOnOff.aspx.cs" Inherits="WebApp.ContentsControl.FieldEvent.FieldEventPartialOnOff" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headerContentHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">
    <form id="Form1" runat="server">
	<table class="lineTable" style="margin-bottom:10px;">
		<tr>
			<th><%# WebApp.DisplayString.ServerChoice %></th>
			<th><%# WebApp.DisplayString.Search %></th>
		</tr>
		<tr>
			<td><asp:DropDownList runat="server" ID="ServerSelect" DataSource="<%# WebApp.WebAdminDB.ServerListWithAll.Select(x => new { ServerName = x.mName, ServerNo = x.mNo }) %>" DataTextField="ServerName" DataValueField="ServerNo"/></td>
			<td><asp:Button runat="server" ID="SelectOption" Text="Search" OnClick="SelectOption_Click" /></td>
		</tr>
	</table>
	<table class="lineTable ">
		<asp:Repeater runat="server" ID="FieldEventPartialOnOffStateList">
			<HeaderTemplate>
				<tr>
   					<th><%# WebApp.DisplayString.ServerName %></th>
					<th><%# WebApp.DisplayString.Guardians_Mission_Name %></th>
					<th><%# WebApp.DisplayString.Location %></th>
					<th><%# WebApp.DisplayString.State %></th>
					<th><%# WebApp.DisplayString.Control %></th>
				</tr>
			</HeaderTemplate>
			<ItemTemplate>
				<tr>
					<td><%# Eval("ServerName") %></td>
   					<td><%# Eval("QuestName") %></td>
					<td><%# Eval("ContinentId") %></td>
					<td><%# (bool)Eval("State") ? "On" : "Off" %></td>
					<td>
						<asp:button runat="server" ID="EventOnOff" Text='<%# (bool)Eval("State") ? "Off" : "On" %>' CommandName="EventOnOff" CommandArgument='<%# Eval("ServerNo") + "/" + ((bool)Eval("State") ? "Off" : "On") + "/" + Eval("MinorId") + "/" + Eval("ContinentId")  %>' OnCommand="EventOnOff_Command"/>
					</td>
				</tr>
			</ItemTemplate>
		</asp:Repeater>
	</table>
</form>
</asp:Content>

