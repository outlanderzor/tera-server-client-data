﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="FieldEventReward.aspx.cs" Inherits="WebApp.ContentsControl.FieldEvent.FieldEventReward" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headerContentHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">
    <form id="Form1" runat="server">
	<table class="lineTable" style="margin-bottom:10px;">
		<tr>
			<th><%# WebApp.DisplayString.ServerChoice %></th>
			<th><%# WebApp.DisplayString.Search %></th>
		</tr>
		<tr>
			<td><asp:DropDownList runat="server" ID="ServerSelect" DataSource="<%# WebApp.WebAdminDB.ServerListWithAll.Select(x => new { ServerName = x.mName, ServerNo = x.mNo }) %>" DataTextField="ServerName" DataValueField="ServerNo"/></td>
			<td><asp:Button runat="server" ID="SelectOption" Text="<%# WebApp.DisplayString.Search %>" OnClick="SelectOption_Click" /></td>
		</tr>
	</table>
    <table>
        <tr>
		    <td><asp:Button runat="server" ID="AddFieldEventReward" Text="<%# WebApp.DisplayString.AddReward %>" OnClick="AddFieldEventReward_Click" /></td>
		    <td><asp:Button runat="server" ID="DeleteFieldEventReward" Text="<%# WebApp.DisplayString.DeleteReward %>" OnClick="DeleteFieldEventReward_Click" /></td>
	    </tr>
    </table>
	<table class="lineTable ">
		<asp:Repeater runat="server" ID="FieldEventRewardList">
			<HeaderTemplate>
				<tr>
                    <th><asp:CheckBox runat="server" ID="RewardCheckAll" AutoPostBack="true" OnCheckedChanged="ServerCheckAll_CheckedChanged"/></th>
   					<th><%# WebApp.DisplayString.ServerName %></th>
                    <th><%# WebApp.DisplayString.FieldPointType %></th>
                    <th><%# WebApp.DisplayString.ShowListType %></th>
					<th><%# WebApp.DisplayString.RewardLevel %></th>
					<th><%# WebApp.DisplayString.RewardType %></th>
					<th><%# WebApp.DisplayString.RewardInfo %></th>
				</tr>
			</HeaderTemplate>
			<ItemTemplate>
				<tr>
                    <td><asp:CheckBox runat="server" ID="RewardCheck" /><asp:HiddenField runat="server" ID="Id" Value='<%# Eval("ServerNo") + " " + Eval("Id") %>'/></td>
					<td><%# Eval("ServerName") %></td>
   					<td><%# Eval("FieldPointType") %></td>
                    <td><%# Eval("ListType") %></td>
					<td><%# Eval("RewardLevel") %></td>
                    <td><%# Eval("RewardType") %></td>
                    <td><%# Eval("RewardInfo") %></td>
				</tr>
			</ItemTemplate>
		</asp:Repeater>
	</table>
    </form>
</asp:Content>
