<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/AppCode/DataSetAskDialog.master" CodeBehind="ATO_FieldEventDelReward.aspx.cs" Inherits="WebApp.ContentsControl.FieldEvent.ATO_FieldEventDelReward" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
	<table class="lineTable width-full">
		<asp:Repeater runat="server" ID="DeleteRewardList">
			<HeaderTemplate>
				<tr>
					<th><%# WebApp.DisplayString.ServerName %></th>
					<th><%# WebApp.DisplayString.FieldPointType %></th>
                    <th><%# WebApp.DisplayString.ShowListType %></th>
					<th><%# WebApp.DisplayString.RewardLevel %></th>
					<th><%# WebApp.DisplayString.RewardType %></th>
					<th><%# WebApp.DisplayString.RewardInfo %></th>
				</tr>
			</HeaderTemplate>
			<ItemTemplate>
				<tr>
					<td><%# Eval("ServerName") %></td>
					<td><%# Eval("FieldPointType") %></td>
                    <td><%# Eval("ListType") %></td>
					<td><%# Eval("RewardLevel") %></td>
					<td><%# Eval("RewardType") %></td>
					<td><%# Eval("RewardInfo") %></td>
				</tr>
			</ItemTemplate>
		</asp:Repeater>
	</table>
</asp:Content>