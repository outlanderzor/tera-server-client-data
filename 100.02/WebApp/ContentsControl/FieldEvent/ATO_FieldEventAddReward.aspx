<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/AppCode/DataSetAskDialog.master" CodeBehind="ATO_FieldEventAddReward.aspx.cs" Inherits="WebApp.ContentsControl.FieldEvent.ATO_FieldEventAddReward" %>
<asp:Content ID="Content0" ContentPlaceHolderID="ContentHeader" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
	<div class="ato-serverlist">
		<label><%# WebApp.DisplayString.CheckServer %></label>
		<ul>
			<asp:Repeater runat="server" ID="ServerCheckList">
				<ItemTemplate>
					<li><asp:CheckBox runat="server" ID="ServerName" Text='<%# Eval("ServerName") %>' /><asp:HiddenField runat="server" ID="ServerNo" Value='<%# Eval("ServerNo") %>' /></li>
				</ItemTemplate>
			</asp:Repeater>
		</ul>
		<label><%# WebApp.DisplayString.CheckAllServer %></label>
	</div>
	<div class="ato-main">
		<ul class="form-style">
			<li><label class="width100"><%# WebApp.DisplayString.RewardLevel %></label>
                <asp:TextBox runat="server" ID="MinLevel" type="number" /> ~ <asp:TextBox runat="server" ID="MaxLevel" type="number" />
			</li>
            <li><label class="width100"><%# WebApp.DisplayString.FieldPointType %></label>
                <asp:DropDownList runat="server" ID="FieldPointKindList" DataValueField="TypeId" DataTextField="TypeName" />
            </li>
			<li><label class="width100"><%# WebApp.DisplayString.RewardType %></label>
                <asp:DropDownList runat="server" ID="RewardTypeList" DataValueField="TypeId" DataTextField="TypeName"/>
			</li>
            <li><label class="width100"><%# WebApp.DisplayString.ShowListType %></label>
                <asp:DropDownList runat="server" ID="ShowListType" DataValueField="TypeId" DataTextField="TypeName"/>
			</li>
			<li><label class="width100"><%# WebApp.DisplayString.TID %></label><asp:TextBox runat="server" ID="TemplateId" type="number" class="TemplateId" /></li>
			<li><label class="width100"><%# WebApp.DisplayString.Amount %></label><asp:TextBox runat="server" ID="Amount" type="number" /></li>
		</ul>

		<asp:Button runat="server" ID="AddReward" Text="<%# WebApp.DisplayString.AddReward %>" OnClick="AddReward_Click" style="margin-left: 100px;"/>

		<table class="lineTable">
			<asp:Repeater runat="server" ID="AddRewardList">
				<HeaderTemplate>
					<tr>
						<th><%# WebApp.DisplayString.FieldPointType %></th>
                        <th><%# WebApp.DisplayString.ShowListType %></th>
					    <th><%# WebApp.DisplayString.RewardLevel %></th>
					    <th><%# WebApp.DisplayString.RewardType %></th>
					    <th><%# WebApp.DisplayString.RewardInfo %></th>
                        <th><%# WebApp.DisplayString.DeleteFromList %></th>
					</tr>
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
                        <td><%# Eval("PointType") %></td>
                        <td><%# Eval("ListType") %></td>
					    <td><%# Eval("RewardLevel") %></td>
					    <td><%# Eval("RewardType") %></td>
					    <td><%# Eval("RewardInfo") %></td>
						<td><asp:Button runat="server" ID="DeleteReward" CommandArgument='<%# Eval("Index") %>' OnCommand="DeleteReward_Command" Text="<%# WebApp.DisplayString.DeleteFromList %>" /></td>
					</tr>
				</ItemTemplate>
			</asp:Repeater>
		</table>
	</div>
</asp:Content>
