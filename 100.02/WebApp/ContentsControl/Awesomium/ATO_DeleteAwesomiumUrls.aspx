﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_DeleteAwesomiumUrls.aspx.cs" Inherits="WebApp.ContentsControl.ATO_DeleteAwesomiumUrls" %>

<asp:Content ID="Content2" ContentPlaceHolderID="TERA_ATO" runat="server">

    <asp:Table runat="server" CssClass="lineTable">
        <asp:TableRow>
            <asp:TableHeaderCell><%# WebApp.DisplayString.ServerName %></asp:TableHeaderCell>
            <asp:TableHeaderCell><%# WebApp.DisplayString.Title %></asp:TableHeaderCell>
            <asp:TableHeaderCell><%# WebApp.DisplayString.Url %></asp:TableHeaderCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><asp:Label runat="server" ID="ServerName" /></asp:TableCell>
            <asp:TableCell><asp:Label runat="server" ID="AwesomiumTitle" /></asp:TableCell>
            <asp:TableCell><asp:Label runat="server" ID="AwesomiumUrl" /></asp:TableCell>
        </asp:TableRow>
    </asp:Table>

</asp:Content>
