﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_ActivateAwesomiumUrl.aspx.cs" Inherits="WebApp.ContentsControl.ATO_ActivateAwesomiumUrl" %>

<asp:Content ID="Content2" ContentPlaceHolderID="TERA_ATO" runat="server">

    <table class="lineTable">
        <asp:Repeater runat="server" ID="AwesomiumUrls">
            <HeaderTemplate>
                <tr>
                    <th><%# WebApp.DisplayString.Title %></th>
                    <th><%# WebApp.DisplayString.Url %></th>
                    <th><%# WebApp.DisplayString.OnOff %></th>
                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><%# Eval("Title") %></td>
                    <td><%# Eval("Url") %></td>
                    <td>
                        <asp:Button runat="server" ID="AwesomiumUrlOnOffButton" Text='<%# (bool)Eval("IsOn") == true ? WebApp.DisplayString.On : WebApp.DisplayString.Off %>'
                            CommandName="AwesomiumUrlOnOff" CommandArgument='<%# Eval("EventId") %>' OnCommand="AwesomiumUrlOnOff_OnCommand" />
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>

</asp:Content>
