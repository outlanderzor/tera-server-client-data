﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="AwesomiumUrlControl.aspx.cs" Inherits="WebApp.ContentsControl.AwesomiumUrlControl" %>

<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form runat="server">
        <asp:Label runat="server" Text="<%# WebApp.DisplayString.Server %>" />
        <asp:DropDownList runat="server" ID="ServerListDDL" DataTextField="ServerName" DataValueField="ServerNo" />
        <asp:Button runat="server" Text="<%# WebApp.DisplayString.ServerChoice %>" OnClick="ServerChoice_OnClick" />

        <br /> <br />

        <asp:Button runat="server" ID="InsertAwesomiumUrlButton" Text="<%# WebApp.DisplayString.InsertItem %>" OnClick="InsertAwesomiumUrlButton_OnClick" />

        <br /> <br />

        <table class="lineTable">
            <asp:Repeater runat="server" ID="AwesomiumUrls">
                <HeaderTemplate>
                    <tr>
                        <th><%# WebApp.DisplayString.ServerName %></th>
                        <th><%# WebApp.DisplayString.Title %></th>
                        <th><%# WebApp.DisplayString.Url %></th>
                        <th><%# WebApp.DisplayString.DeleteItem %></th>
                    </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><%# Eval("ServerName") %></td>
                        <td><%# Eval("Title") %></td>
                        <td><%# Eval("Url") %></td>
                        <td>
                            <asp:Button runat="server" ID="DeleteAwesomiumButton" Text="<%# WebApp.DisplayString.DeleteItem %>"
                                CommandName="DeleteAwesomium" CommandArgument='<%# Eval("ServerNo") + "|" + Eval("EventId") %>' OnCommand="DeleteAwesomiumButton_OnCommand" />
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>

    </form>

</asp:Content>
