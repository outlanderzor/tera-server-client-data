﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="AwesomiumOnOff.aspx.cs" Inherits="WebApp.ContentsControl.AwesomiumOnOff" %>

<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form runat="server">
        <asp:Label runat="server" Text="<%# WebApp.DisplayString.Server %>" />
        <asp:DropDownList runat="server" ID="ServerListDDL" DataTextField="Text" DataValueField="Value" />
        <asp:Button runat="server" Text="<%# WebApp.DisplayString.ServerChoice %>" OnClick="ServerChoice_OnClick" />

        <br /> <br />

        <table class="lineTable">
            <asp:Repeater runat="server" ID="AwesomiumInfos">
                <HeaderTemplate>
                    <tr>
                        <th><%# WebApp.DisplayString.ServerName %></th>
                        <th><%# WebApp.DisplayString.OnOff %></th>
                        <th><%# WebApp.DisplayString.Title %></th>
                        <th><%# WebApp.DisplayString.Url %></th>
                        <th><%# WebApp.DisplayString.DoControl %></th>
                    </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><%# Eval("ServerName") %></td>
                        <td>
                            <asp:Button runat="server" ID="ConverterOnOff" Text='<%# (bool)Eval("IsConverterOn") ? "On" : "Off" %>' 
                                CommandName="ConverterOnOff" CommandArgument='<%# Eval("ServerNo") + "|" + ((bool)Eval("IsConverterOn") ? "Off" : "On") %>' OnCommand="ConverterOnOff_OnCommand" />
                        </td>
                        <td><%# Eval("Title") %></td>
                        <td><%# Eval("Url") %></td>
                        <td>
                            <asp:Button runat="server" ID="ActivateAwesomiumUrlButton" Text="<%# WebApp.DisplayString.DoControl %>"
                                CommandName="ActivateAwesomiumUrl" CommandArgument='<%# Eval("ServerNo") %>' OnCommand="ActivateAwesomiumUrl_OnCommand" />
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </form>

</asp:Content>
