﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/AppCode/DataSetAskDialog.master" CodeBehind="ATO_GmEventAddEvent.aspx.cs" Inherits="WebApp.ContentsControl.GmEvent.ATO_GmEventAddEvent" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
	<div class="ato-serverlist">
		<label><%# WebApp.DisplayString.CheckServer %></label>
		<ul>
			<asp:Repeater runat="server" ID="ServerCheckList">
				<ItemTemplate>
					<li><asp:CheckBox runat="server" ID="ServerName" Text='<%# Eval("ServerName") %>' /><asp:HiddenField runat="server" ID="ServerNo" Value='<%# Eval("ServerNo") %>' /></li>
				</ItemTemplate>
			</asp:Repeater>
		</ul>
		<label><%# WebApp.DisplayString.CheckAllServer %></label>
	</div>
	<div class="ato-main">
		<ul class="form-style">
			<li><label class="width100"><%# WebApp.DisplayString.EventList %></label><asp:DropDownList runat="server" ID="EventType" /></li>
			<li><label class="width100"><%# WebApp.DisplayString.EventId %></label><asp:TextBox runat="server" ID="EventId" AutoPostBack="true" OnTextChanged="EventId_TextChanged" placeholder="<%# WebApp.DisplayString.ErrorMsg_EnterNumberNotDuplicated %>"/></li>
			<li><label class="width100"><%# WebApp.DisplayString.EventName %></label><asp:TextBox runat="server" ID="EventName" /></li>
			<li><label class="width100"><%# WebApp.DisplayString.ChannelCount %></label><asp:DropDownList runat="server" ID="ChannelCount" /></li>
			<li><label class="width100"><%# WebApp.DisplayString.UserLimitPerChannel %></label><asp:TextBox runat="server" ID="MaxUserCount" /></li>
			<li><label class="width100"><%# WebApp.DisplayString.StartTime %></label><asp:TextBox runat="server" ID="StartTime" type="date" /></li>
			<li><label class="width100"><%# WebApp.DisplayString.WaitingTime %></label><asp:DropDownList runat="server" ID="WaitingSeconds" /></li>
			<li><label class="width100"><%# WebApp.DisplayString.LevelLimit %></label><asp:DropDownList runat="server" ID="MinLevel" /><span>~</span><asp:DropDownList runat="server" ID="MaxLevel" /></li>
			<li><label class="width100"><%# WebApp.DisplayString.AutoSystem %></label><asp:DropDownList runat="server" ID="AutoSystem" class="SelectAutoSystem" /></li>
			<li id="li-repeQuizStartRatio">
				<label class="width100"><%# WebApp.DisplayString.RepeQuizStartRatio %></label>
				<asp:TextBox runat="server" ID="RepeQuizStartRatio" />
				<span>값(r): (현재살아있는유저수)가 (시작유저수) * (r) 보다 작으면 패자부활 문제 시작</span>
			</li>
			<li style="display:none;"><label class="width100"><%# WebApp.DisplayString.SetReward %></label><asp:DropDownList runat="server" ID="PointRewardList" /></li>
		</ul>
	</div>
	<script type="text/javascript">
		$(function () {
			$('[type=date]').each(function(){
				$(this).appendDtpicker({ "dateOnly": false, "minuteInterval": 5 });
			})
			$('[type=date]').keypress(function () { return false; })
			$('[type=date]').keydown(function () { return false; })

			$('.SelectAutoSystem').change(function () {
				var li = $('#li-repeQuizStartRatio');
				if ($(this).val() == '0') { // 수동진행
					$(li).find('input').val(0);
					li.hide();
				} else {
					li.show();
				}
			})
		});
	</script>
</asp:Content>
