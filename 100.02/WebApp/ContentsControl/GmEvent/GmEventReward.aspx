﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="GmEventReward.aspx.cs" Inherits="WebApp.ContentsControl.GmEvent.GmEventReward" %>
<asp:Content ID="Content3" ContentPlaceHolderID="headerContentHolder" runat="server">
	<style type="text/css">
		div#controlButtons {
			margin: 5px 0;
		}

		div#controlButtons input[type=submit] {
			float: right;
		}
	</style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="mainContentHolder" runat="server">
	<form id="Form1" runat="server">
		<table class="lineTable">
			<tr>
				<th><%# WebApp.DisplayString.ServerChoice %></th>
				<th><%# WebApp.DisplayString.RewardCategory %></th>
				<th><%# WebApp.DisplayString.AdminRewardId %></th>
				<th><%# WebApp.DisplayString.EventChoice %></th>
				<th><%# WebApp.DisplayString.Point %></th>
				<th><%# WebApp.DisplayString.Search %></th>
			</tr>
			<tr>
				<td><asp:DropDownList runat="server" ID="ServerSelect" DataSource="<%# WebApp.WebAdminDB.ServerListWithAll.Select(x => new { ServerName = x.mName, ServerNo = x.mNo }) %>" DataTextField="ServerName" DataValueField="ServerNo"/></td>
				<td><asp:DropDownList runat="server" ID="RewardCategory" DataTextField="Text" DataValueField="Value"/></td>
				<td><asp:DropDownList runat="server" ID="RewardIdList" DataTextField="Text" DataValueField="Value"/></td>
				<td><asp:DropDownList runat="server" ID="GmEventSelect" DataTextField="EventName" DataValueField="EventId" AutoPostBack="true" OnSelectedIndexChanged="GmEventSelect_SelectedIndexChanged"/></td>
				<td><asp:DropDownList runat="server" ID="PointList" DataTextField="PointText" DataValueField="Point"/></td>
				<td><asp:Button runat="server" ID="SelectOption" Text="Search" OnClick="SelectOption_Click" /></td>
			</tr>
		</table>
		<div id="controlButtons">
			<asp:Button runat="server" ID="DeleteReward" CommandName="DeleteReward" OnCommand="DeleteReward_Command" Text="<%# WebApp.DisplayString.DeleteReward %>"/>
			<asp:Button runat="server" ID="AddReward" CommandName="AddReward" OnCommand="AddReward_Command" Text="<%# WebApp.DisplayString.AddReward %>"/>
		</div>
		<table class="lineTable width-full">
			<asp:Repeater runat="server" ID="GmEventRewardList">
				<HeaderTemplate>
					<tr>
						<th><asp:CheckBox runat="server" ID="RewardCheckAll" AutoPostBack="true" OnCheckedChanged="RewardCheckAll_CheckedChanged"/></th>
						<th><%# WebApp.DisplayString.ServerName %></th>
						<th><%# WebApp.DisplayString.RewardCategory %></th>
						<th><%# WebApp.DisplayString.AdminRewardId %></th>
						<th><%# WebApp.DisplayString.EventName %></th>
						<th><%# WebApp.DisplayString.Score %></th>
						<th><%# WebApp.DisplayString.RewardType %></th>
						<th><%# WebApp.DisplayString.RewardInfo %></th>
					</tr>
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
						<td><asp:CheckBox runat="server" ID="RewardCheck" /><asp:HiddenField runat="server" ID="RewardDbId" Value='<%# Eval("ServerNo") + " " + Eval("DbId") %>'/></td>
						<td><%# Eval("ServerName") %></td>
						<td><%# Eval("Category") %></td>
						<td><%# (int)Eval("RewardId") == 0 ? "" : Eval("RewardIdName") %></td>
						<td><%# (int)Eval("EventId") == 0 ? "" : Eval("EventName") %></td>
						<td><%# Eval("PointRange") %></td>
						<td><%# Eval("RewardTypeName") %></td>
						<td><%# Eval("RewardInfo") %></td>
					</tr>
				</ItemTemplate>
			</asp:Repeater>
		</table>
	</form>
</asp:Content>
