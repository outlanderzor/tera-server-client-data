<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/AppCode/DataSetAskDialog.master"CodeBehind="ATO_GmEventDelEvent.aspx.cs" Inherits="WebApp.ContentsControl.GmEvent.ATO_GmEventDelEvent" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
	<table class="lineTable width-full">
		<asp:Repeater runat="server" ID="GmEventList">
			<HeaderTemplate>
				<tr>
					<th><%# WebApp.DisplayString.ServerName %></th>
					<th><%# WebApp.DisplayString.EventId %></th>
					<th><%# WebApp.DisplayString.EventName %></th>
					<th><%# WebApp.DisplayString.StartTime %></th>
					<th><%# WebApp.DisplayString.WaitingTime %></th>
					<th><%# WebApp.DisplayString.EventType %></th>
					<th><%# WebApp.DisplayString.UserLimit %></th>
					<th><%# WebApp.DisplayString.LevelLimit %></th>
					<th><%# WebApp.DisplayString.Status %></th>
				</tr>
			</HeaderTemplate>
			<ItemTemplate>
				<tr>
					<td><%# Eval("ServerName") %></td>
					<td><%# Eval("EventId") %></td>
					<td><%# Eval("EventName") %></td>
					<td><%# Eval("StartTime") %></td>
					<td><%# Eval("WaitingMinutes") %></td>
					<td><%# Eval("EventType") %></td>
					<td><%# Eval("MaxUserCount") %></td>
					<td><%# Eval("MinLevel") + " ~ " + Eval("MaxLevel") %></td>
					<td><%# Eval("Status") %></td>
				</tr>
			</ItemTemplate>
		</asp:Repeater>
	</table>
</asp:Content>
