<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/AppCode/DataSetAskDialog.master" CodeBehind="ATO_GmEventChangeMaxJoinCount.aspx.cs" Inherits="WebApp.ContentsControl.GmEvent.ATO_GmEventChangeMaxJoinCount" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
	<table class="lineTable" style="margin-bottom: 20px;">
		<tr>
			<th><%# WebApp.DisplayString.ServerName %></th>
			<th><%# WebApp.DisplayString.EnterLimitCountFull %></th>
		</tr>
		<tr>
			<td><asp:Label runat="server" ID="ServerName"/></td>
			<td><asp:DropDownList runat="server" ID="MaxJoinCount" /></td>
		</tr>
	</table>
</asp:Content>
