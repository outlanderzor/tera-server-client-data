<%@ Page Language="C#" MasterPageFile="~/AppCode/CustomAskDialog.Master" AutoEventWireup="true" CodeBehind="ATO_GmEventAddRewardTemplate.aspx.cs" Inherits="WebApp.ContentsControl.GmEvent.ATO_GmEventAddRewardTemplate" %>
<asp:Content ID="ContentHeader" ContentPlaceHolderID="ContentHeader" runat="server">
	<style type="text/css">
		.li-rewardItem {
			display: none;
		}
	</style>

</asp:Content>

<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentBody" runat="server">
	<form id="Form2" runat="server" class="nolineTable">   
		<!-- 잘못 입력 된 값에 대한 안내 메세지  -->
		<p style="color:black; font-weight:bold"><asp:Label ID="ErrorInfo" runat="server" visible="false"/></p><br />
		
		<asp:Panel id="EditPanel" runat="server" visible="true">

		<div class="ato-main">
			<ul class="form-style">
				<li><label class="width100"><%# WebApp.DisplayString.RewardCategory %></label><asp:DropDownList runat="server" ID="RewardCategory" class="rewardCategoryList" /></li>
				<li><label class="width100"><%# WebApp.DisplayString.AdminRewardId %></label><asp:TextBox runat="server" ID="RewardId" type="number" /><asp:Label runat="server" ID="RewardIdDesc"/></li>
				<li class="li-rewardPoint"><label class="width100"><%# WebApp.DisplayString.Point %></label><asp:TextBox runat="server" ID="Point" type="number" /><asp:Label runat="server" ID="PointDesc"/></li>
				<li><label class="width100"><%# WebApp.DisplayString.RewardType %></label><asp:DropDownList runat="server" ID="RewardTypeList" class="rewardTypeList" DataValueField="Value" DataTextField="Text"/></li>
				<li class="li-rewardItem" id="li-itemTemplateId"><label class="width100"><%# WebApp.DisplayString.ItemTemplateId %></label><asp:TextBox runat="server" ID="ItemTemplateId" type="number" AutoPostBack="true" OnTextChanged="ItemTemplateId_TextChanged" /><asp:Label runat="server" ID="ItemName"/></li>
				<li class="li-rewardItem" id="li-itemAmount"><label class="width100"><%# WebApp.DisplayString.ItemAmount %></label><asp:TextBox runat="server" ID="ItemAmount" type="number" /></li>
				<li class="li-rewardItem" id="li-money"><label class="width100"><%# WebApp.DisplayString.Gold %></label><asp:TextBox runat="server" ID="Money" type="number"/></li>
				<li class="li-rewardItem" id="li-exp"><label class="width100"><%# WebApp.DisplayString.Exp %></label><asp:TextBox runat="server" ID="Exp" type="number"/></li>
				<li class="li-rewardItem" id="li-valkyon"><label class="width100"><%# WebApp.DisplayString.Exp %></label><asp:TextBox runat="server" ID="ValkyonPoint" type="number"/></li>
			</ul>

			<asp:Button runat="server" ID="AddReward" Text="<%# WebApp.DisplayString.AddReward %>" OnClick="AddReward_Click" style="margin-left: 100px;"/>

			<table class="lineTable">
				<asp:Repeater runat="server" ID="AddRewardList">
					<HeaderTemplate>
						<tr>
							<th><%# WebApp.DisplayString.RewardCategory %></th>
							<th><%# WebApp.DisplayString.AdminRewardId %></th>
							<th><%# WebApp.DisplayString.Point %></th>
							<th><%# WebApp.DisplayString.RewardType %></th>
							<th><%# WebApp.DisplayString.RewardInfo %></th>
							<th><%# WebApp.DisplayString.DeleteFromList %></th>
						</tr>
					</HeaderTemplate>
					<ItemTemplate>
						<tr>
							<td><%# Eval("RewardCategory") %></td>
							<td><%# Eval("RewardId") %></td>
							<td><%# Eval("Point") %></td>
							<td><%# Eval("RewardTypeName") %></td>
							<td><%# Eval("RewardInfo") %></td>
							<td><asp:Button runat="server" ID="DeleteReward" CommandArgument='<%# Eval("Index") %>' OnCommand="DeleteReward_Command" Text="<%# WebApp.DisplayString.DeleteFromList %>" /></td>
						</tr>
					</ItemTemplate>
				</asp:Repeater>
			</table>
		</div>
		
		<!-- 메모 입력란 -->
		<p style="font-weight:bold">[<%= WebApp.DisplayString.MemoStr %>]</p><br />
		<asp:TextBox runat="server" width="250px" height="100px" ID="MemoBox" TextMode="MultiLine" style="overflow:auto"/><br /><br />    

		<!-- 확인 <%# WebApp.DisplayString.Button %> -->
		<asp:Button runat="server" ID="ConfirmButton" Text="<%# WebApp.DisplayString.InsertItem %>" OnClick="ConfirmButton_Click" />
		</asp:Panel>
	
		<asp:Button runat="server" ID="CloseButton" Text="<%# WebApp.DisplayString.Close%>" Visible="false" OnClick="CloseButton_Click"/><br />
	</form>

	
	<script type="text/javascript">
		$(document).ready(function () {
			ShowRewardCategoryItem()
			ShowRewardTypeItem()
		})
		
		$('.rewardCategoryList').change(function () {
			ShowRewardCategoryItem()
		})

		$('.rewardTypeList').change(function () {
			ShowRewardTypeItem();
		})

		function ShowRewardCategoryItem()
		{
			$('.li-rewardPoint').hide()

			switch ($('.rewardCategoryList').val()) {
				case '0':
					break;
				case '1':
					break;
				case '2':
					$('.li-rewardPoint').show()
					break;
			}
		}

		function ShowRewardTypeItem()
		{
			$('.li-rewardItem').hide()

			switch ($('.rewardTypeList').val()) {
				case '0':
					break;
				case '1':
					$('#li-itemTemplateId').show()
					$('#li-itemAmount').show()
					break;
				case '2':
					$('#li-money').show()
					break;
				case '3':
					$('#li-exp').show()
					break;
				case '4':
					$('#li-valkyon').show()
					break;
			}
		}
	</script>

</asp:Content>
