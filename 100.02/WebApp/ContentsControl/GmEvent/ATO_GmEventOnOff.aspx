<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/AppCode/DataSetAskDialog.master" CodeBehind="ATO_GmEventOnOff.aspx.cs" Inherits="WebApp.ContentsControl.GmEvent.ATO_GmEventOnOff" %>
<asp:Content ID="Content0" ContentPlaceHolderID="ContentHeader" runat="server">
	<style type="text/css">
	</style>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
	<table class="lineTable" style="margin-bottom:20px;">
		<tr>
			<th><%# WebApp.DisplayString.ServerName %></th>
			<th><%# WebApp.DisplayString.OnOff %></th>
		</tr>
		<tr>
			<td><asp:Label runat="server" ID="ServerName"/></td>
			<td><asp:Label runat="server" ID="OnOff"/></td>
		</tr>
	</table>
	<script type="text/javascript">
		$(document).ready(function () {
			$('input#ContentBody_Submit').val('<%=this.Request["isOn"].ToLower()=="true" ? WebApp.DisplayString.EventON : WebApp.DisplayString.EventOFF %>')
		})
	</script>
</asp:Content>
