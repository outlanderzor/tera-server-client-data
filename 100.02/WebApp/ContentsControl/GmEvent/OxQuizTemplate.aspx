﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="OxQuizTemplate.aspx.cs" Inherits="WebApp.ContentsControl.GmEvent.OxQuizTemplate" %>
<asp:Content ID="Content3" ContentPlaceHolderID="headerContentHolder" runat="server">
	<style type="text/css">
		div#controlButtons {
			margin: 5px 0;
		}

		div#controlButtons input[type=submit] {
			float: right;
		}
	</style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="mainContentHolder" runat="server">
	<form id="Form1" runat="server">
		<div id="controlButtons">
			<asp:Button runat="server" ID="DeleteQuiz" CommandName="DeleteQuiz" OnCommand="DeleteQuiz_Command" Text="<%# WebApp.DisplayString.DeleteQuiz %>"/>
			<asp:Button runat="server" ID="AddQuiz" CommandName="AddQuiz" OnCommand="AddQuiz_Command" Text="<%# WebApp.DisplayString.InsertQuiz %>"/>
			<asp:Button runat="server" ID="ExportExcelQuizTemplate" Text="<%# WebApp.DisplayString.ExcelDownload %>" OnClick="ExportExcelQuizTemplate_Click" />
		</div>

		<table class="lineTable width-full">
			<asp:Repeater runat="server" ID="OxQuizList">
				<HeaderTemplate>
					<tr>
						<th><asp:CheckBox runat="server" ID="QuizCheckAll" AutoPostBack="true" OnCheckedChanged="QuizCheckAll_CheckedChanged"/></th>
						<th><%# WebApp.DisplayString.QuizString %></th>
						<th><%# WebApp.DisplayString.Answer %></th>
						<th><%# WebApp.DisplayString.AnswerDesc %></th>
					</tr>
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
						<td><asp:CheckBox runat="server" ID="QuizCheck" /><asp:HiddenField runat="server" ID="QuizDbId" Value='<%# Eval("DbId") %>'/></td>
						<td><%# Eval("QuizString") %></td>
						<td><%# Eval("AnswerOX") %></td>
						<td><%# Eval("AnswerString") %></td>
					</tr>
				</ItemTemplate>
			</asp:Repeater>
		</table>
	</form>
</asp:Content>
