﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/CustomAskDialog.Master" AutoEventWireup="true" CodeBehind="ATO_OxQuizAddQuiz.aspx.cs" Inherits="WebApp.ContentsControl.GmEvent.ATO_OxQuizAddQuiz" %>
<asp:Content ID="ContentHeader" ContentPlaceHolderID="ContentHeader" runat="server">
	<style type="text/css">
	</style>
</asp:Content>

<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentBody" runat="server">
	<form id="Form2" runat="server" class="nolineTable">   
		<!-- 잘못 입력 된 값에 대한 안내 메세지  -->
		<p style="color:black; font-weight:bold"><asp:Label ID="ErrorInfo" runat="server" visible="false" style="color: red;"/></p><br />
		<p style="color:black; font-weight:bold"><asp:Label ID="ResultInfo" runat="server" visible="false" /></p><br />
		
		<asp:Panel id="EditPanel" runat="server" visible="true">
			<div class="ato-main">
				<span style="display: block; font-weight: bolder; margin-bottom: 10px;">[<%# WebApp.DisplayString.UploadOxQuizExcel %>]</span>
				<a href="OxQuizTemplate.xlsx" target="_blank" style="display: block; margin-bottom: 10px; text-decoration: underline;">Download template file</a>
				<asp:FileUpload runat="server" ID="QuizFile"/>
				<asp:Button runat="server" ID="UploadQuiz" OnClick="UploadQuiz_Click" Text="<%# WebApp.DisplayString.Upload %>"/>
			</div>

			<div class="ato-main">
				<span style="display: block; font-weight: bolder; margin-bottom: 10px;">[<%# WebApp.DisplayString.InsertQuizOnce %>]</span>
				<ul class="form-style">
					<li><label class="width100"><%# WebApp.DisplayString.QuizString %></label><asp:TextBox runat="server" ID="TextQuiz" style="width: 300px;"/></li>
					<li><label class="width100"><%# WebApp.DisplayString.Answer %></label><asp:DropDownList runat="server" ID="AnswerList" style="width: 100px;"/></li>
					<li><label class="width100"><%# WebApp.DisplayString.QuizString %>설명</label><asp:TextBox runat="server" ID="TextAnswerString" style="width: 300px;"/></li>
				</ul>

				<asp:Button runat="server" ID="AddQuiz" OnClick="AddQuiz_Click" Text="<%# WebApp.DisplayString.InsertQuiz %>"/>
			</div>

			<div class="ato-main">
				<table class="lineTable">
					<asp:Repeater runat="server" ID="QuizList">
						<HeaderTemplate>
							<tr>
								<th><%# WebApp.DisplayString.QuizString %></th>
								<th><%# WebApp.DisplayString.Answer %></th>
								<th><%# WebApp.DisplayString.AnswerDesc %></th>
								<th><%# WebApp.DisplayString.Button %></th>
							</tr>
						</HeaderTemplate>
						<ItemTemplate>
							<tr>
								<td><%# Eval("QuizString") %></td>
								<td><%# Eval("Answer") %></td>
								<td><%# Eval("AnswerString") %></td>
								<td><asp:Button runat="server" ID="DeleteQuiz" CommandArgument='<%# Eval("Index") %>' OnCommand="DeleteQuiz_Command" Text="<%# WebApp.DisplayString.DeleteFromList %>" /></td>
							</tr>
						</ItemTemplate>
					</asp:Repeater>
				</table>
			</div>
			
			<!-- 메모 입력란 -->
			<p style="font-weight:bold">[<%= WebApp.DisplayString.MemoStr %>]</p><br />
			<asp:TextBox runat="server" width="250px" height="100px" ID="MemoBox" TextMode="MultiLine" style="overflow:auto"/><br /><br />    

			<!-- 확인 <%# WebApp.DisplayString.Button %> -->
			<asp:Button runat="server" ID="ConfirmButton" Text="<%# WebApp.DisplayString.InsertItem %>" OnClick="ConfirmButton_Click" />
		</asp:Panel>
	
		<asp:Button runat="server" ID="CloseButton" Text="<%# WebApp.DisplayString.Close%>" Visible="false" OnClick="CloseButton_Click"/><br />
	</form>
</asp:Content>