<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/AppCode/DataSetAskDialog.master" CodeBehind="ATO_GmEventDelReward.aspx.cs" Inherits="WebApp.ContentsControl.GmEvent.ATO_GmEventDelReward" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
	<table class="lineTable width-full">
		<asp:Repeater runat="server" ID="DeleteRewardList">
			<HeaderTemplate>
				<tr>
					<th><%# WebApp.DisplayString.ServerName %></th>
					<th><%# WebApp.DisplayString.RewardCategory %></th>
					<th><%# WebApp.DisplayString.AdminRewardId %></th>
					<th><%# WebApp.DisplayString.EventId %></th>
					<th><%# WebApp.DisplayString.EventName %></th>
					<th><%# WebApp.DisplayString.Score %></th>
					<th><%# WebApp.DisplayString.RewardType %></th>
					<th><%# WebApp.DisplayString.RewardInfo %></th>
				</tr>
			</HeaderTemplate>
			<ItemTemplate>
				<tr>
					<td><%# Eval("ServerName") %></td>
					<td><%# Eval("Category") %></td>
					<td><%# (int)Eval("RewardId") == 0 ? "" : Eval("RewardIdName") %></td>
					<td><%# (int)Eval("EventId") == 0 ? "" : Eval("EventId") %></td>
					<td><%# Eval("EventName") %></td>
					<td><%# Eval("PointRange") %></td>
					<td><%# Eval("RewardTypeName") %></td>
					<td><%# Eval("RewardInfo") %></td>
				</tr>
			</ItemTemplate>
		</asp:Repeater>
	</table>
</asp:Content>