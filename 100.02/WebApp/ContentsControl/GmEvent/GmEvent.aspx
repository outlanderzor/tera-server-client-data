﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="GmEvent.aspx.cs" Inherits="WebApp.ContentsControl.GmEvent.GmEvent" %>
<asp:Content ID="Content3" ContentPlaceHolderID="headerContentHolder" runat="server">
	<style type="text/css">
	</style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="mainContentHolder" runat="server">
	<form id="Form1" runat="server">
		<table class="lineTable" style="margin-bottom:10px;">
			<tr>
				<th><%# WebApp.DisplayString.ServerChoice %></th>
				<th><%# WebApp.DisplayString.Search %></th>
			</tr>
			<tr>
				<td><asp:DropDownList runat="server" ID="ServerSelect" DataSource="<%# WebApp.WebAdminDB.ServerListWithAll.Select(x => new { ServerName = x.mName, ServerNo = x.mNo }) %>" DataTextField="ServerName" DataValueField="ServerNo"/></td>
				<td><asp:Button runat="server" ID="SelectOption" Text="Search" OnClick="SelectOption_Click" /></td>
			</tr>
		</table>
		<table class="lineTable ">
			<asp:Repeater runat="server" ID="GmEventInfoList">
				<HeaderTemplate>
					<tr>
						<th><%# WebApp.DisplayString.ServerName %></th>
						<th><%# WebApp.DisplayString.OnOff %></th>
						<th><%# WebApp.DisplayString.EnterLimitCountFull %></th>
						<th><%# WebApp.DisplayString.Control %></th>
					</tr>
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
						<td><%# Eval("ServerName") %></td>
						<td><%# (bool)Eval("IsOn") ? "On" : "Off" %></td>
						<td><%# (int)Eval("MaxJoinCount") == 0 ? WebApp.DisplayString.UnLimited : Eval("MaxJoinCount").ToString() + WebApp.DisplayString.CountValue %></td>
						<%--<td><asp:button runat="server" ID="EventOnOff" Text="<%# Eval("OnOff") == "On" ? "Off" : "On" %>" CommandName="EventOnOff" CommandArgument='<%# Eval("ServerNo") + " " + Eval("EventId") + " " + (Eval("OnOff") == "On" ? "Off" : "On") %>' OnCommand=""/></td>--%>
						<td>
							<asp:button runat="server" ID="EventOnOff" Text='<%# (bool)Eval("IsOn") ? "Off" : "On" %>' CommandName="EventOnOff" CommandArgument='<%# Eval("ServerNo") + " " + ((bool)Eval("IsOn") ? "Off" : "On") %>' OnCommand="EventOnOff_Command"/>
							<asp:button runat="server" ID="EventJoinCount" Text='<%# WebApp.DisplayString.ChangeJoinLimitCount %>' CommandName="EventJoinCount" CommandArgument='<%# Eval("ServerNo") + " " + Eval("MaxJoinCount") %>' OnCommand="EventJoinCount_Command"/>
						</td>
					</tr>
				</ItemTemplate>
			</asp:Repeater>
		</table>
	</form>
</asp:Content>
