﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="GmEventOxGame.aspx.cs" Inherits="WebApp.ContentsControl.GmEvent.GmEventOxGame" %>
<asp:Content ID="Content3" ContentPlaceHolderID="headerContentHolder" runat="server">
	<style type="text/css">
		div#controlButtons {
			margin: 5px 0;
		}

		div#controlButtons input[type=submit] {
			float: right;
		}

		a.underline {
			text-decoration: underline;
		}

		.quizString {
			max-width: 400px;
		}
	</style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="mainContentHolder" runat="server">
	<form id="Form1" runat="server">
		<table class="lineTable">
			<tr>
				<th><%# WebApp.DisplayString.ServerChoice %></th>
				<th><%# WebApp.DisplayString.EventChoice %></th>
				<th><%# WebApp.DisplayString.Search %></th>
			</tr>
			<tr>
				<td><asp:DropDownList runat="server" ID="ServerSelect" DataSource="<%# WebApp.WebAdminDB.ServerListWithAll.Select(x => new { ServerName = x.mName, ServerNo = x.mNo }) %>" DataTextField="ServerName" DataValueField="ServerNo"/></td>
				<td><asp:DropDownList runat="server" ID="GmEventSelect" DataTextField="EventName" DataValueField="EventId" /></td>
				<td><asp:Button runat="server" ID="SelectOption" Text="Search" OnClick="SelectOption_Click" /></td>
			</tr>
		</table>

		<div id="controlButtons">
			<span><%# WebApp.DisplayString.InsertModifyDeleteActionIn %></span> <a class="underline" href="gmeventcontrol.aspx"><%# WebApp.DisplayString.NAVMENU_GmEventControl_Short %></a> <span><%# WebApp.DisplayString.ActionInPage %></span>
		</div>

		<table class="lineTable width-full">
			<asp:Repeater runat="server" ID="QuizList">
				<HeaderTemplate>
					<tr>
						<th><%# WebApp.DisplayString.ServerName %></th>
						<th><%# WebApp.DisplayString.EventName %></th>
						<th><%# WebApp.DisplayString.QuizCategory %></th>
						<th><%# WebApp.DisplayString.QuizIndex %></th>
						<th class="quizString"><%# WebApp.DisplayString.QuizString %></th>
						<th><%# WebApp.DisplayString.Answer %></th>
						<th class="quizString"><%# WebApp.DisplayString.Desc %></th>
						<th><%# WebApp.DisplayString.AdminRewardId %></th>
					</tr>
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
						<td><%# Eval("ServerName") %></td>
						<td><%# Eval("EventName") %></td>
						<td><%# Eval("QuizCategory") %></td>
						<td><%# Eval("QuizIndex") %></td>
						<td class="quizString"><%# Eval("QuizString") %></td>
						<td><%# Eval("AnswerOX") %></td>
						<td class="quizString"><%# Eval("AnswerString") %></td>
						<td><%# Eval("RewardId") %></td>
					</tr>
				</ItemTemplate>
			</asp:Repeater>
		</table>
	</form>
</asp:Content>
