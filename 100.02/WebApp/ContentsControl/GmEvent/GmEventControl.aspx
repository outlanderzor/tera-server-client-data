﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="GmEventControl.aspx.cs" Inherits="WebApp.ContentsControl.GmEvent.GmEventControl" %>
<asp:Content ID="Content3" ContentPlaceHolderID="headerContentHolder" runat="server">
	<style type="text/css">
		div#controlButtons input[type=submit] {
			float: right;
		}
		a.underline {
			text-decoration: underline;
		}
	    .auto-style2 {
            height: 27px;
        }
        .auto-style3 {
            height: 59px;
        }
	</style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="mainContentHolder" runat="server">
	<form id="Form1" runat="server">
		<table class="lineTable">
			<tr>
				<th><%# WebApp.DisplayString.ServerChoice %></th>
				<th><%# WebApp.DisplayString.EventChoice %></th>
				<th><%# WebApp.DisplayString.Search %></th>
			</tr>
			<tr>
				<td><asp:DropDownList runat="server" ID="ServerSelect" DataSource="<%# WebApp.WebAdminDB.ServerListWithAll.Select(x => new { ServerName = x.mName, ServerNo = x.mNo }) %>" DataTextField="ServerName" DataValueField="ServerNo"/></td>
				<td><asp:DropDownList runat="server" ID="GmEventSelect" DataTextField="EventName" DataValueField="EventId"/></td>
				<%--<td><asp:DropDownList runat="server" ID="PointList" DataTextField="PointText" DataValueField="Point"/></td>--%>
				<td><asp:Button runat="server" ID="SelectOption" Text="Search" OnClick="SelectOption_Click" /></td>
			</tr>
		</table>
		<div id="controlButtons">
			<asp:Button runat="server" ID="DeleteEvent" CommandName="DeleteEvent" OnCommand="DeleteEvent_Command" Text="<%# WebApp.DisplayString.DeleteEvent %>"/>
			<asp:Button runat="server" ID="AddEvent" CommandName="AddEvent" OnCommand="AddEvent_Command" Text="<%# WebApp.DisplayString.AddEvent %>"/>
		</div>
		<table class="lineTable width-full">
			<asp:Repeater runat="server" ID="GmEventList" OnItemDataBound="GmEventList_ItemDataBound">
				<HeaderTemplate>
					<tr>
						<th><asp:CheckBox runat="server" ID="ServerCheckAll" AutoPostBack="true" OnCheckedChanged="ServerCheckAll_CheckedChanged"/></th>
						<th><%# WebApp.DisplayString.ServerName %></th>
						<th><%# WebApp.DisplayString.EventId %></th>
						<th><%# WebApp.DisplayString.EventName %></th>
						<th><%# WebApp.DisplayString.StartTime %></th>
						<th><%# WebApp.DisplayString.WaitingTime %></th>
						<th><%# WebApp.DisplayString.EventType %></th>
						<th><%# WebApp.DisplayString.AutoSystem%></th>
						<th><%# WebApp.DisplayString.ChannelCount %></th>
						<th><%# WebApp.DisplayString.UserLimitPerChannel %></th>
						<th><%# WebApp.DisplayString.LevelLimit %></th>
						<th><%# WebApp.DisplayString.Status %></th>
						<th><%# WebApp.DisplayString.RewardInfo %></th>
						<th><%# WebApp.DisplayString.Control %></th>
					</tr>
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
						<td><asp:CheckBox runat="server" ID="ServerCheck" /><asp:HiddenField runat="server" ID="EventId" Value='<%# Eval("ServerNo") + " " + Eval("EventId") %>'/></td>
						<td><%# Eval("ServerName") %></td>
						<td><%# Eval("EventId") %></td>
						<td><%# Eval("EventName") %></td>
						<td><%# Eval("StartTime") %></td>
						<td><%# Eval("WaitingMinutes") %></td>
						<td><%# Eval("EventType") %></td>
						<td><%# Eval("AutoSystem") %></td>
						<td><%# Eval("ChannelCount") %></td>
						<td><%# Eval("MaxUserCount") %></td>
						<td><%# Eval("MinLevel") + " ~ " + Eval("MaxLevel") %></td>
						<td><%# Eval("Status") %></td>
						<td><a class="underline" href='/ContentsControl/GmEvent/GmEventReward.aspx?serverno=<%# Eval("ServerNo") %>&eventid=<%# Eval("EventId") %>'><%# WebApp.DisplayString.NAVMENU_GmEventReward_Short %></a></td>
						<td>
							<asp:Button runat="server" ID="ManageEvent" Text="<%# WebApp.DisplayString.EditEvent %>" CommandArgument='<%# Eval("ServerNo") + " " + Eval("EventId") %>' OnCommand="ManageEvent_Command" />
							<asp:Button runat="server" ID="ManageGame" CommandArgument='<%# Eval("ServerNo") + " " + Eval("EventId") %>' OnCommand="ManageGame_Command" />
						</td>
					</tr>
				</ItemTemplate>
			</asp:Repeater>
		</table>
	</form>
</asp:Content>
