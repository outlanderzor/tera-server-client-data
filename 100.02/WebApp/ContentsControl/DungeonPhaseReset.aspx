﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="DungeonPhaseReset.aspx.cs" Inherits="WebApp.ContentsControl.DungeonPhaseReset" %>
<asp:Content ID="Content3" ContentPlaceHolderID="headerContentHolder" runat="server">
	<style type="text/css">
		ul.conditions > li {
			margin-bottom: 10px;
		}
	</style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="mainContentHolder" runat="server">
	<form id="Form1" runat="server">
		<ul class="conditions">
			<li> <!-- 서버 목록 -->
				<span>서버</span>
				<asp:DropDownList runat="server" ID="ServerList" DataSource="<%# WebApp.WebAdminDB.ServerListWithAll.Select(x => new { ServerName = x.mName, ServerNo = x.mNo }) %>" DataTextField="ServerName" DataValueField="ServerNo"/>
				<asp:Button runat="server" ID="SelectServer" Text="Search" OnClick="SelectServer_Click"/>
			</li>
			<li> <!-- 던전 목록 -->
				<span>던전</span>
				<asp:DropDownList runat="server" ID="DungeonList" DataSource="<%# WebApp.DungeonDatasheet.DungeonPhaseDungeonListWithAll.Select(x => new { DungeonName = x.name, ContinentId = x.continentId }) %>" DataTextField="DungeonName" DataValueField="ContinentId" />
				<asp:button runat="server" ID="SelectDungeon" Text="Select" OnClick="SelectDungeon_Click"/>
			</li>
		</ul>
		<table class="lineTable">
			<asp:Repeater runat="server" ID="DungeonPhaseDungeonList">
				<HeaderTemplate>
					<tr>
						<th><%# WebApp.DisplayString.ServerName %></th>
						<th><%# WebApp.DisplayString.Dungeon %></th>
						<th><%# WebApp.DisplayString.DungeonPhaseResetShort %></th>
					</tr>
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
						<td><%# Eval("ServerName") %></td>
						<td><%# Eval("DungeonName") %></td>
						<td><asp:button runat="server" ID="ResetPhase" Text="<%# WebApp.DisplayString.Reset %>" CommandName="ResetPhase" CommandArgument='<%# Eval("ServerNo") + " " + Eval("ContinentId") %>' OnCommand="ResetPhase_Command"/></td>
					</tr>
				</ItemTemplate>
			</asp:Repeater>
		</table>
	</form>
</asp:Content>
