﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_StoreBuyLimitResetReservation.aspx.cs" Inherits="WebApp.ContentsControl.StoreBuyLimit.ATO_StoreBuyLimitResetReservation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TERA_ATO" runat="server">
    <div class="ato-serverlist">
		<label><%# WebApp.DisplayString.CheckServer %></label>
		<ul>
			<asp:Repeater runat="server" ID="ServerCheckList">
				<ItemTemplate>
					<li><asp:CheckBox runat="server" ID="ServerCheckBox" Text='<%# Eval("ServerName") %>' /><asp:HiddenField runat="server" ID="ServerPlanetId" Value='<%# Eval("ServerNo") %>' /></li>
				</ItemTemplate>
			</asp:Repeater>
		</ul>
		<label><%# WebApp.DisplayString.CheckAllServer %></label>
	</div>
     <div class="ato-main">
		<ul class="form-style">
			<li>
                <label class="width150"><%# WebApp.DisplayString.StoreBuyLimitResetTime %></label>
			    <asp:TextBox runat="server" ID="EndTime" type="date" />
			</li>
            <li>
                <asp:Label runat="server" ID="menuLabel" Text="<%# WebApp.DisplayString.StoreMenuId%>" />
                <asp:DropDownList runat="server" ID="StoreMenuDDL" DataTextField="MenuName" DataValueField="StoreMenuId"/>
            </li>
        </ul>
    </div>
    <script type="text/javascript">
        $(function () {
            $('[type=date]').each(function () {
                $(this).appendDtpicker({ "dateOnly": false, "minuteInterval": 1 });
            })
            $('[type=date]').keypress(function () { return false; })
            $('[type=date]').keydown(function () { return false; })
        });
	</script>
</asp:Content>
