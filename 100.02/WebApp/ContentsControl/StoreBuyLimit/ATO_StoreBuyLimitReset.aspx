﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_StoreBuyLimitReset.aspx.cs" Inherits="WebApp.ContentsControl.StoreBuyLimit.ATO_StoreBuyLimitReset" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TERA_ATO" runat="server">
     <div class="ato-serverlist">
		<label><%# WebApp.DisplayString.CheckServer %></label>
		<ul>
			<asp:Repeater runat="server" ID="ServerCheckList">
				<ItemTemplate>
					<li><asp:CheckBox runat="server" ID="ServerCheckBox" Text='<%# Eval("ServerName") %>' /><asp:HiddenField runat="server" ID="ServerPlanetId" Value='<%# Eval("ServerNo") %>' /></li>
				</ItemTemplate>
			</asp:Repeater>
		</ul>
		<label><%# WebApp.DisplayString.CheckAllServer %></label>
	</div>
    <div class="ato-main">
		<ul class="form-style">
            <asp:Label runat="server" ID="menuLabel" Text="<%# WebApp.DisplayString.StoreMenuId%>" />
            <asp:DropDownList runat="server" ID="StoreMenuDDL" DataTextField="MenuName" DataValueField="StoreMenuId"/>
            <asp:Button runat="server" ID="SelectButton" Text="<%# WebApp.DisplayString.StoreBuyLimitResetSaveBtnStr%>" OnClick="SelectButton_Click" />
        </ul>
    </div>
</asp:Content>
