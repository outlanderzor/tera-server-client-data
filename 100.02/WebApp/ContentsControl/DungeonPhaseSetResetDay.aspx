﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="DungeonPhaseSetResetDay.aspx.cs" Inherits="WebApp.ContentsControl.DungeonPhaseSetResetDay" %>
<asp:Content ID="Content3" ContentPlaceHolderID="headerContentHolder" runat="server">
	<style type="text/css">
		ul.conditions > li {
			margin-bottom: 10px;
		}

		input.reset-dayhour {
			margin: 10px 0;
		}
	</style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="mainContentHolder" runat="server">
	<form id="Form1" runat="server">
		<ul class="conditions">
			<li> <!-- 서버 목록 -->
				<span><%# WebApp.DisplayString.ServerName %></span>
				<asp:DropDownList runat="server" ID="ServerList" DataSource="<%# WebApp.WebAdminDB.ServerListWithAll.Select(x => new { ServerName = x.mName, ServerNo = x.mNo }) %>" DataTextField="ServerName" DataValueField="ServerNo"/>
				<asp:Button runat="server" ID="SelectServer" Text="Search" OnClick="SelectServer_Click"/>
			</li>
		</ul>

		<asp:button CssClass="reset-dayhour" runat="server" ID="ResetDayHour" Text="<%# WebApp.DisplayString.DungeonPhaseSetResetDayHour %>" CommandName="ChangeResetDay" CommandArgument='' OnCommand="ResetDayHour_Command"/>

		<table class="lineTable">
			<asp:Repeater runat="server" ID="DungeonPhaseDungeonList">
				<HeaderTemplate>
					<tr>
						<th><%# WebApp.DisplayString.ServerName %></th>
						<th><%# WebApp.DisplayString.ResetDay %></th>
						<th><%# WebApp.DisplayString.ResetHour %></th>
					</tr>
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
						<td><%# Eval("ServerName") %></td>
						<td><%# Eval("ResetDayStr") %></td>
						<td><%# Eval("ResetHour") %></td>
					</tr>
				</ItemTemplate>
			</asp:Repeater>
		</table>
	</form>
</asp:Content>
