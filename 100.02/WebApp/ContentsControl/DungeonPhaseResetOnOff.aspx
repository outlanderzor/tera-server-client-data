﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="DungeonPhaseResetOnOff.aspx.cs" Inherits="WebApp.ContentsControl.DungeonPhaseResetOnOff" %>
<asp:Content ID="Content3" ContentPlaceHolderID="headerContentHolder" runat="server">
	<style type="text/css">
		ul.conditions > li {
			margin-bottom: 10px;
		}
	</style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="mainContentHolder" runat="server">
	<form id="Form1" runat="server">
		<ul class="conditions">
			<li> <!-- 서버 목록 -->
				<span>서버</span>
				<asp:DropDownList runat="server" ID="ServerList" DataSource="<%# WebApp.WebAdminDB.ServerListWithAll.Select(x => new { ServerName = x.mName, ServerNo = x.mNo }) %>" DataTextField="ServerName" DataValueField="ServerNo"/>
				<asp:Button runat="server" ID="SelectServer" Text="Search" OnClick="SelectServer_Click"/>
			</li>
		</ul>
		<table class="lineTable">
			<asp:Repeater runat="server" ID="DungeonPhaseDungeonList">
				<HeaderTemplate>
					<tr>
						<th><%# WebApp.DisplayString.ServerName %></th>
						<th><%# WebApp.DisplayString.DungeonPhaseSave %></th>
						<th><%# WebApp.DisplayString.Modify %></th>
					</tr>
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
						<td><%# Eval("ServerName") %></td>
						<td><%# Eval("ResetFlagStr") %></td>
						<td><asp:button runat="server" ID="UpdateResetFlag"
								Text='<%# (Eval("ResetFlagStr") == "On" ? "Off" : "On") %>' 
								CommandArgument='<%# Eval("ServerNo") + " " + (Eval("ResetFlagStr") == "On" ? "Off" : "On") %>' 
								OnCommand="UpdateResetFlag_Command"/></td>
					</tr>
				</ItemTemplate>
			</asp:Repeater>
		</table>
	</form>
</asp:Content>
