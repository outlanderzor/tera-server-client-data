﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_CityWarChangeMapSize.aspx.cs" Inherits="WebApp.ContentsControl.ATO_CityWarChangeMapSize" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
    <p style="font-weight:bold"><%# WebApp.DisplayString.CurrentCityWarMapState %> </p>
    <table class="lineTable" style="margin-bottom: 10px;">
        <tr>
            <th><%# WebApp.DisplayString.ServerName %></th>
            <th><%# WebApp.DisplayString.CityWarLeagueId %></th>
            <th><%# WebApp.DisplayString.CityWarMapStateId %></th>
        </tr>
        <tr>
            <td><asp:Label runat="server" ID="CurServerName"/> </td>
            <td><asp:Label runat="server" ID="CurLeagueId"/> </td>
            <td><asp:Label runat="server" ID="CurStateId"/></td>
        </tr>
    </table>
    <br />
    <br />
    <p style="font-weight:bold"><%# WebApp.DisplayString.CHANGE_CITYWAR_MAPSIZE %> </p>
    <table class="lineTable" style="margin-bottom: 10px;">
        <tr>
            <th><%# WebApp.DisplayString.ServerName %></th>
            <th><%# WebApp.DisplayString.CityWarLeagueId %></th>
            <th><%# WebApp.DisplayString.CityWarMapStateId %></th>
        </tr>
        <tr>
            <td><asp:Label runat="server" ID="NewServerName"/> </td>
            <td><asp:Label runat="server" ID="NewLeagueId"/> </td>
            <td><asp:DropDownList runat="server" ID="StateDDL" DataTextField="Text" DataValueField="Value" OnSelectedIndexChanged="StateDDL_SelectedIndexChanged"/></td>
        </tr>
    </table>
</asp:Content>
