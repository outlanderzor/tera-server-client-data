﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="StatueControl.aspx.cs" Inherits="WebApp.ContentsControl.Statue.StatueControl" %>


<asp:Content ID="Content1" ContentPlaceHolderID="headerContentHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">
    <form id="Form1" runat="server">
	<table class="lineTable" style="margin-bottom:10px;">
		<tr>
			<th><%# WebApp.DisplayString.ServerChoice %></th>
			<th><%# WebApp.DisplayString.Search %></th>
		</tr>
		<tr>
			<td><asp:DropDownList runat="server" ID="ServerSelect" DataSource="<%# WebApp.WebAdminDB.ServerListWithAll.Select(x => new { ServerName = x.mName, ServerNo = x.mNo }) %>" DataTextField="ServerName" DataValueField="ServerNo"/></td>
			<td><asp:Button runat="server" ID="SelectOption" Text="Search" OnClick="SelectOption_Click" /></td>
		</tr>
	</table>
	<table class="lineTable ">
		<asp:Repeater runat="server" ID="OnOffStateList">
			<HeaderTemplate>
				<tr>
					<th><%# WebApp.DisplayString.ServerName %></th>
				</tr>
			</HeaderTemplate>
			<ItemTemplate>
				<tr>
					<td><%# Eval("ServerName") %></td>
					<td><%# (bool)Eval("IsConverterOn") ? "On" : "Off" %></td>
					<td>
						<asp:button runat="server" ID="ConverterOnOff" Text='<%# (bool)Eval("IsConverterOn") ? "Off" : "On" %>' CommandName="ConverterOnOff" CommandArgument='<%# Eval("ServerNo") + " " + ((bool)Eval("IsConverterOn") ? "Off" : "On") %>' OnCommand="ConverterOnOff_Command"/>
					</td>
				</tr>
			</ItemTemplate>
		</asp:Repeater>
	</table>
</form>
</asp:Content>