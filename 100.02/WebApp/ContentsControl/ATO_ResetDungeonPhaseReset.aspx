﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/AppCode/DataSetAskDialog.master" CodeBehind="ATO_ResetDungeonPhaseReset.aspx.cs" Inherits="WebApp.ContentsControl.ATO_ResetDungeonPhaseReset" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
	<table>
		<tr>
			<th><%# WebApp.DisplayString.ServerName %></th>
			<th><%# WebApp.DisplayString.DungeonName %></th>
		</tr>
		<tr>
			<td><asp:Label runat="server" ID="ServerName"/></td>
			<td><asp:Label runat="server" ID="DungeonName"/></td>
		</tr>
	</table>
</asp:Content>
