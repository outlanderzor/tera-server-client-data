﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="DungeonWorkMedalStoreControl.aspx.cs" Inherits="WebApp.ContentsControl.DungeonWorkMedalStoreControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form id="Form2" runat="server" class="nolineTable">
    
        <table class="nolineTable">
            <tr>
                <td> <%= WebApp.DisplayString.ServerChoice %> </td>
                <td> <asp:DropDownList runat="server" ID="ServNoDDL" DataTextField="Text" DataValueField="Value" /> </td>
                <td> <asp:Button runat="server" ID="SearchButton" OnClick="SearchButton_Click" /> </td>
                <td> &nbsp; </td>
            </tr>
        </table>
        
        <asp:Panel runat="server" ID="OnOffBtnPanel" Visible="false">
        </asp:Panel>

        <asp:Panel runat="server" ID="ResultPanel" Visible="false">
        </asp:Panel>
    </form>
</asp:Content>