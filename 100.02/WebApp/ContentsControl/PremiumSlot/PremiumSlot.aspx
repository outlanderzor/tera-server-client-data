﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="PremiumSlot.aspx.cs" Inherits="WebApp.ContentsControl.PremiumSlot" %>

<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form runat="server">
        <table class="lineTable" style="margin-bottom:10px;">
		    <tr>
			    <th><%# WebApp.DisplayString.ServerChoice %></th>
			    <th><%# WebApp.DisplayString.Search %></th>
		    </tr>
		    <tr>
			    <td><asp:DropDownList runat="server" ID="ServerSelect" DataSource="<%# WebApp.WebAdminDB.ServerList.Select(x => new { ServerName = x.mName, ServerNo = x.mNo }) %>" DataTextField="ServerName" DataValueField="ServerNo"/></td>
			    <td><asp:Button runat="server" ID="SelectOption" Text="Search" OnClick="SelectOption_Click" /></td>
		    </tr>
	    </table>

        <br />

        <table class="lineTable">
            <asp:Repeater runat="server" ID="SlotReserveDataRepeater">
                <HeaderTemplate>
                    <tr>
                        <th><asp:CheckBox runat="server" ID="ReserveDataAllCheck" OnCheckedChanged="ReserveDataAllCheck_OnCheckedChanged" /></th>
                        <th><%# WebApp.DisplayString.ServerName %></th>
                        <th><%# WebApp.DisplayString.SlotSetId %></th>
                        <th><%# WebApp.DisplayString.SlotPos %></th>
                        <th><%# WebApp.DisplayString.LimitType %></th>
                        <th><%# WebApp.DisplayString.SlotType %></th>
                        <th><%# WebApp.DisplayString.IsCoolTimeAlwaysSpend %></th>
                        <th><%# WebApp.DisplayString.TemplateId2 %></th>
                        <th><%# WebApp.DisplayString.Amount %></th>
                        <th><%# WebApp.DisplayString.CoolTime_MilliSecond %></th>
                        <th><%# WebApp.DisplayString.GiveItemTemplateId %></th>
                        <th><%# WebApp.DisplayString.GiveItemAmount %></th>
                        <th><%# WebApp.DisplayString.StartTime %></th>
                    </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" ID="ReserveDataCheck" />
                            <asp:HiddenField runat="server" ID="ReserveDataHidden" Value='<%# Eval("ServerNo") + "|" + Eval("IdentityNo") %>' />
                        </td>
                        <td><%# Eval("ServerName") %></td>
                        <td><%# Eval("SlotSetId") %></td>
                        <td><%# Eval("SlotPos") %></td>
                        <td><%# Eval("LimitType") %></td>
                        <td><%# Eval("SlotType") %></td>
                        <td><%# (bool)Eval("IsCoolTimeAlwaysSpend") ? WebApp.DisplayString.On : WebApp.DisplayString.Off %></td>
                        <td><%# Eval("TemplateId") %></td>
                        <td><%# Eval("Amount") %></td>
                        <td><%# Eval("CoolTimeMilliSecond") %></td>
                        <td><%# Eval("GiveItemName") %></td>
                        <td><%# Eval("GiveItemAmount") %></td>
                        <td><%# Eval("StartTime") %></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>

        <asp:Button runat="server" ID="InsertSlotReserveDataButton" Text="<%# WebApp.DisplayString.InsertPremiumSlotReserveData %>" OnClick="InsertSlotReserveDataButton_OnClick" />
        <asp:Button runat="server" ID="DeleteSlotReserveDataButton" Text="<%# WebApp.DisplayString.DeletePremiumSlotReserveData %>" OnClick="DeleteSlotReserveDataButton_OnClick" />
        <asp:Button runat="server" ID="ResetPremiumSlot" Text="<%# WebApp.DisplayString.ResetPremiumSlot %>" OnClick="ResetPremiumSlot_OnClick" />
    </form>

</asp:Content>
