﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="DecoUIControl.aspx.cs" Inherits="WebApp.ContentsControl.DecoUIControl.DecoUIControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headerContentHolder" runat="server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">
      <form runat="server">
          <table class="lineTable" style="margin-bottom:10px;">
		    <tr>
			    <th><%# WebApp.DisplayString.DecoUI + WebApp.DisplayString.Type %></th>
			    <th><%# WebApp.DisplayString.Apply %></th>
		    </tr>
		    <tr>
                <td> <asp:DropDownList runat="server" ID="DecoSelect" DataTextField="Text" DataValueField="Value" /> </td>
			    <td><asp:Button runat="server" ID="SelectOption" Text="<%# WebApp.DisplayString.Apply %>" OnClick="SelectOption_Click" /></td>
		    </tr>
	    </table>
        <table class="lineTable">
            <asp:Repeater runat="server" ID="DecoUIDataRepeater">
                <HeaderTemplate>
                    <tr>
                        <th><asp:CheckBox runat="server" ID="DecoUIDataAllCheck" AutoPostBack="true" OnCheckedChanged="DecoUIDataAllCheck_OnCheckedChanged" /></th>
                        <th><%# WebApp.DisplayString.ServerName %></th>
                        <th><%# WebApp.DisplayString.DecoUI + WebApp.DisplayString.Type %></th>
                    </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" ID="ServerCheck" />
                        </td>
                        <td><%# Eval("ServerName") %></td>
                        <td><%# Eval("DecoUIType") %></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>        
    </form>
</asp:Content>