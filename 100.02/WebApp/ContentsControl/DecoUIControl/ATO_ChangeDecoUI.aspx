﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_ChangeDecoUI.aspx.cs" Inherits="WebApp.ContentsControl.DecoUIControl.ATO_ChangeDecoUI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TERA_ATO" runat="server">
     <table class="lineTable">
         <li id="warning-li">
				<label class="width100"></label>
				<label style="color: red; font-weight: bold;"><%# WebApp.DisplayString.SelectedUIAppliedEverywhere %></label>
			</li>
		<asp:Repeater runat="server" ID="DecoUIList">
			<HeaderTemplate>
				<tr>
					<th><%# WebApp.DisplayString.ServerName %></th>
                    <th><%# WebApp.DisplayString.AfterDecoUI %></th>
                    <th><%# WebApp.DisplayString.BeforeDecoUI %></th>
				</tr>
			</HeaderTemplate>
			<ItemTemplate>
				<tr>
					<td><%# Eval("ServerName") %></td>
   					<td><%# Eval("AfterDecoUI") %></td>
                    <td><%# Eval("BeforeDecoUI") %></td>
				</tr>
			</ItemTemplate>
		</asp:Repeater>
	</table>
</asp:Content>
