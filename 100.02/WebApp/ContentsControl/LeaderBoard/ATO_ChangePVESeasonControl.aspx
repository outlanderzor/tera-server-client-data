﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_ChangePVESeasonControl.aspx.cs" Inherits="WebApp.ContentsControl.LeaderBoard.ATO_ChangePVESeasonControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
    <table class="lineTable">
        <asp:Repeater runat="server" ID="ControlList">
            <HeaderTemplate>
                <tr>
                    <th><%# WebApp.DisplayString.ServerName %></th>
                    <th><%# WebApp.DisplayString.ResetPeriod %></th>
                    <th><%# WebApp.DisplayString.ResetDay %></th>
                    <th><%# WebApp.DisplayString.ResetHour %></th>
                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><%# Eval("ServerName") %></td>
                    <td><asp:Label runat="server" ID="ResetPeriod" Text='<%# Eval("resetPeriod") %>' /> <%# WebApp.DisplayString.Day %></td>
                    <td><asp:Label runat="server" ID="ResetDay" Text='<%# Eval("resetDayString") %>' /> </td>
                    <td><asp:Label runat="server" ID="ResetHour" Text='<%# Eval("resetHour") %>' /> <%# WebApp.DisplayString.Hour %></td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
    <br /><br />
    <table style="margin-top: 20px" class="lineTable">
        <!-- 초기화 주기(일단위) -->
        <tr>
            <td><%# WebApp.DisplayString.ResetPeriod %></td>
            <td><asp:TextBox runat="server" ID="ResetPeriod" style="ime-mode:disabled"/></td>
        </tr>
    </table>
    <table style="margin-top: 20px" class="lineTable">
        <!-- 초기화 요일 -->
        <tr>
            <td><%# WebApp.DisplayString.ResetDay %></td>
            <td><asp:DropDownList runat="server" ID="ResetDaySelect" DataValueField="Item1" DataTextField="Item2" /></td>
        </tr>
    </table>
    <table style="margin-top: 20px" class="lineTable">
        <!-- 초기화 시간 -->
        <tr>
            <td><%# WebApp.DisplayString.ResetHour %></td>
            <td><asp:DropDownList runat="server" ID="ResetHourSelect" DataValueField="Item1" DataTextField="Item2" /></td>
        </tr>
    </table>

</asp:Content>

