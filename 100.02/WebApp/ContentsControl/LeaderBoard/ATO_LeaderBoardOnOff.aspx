﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_LeaderBoardOnOff.aspx.cs" Inherits="WebApp.ContentsControl.LeaderBoard.ATO_LeaderBoardOnOff" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
	<table class="lineTable" style="margin-bottom:20px;">
		<tr>
			<th><%# WebApp.DisplayString.ServerName %></th>
			<th><%# "변경 후 " + WebApp.DisplayString.State %></th>
		</tr>
		<tr>
			<td><asp:Label runat="server" ID="ServerName"/></td>
			<td><asp:Label runat="server" ID="OnOff"/></td>
		</tr>
	</table>
	<script type="text/javascript">
	    $(document).ready(function () {
	        $('input#ContentBody_Submit').val('<%=this.Request["isOff"].ToLower()=="true" ? WebApp.DisplayString.Off : WebApp.DisplayString.On %>')
	    })
	</script>
</asp:Content>

