﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true"  CodeBehind="PacketCountChecker.aspx.cs" Inherits="WebApp.DetectBOT.PacketCountChecker" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headerContentHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">
	<form id="Form1" runat="server">
		<div id="controlButtons">
			<asp:Button runat="server" ID="AddSetting" CommandName="AddSetting" OnCommand="AddSetting_Command" Text="<%# WebApp.DisplayString.AddAndEdit %>"/>
		</div>
        <br />
		<table class="lineTable ">
			<asp:Repeater runat="server" ID="SettingList">
				<HeaderTemplate>
					<tr>
						<th><%# WebApp.DisplayString.ServerName %></th>
						<th><%# WebApp.DisplayString.OnOff %></th>
						<th><%# WebApp.DisplayString.CheckTermSeconds %></th>
                        <th><%# WebApp.DisplayString.CountLimit %></th>
                        <th><%# WebApp.DisplayString.KickUserByAdmin %></th>
					</tr>
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
						<td><%# Eval("ServerName") %></td>
                        <td><%# Eval("TurnOnStr") %></td>
						<td><%# Eval("CheckTime") %></td>
                        <td><%# Eval("CountThreshold") %></td>
                        <td><%# Eval("KickCheater") %></td>
					</tr>
				</ItemTemplate>
			</asp:Repeater>
		</table>
	</form>
</asp:Content>
