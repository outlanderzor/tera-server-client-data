﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="SkillCheater.aspx.cs" Inherits="WebApp.DetectBOT.SkillCheater.SkillCheater" %>
<%@ import Namespace="WebApp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headerContentHolder" runat="server">
	<style type="text/css">
		.form-obj {
			margin-top: 5px;
			margin-bottom: 5px;
		}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">
	<strong><%# DisplayString.NAVMENU_SkillCheaterConfig_Full %></strong>
	<form runat="server" id="skillCheaterForm">
	<div class="form-obj">
		<asp:Button runat="server" ID="SetConfig" Text="<%# DisplayString.AddAndEdit %>" OnClick="SetConfig_Click" />
	</div>
	<table class="lineTable form-obj">
		<asp:Repeater runat="server" ID="ConfigList">
			<HeaderTemplate>
				<tr>
					<th><%# DisplayString.ServerName %></th>
					<th><%# DisplayString.ServerType %></th>
					<th><%# DisplayString.OnOff %></th>
					<th><%# DisplayString.CheckTermSeconds %></th>
					<th><%# DisplayString.CountThreshold %></th>
					<th><%# DisplayString.KickUserByAdmin %></th>
					<th><%# DisplayString.SkipDamage %></th>
					<th><%# DisplayString.Modify %></th>
				</tr>
			</HeaderTemplate>
			<ItemTemplate>
				<tr>
					<td><%# Eval("ServerName") %></td>
					<td><%# Eval("ServerTypeName") %></td>
					<td><%# Eval("TurnOnStr") %></td>
					<td><%# Eval("CheckTermSeconds") %> sec</td>
					<td><%# Eval("CountThreshold") %></td>
					<td><%# Eval("KickCheater") %></td>
					<td><%# Eval("SkipDamage") %></td>
					<td><asp:Button runat="server" ID="ModifyConfig" Text="<%# DisplayString.Modify %>" CommandArgument='<%# Eval("ServerNo") + " " + Eval("ServerTypeNo") %>' OnCommand="ModifyConfig_Command" /></td>
				</tr>
			</ItemTemplate>
		</asp:Repeater>
	</table>
	</form>
</asp:Content>
