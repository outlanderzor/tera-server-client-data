﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_EditPacketCountChecker.aspx.cs" Inherits="WebApp.DetectBOT.ATO_EditPacketCountChecker" %>
<%@ Import Namespace="WebApp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TERA_ATO" runat="server">
	<div class="ato-main">
		<ul class="form-style">
			<li>
                <label class="width150"><%# DisplayString.ServerName %></label><asp:Label ID="ServerName" runat="server"></asp:Label>
            </li>
            <li>
                <label class="width150"><%# DisplayString.OnOff %></label>
				<asp:RadioButtonList ID="TurnOn" RepeatLayout="UnorderedList" class="radio-group" runat="server">
					<asp:ListItem Text="On" Value="True"/>
					<asp:ListItem Text="Off" Value="False" Selected="True"/>
				</asp:RadioButtonList>
			</li>
			<li><label class="width150"><%# DisplayString.CheckTermSeconds %></label><asp:TextBox ID="CheckTime" placeholder="" runat="server" /></li>
			<li><label class="width150"><%# DisplayString.CountLimit %></label><asp:TextBox ID="CountThreshold" style="width: 300px;" placeholder="" runat="server" /></li>
			<li><label class="width150"><%# DisplayString.KickUserByAdmin %></label>
				<asp:RadioButtonList ID="KickCheater" RepeatLayout="UnorderedList" class="radio-group" runat="server">
					<asp:ListItem Text="Enable" Value="True"/>
					<asp:ListItem Text="Disable" Value="False" Selected="True"/>
				</asp:RadioButtonList>
			</li>
		</ul>
	</div>
</asp:Content>
