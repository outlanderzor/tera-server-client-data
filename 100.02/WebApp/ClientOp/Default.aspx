﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApp.ClientOp.LoadingScreenControl" %>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">
    <form runat="Server" id="form">
        <table class="outerlineTable">
            <tr>
                <td style="padding-top:5px;">
                    <strong><%= WebApp.DisplayString.LoadingScreenControl%></strong>
                </td>
            </tr>
            <tr>
                <td>
                     <%= WebApp.DisplayString.ServerChoice %>
                     <asp:DropDownList runat="server" ID="ServNoDDL" DataTextField="Text" 
                         DataValueField="Value" onselectedindexchanged="Onselectedindexchanged" AutoPostBack="true" />
                </td>
            </tr>
            <tr>
                <td>
                    <%= WebApp.DisplayString.LoadingScreenControlStatus%>
                    <asp:Label runat="Server" ID="statusLbl"/>
                    <asp:Button runat="Server" ID="changeStatusBt" OnClick="OnClickChangeStatusBt"/>
                </td>
            </tr>
        </table>
    </form>
</asp:Content>
