﻿<%@ Page MasterPageFile="~/AppCode/WebAppPage.Master" Language="C#" AutoEventWireup="true" CodeBehind="Ranking.aspx.cs" Inherits="WebApp.BattleField.Ranking" %>
<%@ Register TagPrefix="webApp" TagName="SearchForm" Src="~/SearchForm.ascx" %>

<asp:Content ID="Content1" runat="server" 
    contentplaceholderid="mainContentHolder">

    <form runat="server" id ="resultForm" >
    
    <table class="nolineTable">
        <tr>
            <td> <%= WebApp.DisplayString.ServerChoice %> </td>
            <td> <asp:DropDownList runat="server" ID="ServNoDDL" DataTextField="Text" DataValueField="Value" /> </td>
        </tr>
        <tr>
            <td> <asp:Button runat="server" ID="SearchButton" OnClick="SearchButton_Click" /> </td>
        </tr>
    </table>
    <br />
    <table class="lineTable">        
        <tr><th>
        <%= WebApp.DisplayString.BattleFieldPvP %>
        </th></tr>
    </table>   
    
             
    <asp:GridView CssClass="lineTable" ID="pvpRanking" runat="server" AutoGenerateColumns="False" ShowHeader="true" ShowHeaderWhenEmpty="true">
    <Columns>
        <asp:TemplateField HeaderStyle-Width="40">
            <HeaderTemplate><%= WebApp.DisplayString.Ranking %></HeaderTemplate>
            <ItemTemplate><%# Eval("currRank")%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="100">
            <HeaderTemplate><%=WebApp.DisplayString.UserName %></HeaderTemplate>
            <ItemTemplate>                
                <a href='<%# string.Format("../users/default.aspx?{0}={1}&{2}={3}",
                    WebApp.ParamDefine.Common.ServerNo,ServNoDDL.SelectedValue,
                    WebApp.ParamDefine.User.UserDBId, Eval("userDbId"))%>'>
                    <%# Eval("userName") %>
                </a>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="100">
            <HeaderTemplate><%=WebApp.DisplayString.GuildName%></HeaderTemplate>
            <ItemTemplate><%# Eval("guildName")%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="60">
            <HeaderTemplate><%=WebApp.DisplayString.Race%></HeaderTemplate>
            <ItemTemplate><%# WebApp.CommonEnum.Race.ToString((int)Eval("userRace"))%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="60">
            <HeaderTemplate><%=WebApp.DisplayString.Class%></HeaderTemplate>
            <ItemTemplate><%# WebApp.CommonEnum.Class.ToString((int)Eval("userClass"))%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="150">
            <HeaderTemplate><%=WebApp.DisplayString.Kill%>/<%=WebApp.DisplayString.Death%></HeaderTemplate>
            <ItemTemplate><%# string.Format("{0}/{1}", Eval("countOfKill"), Eval("countOfDeath"))%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="60">
            <HeaderTemplate><%=WebApp.DisplayString.WinRate%></HeaderTemplate>
            <ItemTemplate><%# Eval("WinRate")%>%</ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="60">
            <HeaderTemplate><%=WebApp.DisplayString.PrivatePoint%></HeaderTemplate>
            <ItemTemplate><%# Eval("score")%></ItemTemplate>
        </asp:TemplateField>
    </Columns>
    </asp:GridView>
    <%=RoungPaging%>
    <br />
    <br />
    <table class="lineTable">        
        <tr><th>
        <%= WebApp.DisplayString.BattleFieldStronghold %>
        </th></tr>
    </table>            
    <asp:GridView CssClass="lineTable" ID="strongholdRanking" runat="server" AutoGenerateColumns="False" ShowHeader="true" ShowHeaderWhenEmpty="true">
    <Columns>
        <asp:TemplateField HeaderStyle-Width="40">
            <HeaderTemplate><%= WebApp.DisplayString.Ranking %></HeaderTemplate>
            <ItemTemplate><%# Eval("currRank")%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="100">
            <HeaderTemplate><%=WebApp.DisplayString.UserName %></HeaderTemplate>
            <ItemTemplate>                
                <a href='<%# string.Format("../users/default.aspx?{0}={1}&{2}={3}",
                    WebApp.ParamDefine.Common.ServerNo,ServNoDDL.SelectedValue,
                    WebApp.ParamDefine.User.UserDBId, Eval("userDbId"))%>'>
                    <%# Eval("userName") %>
                </a>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="100">
            <HeaderTemplate><%=WebApp.DisplayString.GuildName%></HeaderTemplate>
            <ItemTemplate><%# Eval("guildName")%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="60">
            <HeaderTemplate><%=WebApp.DisplayString.Race%></HeaderTemplate>
            <ItemTemplate><%# WebApp.CommonEnum.Race.ToString((int)Eval("userRace"))%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="60">
            <HeaderTemplate><%=WebApp.DisplayString.Class%></HeaderTemplate>
            <ItemTemplate><%# WebApp.CommonEnum.Class.ToString((int)Eval("userClass"))%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="150">
            <HeaderTemplate><%=WebApp.DisplayString.Kill%>/<%=WebApp.DisplayString.Death%>/<%=WebApp.DisplayString.Assist%></HeaderTemplate>
            <ItemTemplate><%# string.Format("{0}/{1}/{2}", Eval("countOfKill"), Eval("countOfDeath"), Eval("countOfAssist"))%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="60">
            <HeaderTemplate><%=WebApp.DisplayString.WinRate%></HeaderTemplate>
            <ItemTemplate><%# Eval("WinRate")%>%</ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="150">
            <HeaderTemplate><%=WebApp.DisplayString.OccupyStrongHold%></HeaderTemplate>
            <ItemTemplate><%# Eval("countOfAttackStronghold")%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="60">
            <HeaderTemplate><%=WebApp.DisplayString.TotalScore%></HeaderTemplate>
            <ItemTemplate><%# Eval("score")%></ItemTemplate>
        </asp:TemplateField>
    </Columns>
    </asp:GridView>    
    <%=StrongHoldPaging%>
    <br />
    <br />
    <table class="lineTable">        
        <tr><th>
        <%= WebApp.DisplayString.BattleFieldCannon %>
        </th></tr>
    </table>            
    <asp:GridView CssClass="lineTable" ID="cannonRanking" runat="server" AutoGenerateColumns="False" ShowHeader="true" ShowHeaderWhenEmpty="true">
    <Columns>
        <asp:TemplateField HeaderStyle-Width="40">
            <HeaderTemplate><%= WebApp.DisplayString.Ranking %></HeaderTemplate>
            <ItemTemplate><%# Eval("currRank")%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="100">
            <HeaderTemplate><%=WebApp.DisplayString.UserName %></HeaderTemplate>
            <ItemTemplate>                
                <a href='<%# string.Format("../users/default.aspx?{0}={1}&{2}={3}",
                    WebApp.ParamDefine.Common.ServerNo,ServNoDDL.SelectedValue,
                    WebApp.ParamDefine.User.UserDBId, Eval("userDbId"))%>'>
                    <%# Eval("userName") %>
                </a>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="100">
            <HeaderTemplate><%=WebApp.DisplayString.GuildName%></HeaderTemplate>
            <ItemTemplate><%# Eval("guildName")%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="60">
            <HeaderTemplate><%=WebApp.DisplayString.Race%></HeaderTemplate>
            <ItemTemplate><%# WebApp.CommonEnum.Race.ToString((int)Eval("userRace"))%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="60">
            <HeaderTemplate><%=WebApp.DisplayString.Class%></HeaderTemplate>
            <ItemTemplate><%# WebApp.CommonEnum.Class.ToString((int)Eval("userClass"))%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="150">
            <HeaderTemplate><%=WebApp.DisplayString.Kill%>/<%=WebApp.DisplayString.Death%>/<%=WebApp.DisplayString.Assist%></HeaderTemplate>
            <ItemTemplate><%# string.Format("{0}/{1}/{2}", Eval("countOfKill"), Eval("countOfDeath"), Eval("countOfAssist"))%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="60">
            <HeaderTemplate><%=WebApp.DisplayString.DestroyCount%></HeaderTemplate>
            <ItemTemplate><%# Eval("countOfDestroy")%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="60">
            <HeaderTemplate><%=WebApp.DisplayString.WinRate%></HeaderTemplate>
            <ItemTemplate><%# Eval("WinRate")%>%</ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="60">
            <HeaderTemplate><%=WebApp.DisplayString.TotalScore%></HeaderTemplate>
            <ItemTemplate><%# Eval("score")%></ItemTemplate>
        </asp:TemplateField>
    </Columns>
    </asp:GridView>    
    <%=CannonPaging%>
    <br />
    <br />
    <table class="lineTable">        
        <tr><th>
        <%= WebApp.DisplayString.BattleFieldBasePoint %>
        </th></tr>
    </table>            
    <asp:GridView CssClass="lineTable" ID="basePointRanking" runat="server" AutoGenerateColumns="False" ShowHeader="true" ShowHeaderWhenEmpty="true">
    <Columns>
        <asp:TemplateField HeaderStyle-Width="40">
            <HeaderTemplate><%= WebApp.DisplayString.Ranking %></HeaderTemplate>
            <ItemTemplate><%# Eval("currRank")%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="100">
            <HeaderTemplate><%=WebApp.DisplayString.UserName %></HeaderTemplate>
            <ItemTemplate>                
                <a href='<%# string.Format("../users/default.aspx?{0}={1}&{2}={3}",
                    WebApp.ParamDefine.Common.ServerNo,ServNoDDL.SelectedValue,
                    WebApp.ParamDefine.User.UserDBId, Eval("userDbId"))%>'>
                    <%# Eval("userName") %>
                </a>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="100">
            <HeaderTemplate><%=WebApp.DisplayString.GuildName%></HeaderTemplate>
            <ItemTemplate><%# Eval("guildName")%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="60">
            <HeaderTemplate><%=WebApp.DisplayString.Race%></HeaderTemplate>
            <ItemTemplate><%# WebApp.CommonEnum.Race.ToString((int)Eval("userRace"))%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="60">
            <HeaderTemplate><%=WebApp.DisplayString.Class%></HeaderTemplate>
            <ItemTemplate><%# WebApp.CommonEnum.Class.ToString((int)Eval("userClass"))%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="150">
            <HeaderTemplate><%=WebApp.DisplayString.Kill%>/<%=WebApp.DisplayString.Death%>/<%=WebApp.DisplayString.Assist%></HeaderTemplate>
            <ItemTemplate><%# string.Format("{0}/{1}/{2}", Eval("countOfKill"), Eval("countOfDeath"), Eval("countOfAssist"))%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="60">
            <HeaderTemplate><%=WebApp.DisplayString.WinRate%></HeaderTemplate>
            <ItemTemplate><%# Eval("WinRate")%>%</ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="150">
            <HeaderTemplate><%=WebApp.DisplayString.OccupyStrongHold%></HeaderTemplate>
            <ItemTemplate><%# Eval("countOfAttackStronghold")%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="60">
            <HeaderTemplate><%=WebApp.DisplayString.TotalScore%></HeaderTemplate>
            <ItemTemplate><%# Eval("score")%></ItemTemplate>
        </asp:TemplateField>
    </Columns>
    </asp:GridView>
    <%=BasePointPaging%>
    </form>

</asp:Content>


