﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="AwakenEnchantData.aspx.cs" Inherits="WebApp.Item.AwakenEnchantData" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form runat="server" id="form">
        <table>
        <tr>
            <td>
                <table>
                <tr>
                    <td> <%= WebApp.DisplayString.ServerChoice %> </td>
                    <td> <asp:DropDownList runat="server" ID="ServNoDDLForView" DataTextField="Text" DataValueField="Value" /> </td>
                    <td> <asp:Button runat="server" ID="SearchButton" OnClick="SearchButton_OnClick" /> </td>
                </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Panel runat="server" ID="ResultPanel">
                </asp:Panel>
            </td>
        </tr>
        </table>
        <br />
        <br />
        <table>
            <tr>
                <td> <asp:Button runat="server" ID="AddButton" OnClick="AddButton_OnClick" /> </td>
                <td> <asp:Button runat="server" ID="AddFileButton" OnClick="AddFileButton_OnClick" /> </td>
                <td> <asp:Panel runat="server" ID="ButtonPannel"> </asp:Panel> </td>
                <td> <asp:Button runat="server" ID="DeleteAllButton" OnClick="DeleteAllButton_OnClick" /> </td>
            </tr>
        </table>
    </form>

</asp:Content>