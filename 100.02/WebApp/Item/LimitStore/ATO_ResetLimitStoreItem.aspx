﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_ResetLimitStoreItem.aspx.cs" Inherits="WebApp.Item.LimitStore.ATO_ResetLimitStoreItem" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TERA_ATO" runat="server">
    <table class="lineTable">
        <tr>
            <td><asp:Label runat="server" ID="ResetLimitStoreInfo"/> </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Panel runat="server" ID="BuyMenuPanel">
                </asp:Panel>
                <asp:Table runat="server" CssClass="lineTable" ID="BuyMenuDataTable">
                    <asp:TableRow>
                        <asp:TableHeaderCell> <%# WebApp.DisplayString.ServerName %> </asp:TableHeaderCell>
                        <asp:TableHeaderCell> <%# WebApp.DisplayString.BuyMenuDataId %> </asp:TableHeaderCell>
                        <asp:TableHeaderCell> <%# WebApp.DisplayString.MenuId %> </asp:TableHeaderCell>
                    </asp:TableRow>
                </asp:Table>
            </td>
        </tr>
    </table>
</asp:Content>
