﻿<%@ Page MasterPageFile="~/AppCode/WebAppPage.Master" Language="C#" AutoEventWireup="true" CodeBehind="BuyMenuListManage.aspx.cs"  Inherits="WebApp.Item.BuyMenuListManage" %>


<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form runat="server" id="form">
        <table>
        <tr>
            <td> <strong><%= WebApp.DisplayString.BuyMenuListManage%></strong></td>
            <td>
                <table>
                <tr>
                    <td> <%= WebApp.DisplayString.ServerChoice %> </td>
                    <td> <asp:DropDownList runat="server" ID="ServNoDDLForView" DataTextField="Text" DataValueField="Value" /> </td>
                    <td> <asp:Button runat="server" ID="ServerSearchButton" UseSubmitBehavior="false" OnClick="SearchButtonButton_OnClick" /> </td>
                </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td> <strong><%= WebApp.DisplayString.SearchBuyMenuList %></strong> </td>
            <td>
                <table>
                <tr>
                    <td> <asp:TextBox runat="server" ID="BuyMenuListFilterText" /> </td>
                    <td> <asp:Button runat="server" ID="BuyMenuListFilterButton" Text="<%# WebApp.DisplayString.Search %>" OnClick="BuyMenuListFilterButton_OnClick" /></td>
                    <td> <asp:Button runat="server" ID="BuyMenuListNotFilterButton" Text="<%# WebApp.DisplayString.SearchAll %>" OnClick="BuyMenuListNotFilterButton_OnClick" /></td>
                </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td> <strong><%= WebApp.DisplayString.SelectBuyMenuList%></strong></td>
            <td>
                <table>
                <tr>
                    <td> <asp:DropDownList runat="server" ID="BuyMenuListDDLForView" DataTextField="Text" DataValueField="Value" /> </td>
                    <td> <asp:Button runat="server" ID="BuyMenuListSearchButton" OnClick="SearchButtonButton_OnClick" /> </td>
                </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Button runat="server" ID="DoEditButton" Text="<%# WebApp.DisplayString.DoEdit %>" OnClick="DoEditButton_OnClick" />
                <asp:Button runat="server" ID="SetDefaultButton" Text="<%# WebApp.DisplayString.SetDefault %>" OnClick="SetDefaultButton_OnClick" />
                <asp:Button runat="server" ID="ExcelDownloadButton" Text="<%# WebApp.DisplayString.ExcelDownload %>" OnClick="ExcelDownloadButton_OnClick" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Panel runat="server" ID="ResultPanel">
                </asp:Panel>
                <asp:Table runat="server" CssClass="lineTable" ID="BuyMenuListTable">
                    <asp:TableRow>
                        <asp:TableHeaderCell> <%# WebApp.DisplayString.ServerName %> </asp:TableHeaderCell>
                        <asp:TableHeaderCell> <%# WebApp.DisplayString.TabInfo %> </asp:TableHeaderCell>
                        <asp:TableHeaderCell> <%# WebApp.DisplayString.StoreType %> </asp:TableHeaderCell>
                        <asp:TableHeaderCell> <%# WebApp.DisplayString.ItemName %> </asp:TableHeaderCell>
                        <asp:TableHeaderCell> <%# WebApp.DisplayString.ItemID %> </asp:TableHeaderCell>
                        <asp:TableHeaderCell> <%# WebApp.DisplayString.StorePrice %> </asp:TableHeaderCell>
                        <asp:TableHeaderCell> <%# WebApp.DisplayString.StoreInfo %> </asp:TableHeaderCell>
                        <asp:TableHeaderCell> <%# WebApp.DisplayString.TBAFragmentCount %> </asp:TableHeaderCell>
                    </asp:TableRow>
                </asp:Table>
            </td>
        </tr>
        </table>
    </form>

</asp:Content>