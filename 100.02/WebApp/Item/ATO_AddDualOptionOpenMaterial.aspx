﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_AddDualOptionOpenMaterial.aspx.cs" Inherits="WebApp.Item.ATO_AddDualOptionOpenMaterial" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
    <table class="lineTable">
        <tr style="margin: 0px;">
            <td><%# WebApp.DisplayString.ServerChoice %></td>
            <td><asp:CheckBoxList ID="Servers" runat="server" DataSource="<%#WebApp.WebAdminDB.ServerListWithAll.Select(x => new { Text = x.mName, Value = x.mNo })%>" DataTextField="Text" DataValueField="Value" RepeatColumns="5" RepeatLayout="Table" /></td>
        </tr>
    </table>
    <table style="margin-top: 20px;margin-bottom: 20px" class="lineTable">
        <!-- 기간 설정 -->
        <tr>
            <td><%# WebApp.DisplayString.EventPeriod %></td>
            <td><asp:TextBox runat="server" Rows="1" ID="StartTime" type="date" onkeypress="return false;" onkeydown="return false;" /> ~ <asp:TextBox runat="server" Rows="1" ID="EndTime" type="date"  onkeypress="return false;" onkeydown="return false;" />
                <script type="text/javascript">
                    $(function () {
                        $('*[type=date]').appendDtpicker({ "minuteInterval": 1 });
                    });
                </script>
            </td>
        </tr>
    </table>
    <table style="margin-top: 20px" class="lineTable">
        <!-- 착용 부위 -->
        <tr>
            <td><%# WebApp.DisplayString.ItemWearingPart %></td>
            <td><asp:DropDownList runat="server" ID="CombatItemTypeSelect" DataValueField="Item1" DataTextField="Item2"  /></td>
        </tr>
    </table>
    <table style="margin-top: 20px" class="lineTable">
        <!-- 단계(rank) 설정 -->
        <tr>
            <td><%# WebApp.DisplayString.SettingSteps %></td>
            <td><asp:TextBox runat="server" ID="ItemRank" style="ime-mode:disabled"/></td>
        </tr>
    </table>
    <table style="margin-top: 20px" class="lineTable">
        <!-- 등급(grade) 설정 -->
        <tr>
            <td><%# WebApp.DisplayString.Grade %></td>
            <td><asp:DropDownList runat="server" ID="ItemGradeSelect" DataValueField="Item1" DataTextField="Item2" /></td>
        </tr>
    </table>
    <table style="margin-top: 20px" class="lineTable">
        <!-- 아이템설정 현황-->
        <tr>
            <td><%# WebApp.DisplayString.SettingOpenMaterial %></td>
            <td><asp:TextBox runat="server" ID="OpenMaterial" TextMode="MultiLine" ReadOnly="true" Rows="4" Width="300px"/></td>
        </tr>
    </table>
    <table style="margin-top: 20px" class="lineTable">
        <!-- 개방 재료 설정 -->
        <tr>
            <td><%# WebApp.DisplayString.StyleItem_TemplateId %> <asp:TextBox runat="server" ID="ItemTid" style="ime-mode:disabled"/>
                 <%# WebApp.DisplayString.ItemAmount %> <asp:TextBox runat="server" ID="ItemAmount" style="ime-mode:disabled"/>
                 <asp:Button runat="server" ID="AddMaterial" Text="<%# WebApp.DisplayString.Add %>" OnClick="AddMaterial_Click" /></td>
        </tr>
    </table>
    <table style="margin-top: 20px" class="lineTable">
        <!-- 골드 설정 -->
        <tr>
            <td><%# WebApp.DisplayString.Gold %></td>
            <td><asp:TextBox runat="server" ID="Gold" style="ime-mode:disabled"/>
                <asp:Button runat="server" ID="AddGold" Text="<%# WebApp.DisplayString.Add %>" OnClick="AddGold_Click" /></td>
        </tr>
    </table>
    

</asp:Content>
