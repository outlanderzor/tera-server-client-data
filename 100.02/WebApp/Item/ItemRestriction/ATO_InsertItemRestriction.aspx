﻿<%@ Import Namespace="WebApp" %>
<%@ Import Namespace="WebApp.CommonEnum" %>
<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/ServerSelectAskDialog.master" AutoEventWireup="true" CodeBehind="~/Item/ItemRestriction/ATO_InsertItemRestriction.aspx.cs" Inherits="WebApp.Item.ATO_InsertItemRestriction" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TERA_ATO" runat="server">
    <div class="ato-main">
		<ul class="form-style">
			<li>
                <label class="width250"><%# DisplayString.ServerName %></label><asp:Label ID="ServerName" runat="server"></asp:Label>
            </li>
			<li>
                <label class="width250"><%# DisplayString.ItemTemplateId %></label><asp:TextBox ID="ItemTemplateId" Text = "0" placeholder="" runat="server" OnKeyDown="return IsNumeric(event);" />
                <asp:Button runat="server" ID="FindBtn" OnClick="OnFindClicked" Text="<%# DisplayString.ItemInsert %>"/>
                <asp:Label runat="server" ID="ItemName" /> 
                <asp:Label runat="server" ID="CheckItemTemplateId" Visible="false" />                
			</li>
		</ul>
	</div>
</asp:Content>
