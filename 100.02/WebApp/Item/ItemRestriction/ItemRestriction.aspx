﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="ItemRestriction.aspx.cs" Inherits="WebApp.Item.ItemRestriction" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headerContentHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">
    <form id="Form1" runat="server">
	<table class="lineTable">
		<tr>
			<th><%# WebApp.DisplayString.ServerChoice %></th>
			<th><%# WebApp.DisplayString.Search %></th>
		</tr>
		<tr>
			<td><asp:DropDownList runat="server" ID="ServerSelect" DataSource="<%# WebApp.WebAdminDB.ServerListWithAll.Select(x => new { ServerName = x.mName, ServerNo = x.mNo }) %>" DataTextField="ServerName" DataValueField="ServerNo"/></td>
			<td><asp:Button runat="server" ID="SelectOption" Text="Search" OnClick="SelectOption_Click" /></td>
		</tr>
	</table>
    <div id="controlButtons" style="text-align: right; margin-bottom: 10px;">
	    <asp:Button runat="server" ID="InsertData" OnClick="InsertData_Click" Text="<%# WebApp.DisplayString.InsertItem %>"/>
    </div>
	<table class="lineTable" style="width: 100%;">
		<asp:Repeater runat="server" ID="ItemRestrictionList">
			<HeaderTemplate>
				<tr>
					<th><%# WebApp.DisplayString.ServerName %></th>
					<th><%# WebApp.DisplayString.ItemName %></th>
                    <th><%# WebApp.DisplayString.DeleteItem %></th>
				</tr>
			</HeaderTemplate>
			<ItemTemplate>
				<tr>
					<td><%# Eval("ServerName") %></td>
                    <td><%# "[" + Eval("ItemTemplateId") + "] " + Eval("ItemName") %></td>
					<td>
						<asp:button runat="server" ID="DeleteItemRestriction" Text='<%# WebApp.DisplayString.DeleteItem %>' CommandName="DeleteItemRestriction" CommandArgument='<%# Eval("ServerNo") + " " + (Eval("ItemTemplateId")) %>' OnCommand="DeleteData_Click"/>
					</td>
				</tr>
			</ItemTemplate>
		</asp:Repeater>
	</table>
</form>
</asp:Content>