﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/AppCode/DataSetAskDialog.master" CodeBehind="ATO_DeletePeriodItem.aspx.cs" Inherits="WebApp.Item.ATO_DeletePeriodItem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
	<table class="lineTable width-full">
		<asp:Repeater runat="server" ID="PeriodItemEventList">
			<HeaderTemplate>
				<tr>
				    <th><%# WebApp.DisplayString.ServerName %> </th>
                    <th><%# WebApp.DisplayString.Item %> </th>
                    <th><%# WebApp.DisplayString.GiveItemPeriod %> </th>
                    <th><%# WebApp.DisplayString.ItemDeleteTime %> </th>
                    <th><%# WebApp.DisplayString.ItemList %></th>  
				</tr>
			</HeaderTemplate>
			<ItemTemplate>
				<tr>
					<td><%# Eval("ServerName") %></td>
                    <td><%# Eval("ItemStr") %></td>
                    <td><%# Eval("EventStartTime")%> ~<br/><%#Eval("EventEndTime") %></td>
                    <td><%# Eval("ItemPeriodEndTime") %></td>
                    <td><%# Eval("ItemList") %></td>
				</tr>
			</ItemTemplate>
		</asp:Repeater>
	</table>

</asp:Content>
