﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/AppCode/DataSetAskDialog.master" CodeBehind="ATO_AddPeriodItem.aspx.cs" Inherits="WebApp.Item.ATO_AddPeriodItem" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
	<div class="ato-serverlist">
		<label><%# WebApp.DisplayString.CheckServer %></label>
		<ul>
			<asp:Repeater runat="server" ID="ServerCheckList">
				<ItemTemplate>
					<li>
                        <asp:CheckBox runat="server" ID="ServerName" Text='<%# Eval("ServerName") %>' />
                        <asp:HiddenField runat="server" ID="ServerNo" Value='<%# Eval("ServerNo") %>' />
					</li>
				</ItemTemplate>
			</asp:Repeater>
		</ul>
		<label><%# WebApp.DisplayString.CheckAllServer %></label>
	</div>

	<div class="ato-main">
		<ul class="form-style">
            <li>
                <label class="width150"><%# WebApp.DisplayString.GiveItemPeriod %></label>
                <asp:TextBox runat="server" ID="EventStartTime" type="date" /> ~
                <asp:TextBox runat="server" ID="EventEndTime" type="date" />
            </li>
			<li>
                <label class="width150"><%# WebApp.DisplayString.ItemDeleteTime %></label>
                <asp:TextBox runat="server" ID="ItemPeriodEndTime" type="date" />
			</li>
            <li>
                <label class="width150"><%# WebApp.DisplayString.ItemTemplateId %></label>
                <asp:DropDownList runat="server" ID="ItemTemplateId" AutoPostBack="true" OnSelectedIndexChanged="OnItemTemplateIdChanged" DataValueField="Key" DataTextField="Value" />
			</li>
             <li>
                <label class="width150"><%# WebApp.DisplayString.ItemList %></label>
                <asp:Repeater runat="server" ID="InBoxItemList">
				    <ItemTemplate>
					    <ul><label class="width700"><%# Eval("ItemStr") %></label></ul>
				    </ItemTemplate>
			    </asp:Repeater>
             </li>            
		</ul>
	</div>
    <script type="text/javascript">
        $(function () {
            $('[type=date]').each(function () {
                $(this).appendDtpicker({ "dateOnly": false, "minuteInterval": 1 });
            })
            $('[type=date]').keypress(function () { return false; })
            $('[type=date]').keydown(function () { return false; })
        });
	</script>
</asp:Content>
