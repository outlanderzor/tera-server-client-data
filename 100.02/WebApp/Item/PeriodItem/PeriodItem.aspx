﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="PeriodItem.aspx.cs" Inherits="WebApp.Item.PeriodItem" %>

<asp:Content ID="Content3" ContentPlaceHolderID="headerContentHolder" runat="server">
	<style type="text/css">
	</style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="mainContentHolder" runat="server">
	<strong><%# WebApp.DisplayString.NAVMENU_PeriodItem_Full %></strong>
    <ul class="submenus">
		<asp:Repeater runat="server" ID="SubMenus">
			<ItemTemplate>
				<li><a href="<%# Eval("Url") %>" class="<%# Eval("Class") %>"><%# Eval("Text") %></a></li>
			</ItemTemplate>
		</asp:Repeater>
	</ul>

	<form id="Form1" runat="server">
		<table class="lineTable" style="margin-bottom:10px;">
			<tr>
				<th><%# WebApp.DisplayString.ServerChoice %></th>
				<th><%# WebApp.DisplayString.Search %></th>
			</tr>
			<tr>
				<td><asp:DropDownList runat="server" ID="ServerSelect" DataSource="<%# WebApp.WebAdminDB.ServerListWithAll.Select(x => new { ServerName = x.mName, ServerNo = x.mNo }) %>" DataTextField="ServerName" DataValueField="ServerNo"/></td>
				<td><asp:Button runat="server" ID="SelectOption" Text="Search" OnClick="SelectOption_Click" /></td>
			</tr>
		</table>

        <div id="controlButtons">
		    <asp:Button runat="server" ID="AddPeriod" CommandName="AddPeriod" OnCommand="OnAddPeriod" Text="<%# WebApp.DisplayString.AddSetting %>"/>
            <asp:Button runat="server" ID="DeletePeriod" CommandName="DeletePeriod" OnCommand="OnDeletePeriod" Text="<%# WebApp.DisplayString.DeleteSetting %>"/>
            <asp:Button runat="server" ID="DeleteAllPeriod" CommandName="DeleteAllPeriod" OnCommand="OnDeleteAllPeriod" Text="<%# WebApp.DisplayString.DeleteAllSetting %>"/>
	    </div>
        <br />
		<table class="lineTable ">
			<asp:Repeater runat="server" ID="PeriodItemList">
				<HeaderTemplate>
					<tr>
                        <th><asp:CheckBox runat="server" ID="PeriodcheckAll" AutoPostBack="true" OnCheckedChanged="OnPeriodCheckAll"/></th>
						<th><%# WebApp.DisplayString.ServerName %></th>
                        <th><%# WebApp.DisplayString.Item %></th>
                        <th><%# WebApp.DisplayString.GiveItemPeriod %></th>
                        <th><%# WebApp.DisplayString.ItemDeleteTime %></th>
                        <th><%# WebApp.DisplayString.ItemList %></th>
					</tr>
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
                        <td><asp:CheckBox runat="server" ID="PeriodCheck" /><asp:HiddenField runat="server" ID="ServerID" Value='<%# Eval("ServerNo") + " " + Eval("EventId") %>'/></td>
						<td><%# Eval("ServerName") %></td>
                        <td><%# WebApp.DataSheetManager.GetItemNameWithTID((int)Eval("TemplateId")) %></td>
                        <td><%# Eval("EventStartTime")%> ~ <%#Eval("EventEndTime") %></td>
                        <td><%# Eval("ItemPeriodEndTime") %></td>
                        <td><%# Eval("ItemList") %></td>
					</tr>
				</ItemTemplate>
			</asp:Repeater>
		</table>

	</form>
</asp:Content>
