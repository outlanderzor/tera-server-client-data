﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_DeleteDualOptionOpenMaterial.aspx.cs" Inherits="WebApp.Item.ATO_DeleteDualOptionOpenMaterial" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
    <table class="lineTable">
        <asp:Repeater runat="server" ID="EventList">
            <HeaderTemplate>
                <tr>
                    <th><asp:CheckBox runat="server" ID="ServerCheckAll" AutoPostBack="true" OnCheckedChanged="ServerCheckAll_OnClick" /></th>
                    <th><%# WebApp.DisplayString.ServerName %></th>
                    <th><%# WebApp.DisplayString.ItemWearingPart %></th>
                    <th><%# WebApp.DisplayString.Step %></th>
                    <th><%# WebApp.DisplayString.Grade %></th>
                    <th><%# WebApp.DisplayString.Item %></th>
                    <th><%# WebApp.DisplayString.StartDate %></th>
                    <th><%# WebApp.DisplayString.EndDate %></th>
                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="EventCheck" />
                        <asp:HiddenField runat="server" ID="ServerEvent" Value='<%# Eval("ServerNo") + "_" + Eval("SettingId") %>' />
                    </td>
                    <td><%# Eval("ServerName") %></td>
                    <td><asp:Label runat="server" ID="CombatItemType" Text='<%# Eval("CombatItemTypeString") %>' /> </td>
                    <td><asp:Label runat="server" ID="ItemRank" Text='<%# Eval("ItemRank") %>' /> </td>
                    <td><asp:Label runat="server" ID="ItemGrade" Text='<%# Eval("ItemGrade") %>' /> </td>
                    <td><%# Eval("MaterialItemListString") %></td>
                    <td><asp:Label runat="server" ID="StartDate" Text='<%# Eval("StartTime") %>' /> </td>
                    <td><asp:Label runat="server" ID="EndDate" Text='<%# Eval("EndTime") %>' /> </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
        </table>
</asp:Content>