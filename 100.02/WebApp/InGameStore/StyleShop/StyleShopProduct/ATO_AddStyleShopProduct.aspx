﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" ValidateRequest="false" MasterPageFile="~/AppCode/DataSetAskDialog.master" CodeBehind="ATO_AddStyleShopProduct.aspx.cs" Inherits="WebApp.InGameStore.StyleShop.StyleShopProduct.ATO_AddStyleShopProduct" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
	<div class="ato-serverlist">
		<label><%# WebApp.DisplayString.CheckServer %></label>
        <asp:CheckBox runat="server" Text='<%# WebApp.DisplayString.SelectAll %>' ID="CheckAllServer" AutoPostback="true" OnCheckedChanged="OnSelectAllServers"/>
		<ul>
			<asp:Repeater runat="server" ID="ServerCheckList">
				<ItemTemplate>
					<li>
                        <asp:CheckBox runat="server" ID="ServerName" Text='<%# Eval("ServerName") %>' AutoPostback="true" OnCheckedChanged="OnSelectServer" />
                        <asp:HiddenField runat="server" ID="ServerNo" Value='<%# Eval("ServerNo") %>' />
					</li>
				</ItemTemplate>
			</asp:Repeater>
		</ul>
	</div>

	<div class="ato-main">
		<ul class="form-style">
            <li>
                <label class="width150"><%# WebApp.DisplayString.Item %></label>
                <asp:TextBox runat="server" ID="ItemTemplateId" type="int" OnKeyDown="return IsNumeric(event);"/>
                <asp:Button runat="server" ID="FindBtn" OnClick="OnFindClicked" Text="<%# WebApp.DisplayString.ItemInsert %>"/>             
            </li>
            <li>
                <label class="width150"></label>
                <asp:Label runat="server" ID="ItemName" />                
            </li>      
            <li>
                <br />
                <label class="width150"><%# WebApp.DisplayString.SalePeriod %></label>
                <asp:TextBox runat="server" ID="SaleStartDate" type="date" /> ~
                <asp:TextBox runat="server" ID="SaleEndDate" type="date" />
            </li>
            <li>
                <label class="width150"><%# WebApp.DisplayString.MarkId %></label>
                <asp:DropDownList runat="server" AutoPostBack="true" ID="MarkId" DataValueField="Key" DataTextField="Value" />
			</li>
            <li>
                <label class="width150"><%# WebApp.DisplayString.ApplyMarkPeriod %></label>
                <asp:TextBox runat="server" ID="MarkStartDate" type="date" /> ~
                <asp:TextBox runat="server" ID="MarkEndDate" type="date" />
            </li>

            <li>
                <asp:Label runat="server" width="150"><%# WebApp.DisplayString.SaleType %></asp:Label>
                <asp:DropDownList runat="server" AutoPostBack="true" ID="SaleType" DataValueField="Key" DataTextField="Value" />
         	</li>

            <asp:Panel runat="server" ID="SalePanel">
                <li>
                    <asp:Label runat="server" width="150" ID="PriceLabel"><%# WebApp.DisplayString.TCatPrice %></asp:Label>
                    <asp:TextBox runat="server" ID="Price" OnKeyDown="return IsNumeric(event);"/>
                    <asp:RangeValidator runat="server" ControlToValidate="Price" MinimumValue="0" MaximumValue="100000000" Type="Integer" EnableClientScript="false" Text="(1 ~ 1000000000)" ForeColor="Red"/>
			    </li>

                <asp:Panel runat="server" ID="DiscountPanel">
                    <li>
                        <asp:Label runat="server" width="150" ID="DiscountLabel"><%# WebApp.DisplayString.DiscountRate %></asp:Label>
                        <asp:TextBox runat="server" ID="DiscountPercent" OnKeyDown="return IsNumeric(event);"/>
                        <asp:Button runat="server" ID="DiscountBtn" Text="<%# WebApp.DisplayString.OK %>"/>
                        <asp:RangeValidator runat="server" ControlToValidate="DiscountPercent" MinimumValue="1" MaximumValue="99" Type="Integer" EnableClientScript="false" Text="(1 ~ 99)" ForeColor="Red"/>
                    </li>

                    <li>
                        <asp:Label runat="server" width="150" ID="PriceAfterSaleLabel"><%# WebApp.DisplayString.PriceAfterSale %></asp:Label>
                        <asp:TextBox runat="server" ID="PriceAfterSale" ReadOnly="true"/>
			        </li>
                </asp:Panel>
            </asp:Panel>
           

            <li>
                <br />
                <asp:label runat="server" width="150"><%# WebApp.DisplayString.PreviewStartDate %></asp:label>
                <asp:TextBox runat="server" ID="PreviewStartDate" type="date" />
                <asp:CheckBox runat="server" ID="TogglePreview" Text='<%# WebApp.DisplayString.Use %>' AutoPostBack="true" OnCheckedChanged="OnTogglePreview"/>
            </li>
             <li>
                <asp:label runat="server" width="150"><%# WebApp.DisplayString.PreviewString %></asp:label>
                <asp:TextBox runat="server" ID="PreviewDetail" MaxLength="15"/>
			</li>
		</ul>
	</div>
    <script type="text/javascript">
        $(function () {
            $('[type=date]').each(function () {
                $(this).appendDtpicker({ "dateOnly": false, "minuteInterval": 1 });
            })
            $('[type=date]').keypress(function () { return false; })
            $('[type=date]').keydown(function () { return false; })
        });
	</script>
</asp:Content>