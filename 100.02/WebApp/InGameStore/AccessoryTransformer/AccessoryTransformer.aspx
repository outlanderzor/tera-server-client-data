﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="AccessoryTransformer.aspx.cs" Inherits="WebApp.InGameStore.AccessoryTransformer.AccessoryTransformer" %>


<asp:Content ID="Content1" ContentPlaceHolderID="headerContentHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">
    <form id="Form1" runat="server">
	<table class="lineTable" style="margin-bottom:10px;">
		<tr>
			<th><%# WebApp.DisplayString.ServerChoice %></th>
			<th><%# WebApp.DisplayString.Search %></th>
		</tr>
		<tr>
			<td><asp:DropDownList runat="server" ID="ServerSelect" DataSource="<%# WebApp.WebAdminDB.ServerListWithAll.Select(x => new { ServerName = x.mName, ServerNo = x.mNo }) %>" DataTextField="ServerName" DataValueField="ServerNo"/></td>
			<td><asp:Button runat="server" ID="SelectOption" Text="Search" OnClick="SelectOption_Click" /></td>
		</tr>
	</table>
	<table class="lineTable ">
		<asp:Repeater runat="server" ID="OnOffStateList">
			<HeaderTemplate>
				<tr>
					<th><%# WebApp.DisplayString.ServerName %></th>
					<th><%# WebApp.DisplayString.AccessoryTransformerControlState %></th>
					<th><%# WebApp.DisplayString.Control %></th>
                    <th><%# WebApp.DisplayString.PartsControlState %></th>
					<th><%# WebApp.DisplayString.Control %></th>
                    <th><%# WebApp.DisplayString.AccessoryTransformCostControl%></th>
				</tr>
			</HeaderTemplate>
			<ItemTemplate>
				<tr>
					<td><%# Eval("ServerName") %></td>
					<td><%# (bool)Eval("IsConverterOn") ? "On" : "Off" %></td>
					<td>
						<asp:button runat="server" ID="ConverterOnOff" Text='<%# (bool)Eval("IsConverterOn") ? "Off" : "On" %>' CommandName="ConverterOnOff" CommandArgument='<%# Eval("ServerNo") + " " + ((bool)Eval("IsConverterOn") ? "Off" : "On") %>' OnCommand="ConverterOnOff_Command"/>
					</td>
                    <td><%# (bool)Eval("IsPartsOn") ? "On" : "Off" %></td>
					<td>
						<asp:button runat="server" ID="PartsOnOff" Enabled='<%# (bool)Eval("IsConverterOn")%>' Text='<%# (bool)Eval("IsPartsOn") ? "Off" : "On" %>' CommandName="PartsOnOff" CommandArgument='<%# Eval("ServerNo") + " " + ((bool)Eval("IsPartsOn") ? "Off" : "On") %>' OnCommand="PartsOnOff_Command"/>
					</td>
                    <td>
                        <asp:button runat="server" ID="ContInfoControl" Text="<%# WebApp.DisplayString.Modify %>" CommandName="ContInfoControl" CommandArgument='<%# Eval("ServerNo") %>' OnCommand="ContInfoControl_Command"/>
                    </td>
				</tr>
			</ItemTemplate>
		</asp:Repeater>
	</table>
</form>
</asp:Content>