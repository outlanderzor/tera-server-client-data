﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_AccessoryTransformerCostControl.aspx.cs" Inherits="WebApp.InGameStore.AccessoryTransformer.ATO_AccessoryTransformerCostControl" %>
<%@ Import Namespace="WebApp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TERA_ATO" runat="server">
	<div class="ato-main">
		<ul class="form-style">
			<li>
                <label class="width250"><%# DisplayString.ServerName %></label><asp:Label ID="ServerName" runat="server"></asp:Label>
            </li>
            <li>
                <label class="width250"><%# DisplayString.IsFirstFree %></label>
				<asp:RadioButtonList ID="IsFirstFree" RepeatLayout="UnorderedList" class="radio-group" runat="server">
					<asp:ListItem Text="On" Value="True"/>
					<asp:ListItem Text="Off" Value="False" Selected="True"/>
				</asp:RadioButtonList>
			</li>
			<li>
                <label class="width250"><%# DisplayString.CostItemTemplateId %></label><asp:TextBox ID="ItemTemplateId" Text = "0" placeholder="" runat="server" OnKeyDown="return IsNumeric(event);" />
                <asp:Button runat="server" ID="FindBtn" OnClick="OnFindClicked" Text="<%# DisplayString.ItemInsert %>"/>
                <asp:Label runat="server" ID="ItemName" /> 
                <asp:Label runat="server" ID="CheckItemTemplateId" Visible="false" />                
			</li>
			<li>
                <label class="width250"><%# DisplayString.CostCount %></label>
                <asp:TextBox ID="Amount" style="width: 300px;" Text = "0"  placeholder="" runat="server" OnKeyDown="return IsNumeric(event);"/>
                <asp:RangeValidator runat="server" ControlToValidate="Amount" MinimumValue="0" MaximumValue="99" Type="Integer" EnableClientScript="false" Text="(0 ~ 99)" ForeColor="Red"/>
			</li>
		</ul>
	</div>
</asp:Content>
