﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/CustomAskDialog.Master" AutoEventWireup="true" CodeBehind="ATO_DelAchievementSeason.aspx.cs" Inherits="WebApp.Server.ATO_DelAchievementSeason" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentName" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentBody" runat="server">
    <form runat="server" id="form1">                 
        <!-- 잘못 입력 된 값에 대한 안내 메세지  -->
        <p style="color:red; font-weight:bold"><asp:Label ID="ErrorInfo" runat="server" visible="false"/></p><br />

        <asp:Panel id="EditPanel" runat="server" visible="true">
        <!-- 이전 페이지에서 넘어온 시즌 리스트 -->
        <asp:GridView CssClass="lineTable" ID="SeasonInfoGrid" runat="server" AutoGenerateColumns="False" ShowHeader="true" ShowHeaderWhenEmpty="true">
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate></HeaderTemplate>
                    <ItemTemplate><asp:CheckBox ID="chkRow" runat="server"/></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false">
                    <HeaderTemplate></HeaderTemplate>
                    <ItemTemplate><%# Eval("serverNo").ToString() %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField   HeaderStyle-Width="100">
                    <HeaderTemplate><%=WebApp.DisplayString.ServerName%></HeaderTemplate>
                    <ItemTemplate><%# GetServerName(Convert.ToInt16(Eval("serverNo"))) %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>시즌 ID</HeaderTemplate>
                    <ItemTemplate><%# Eval("seasonId").ToString()%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField >
                    <HeaderTemplate><%=WebApp.DisplayString.StartTime%></HeaderTemplate>
                    <ItemTemplate><%# Eval("startDate").ToString()%></ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView><br /><br />    

        <!-- 메모 입력란 -->
        <p style="font-weight:bold">[<%= WebApp.DisplayString.MemoStr %>]</p><br />
        <asp:TextBox runat="server" width="250px" height="100px" ID="MemoBox" TextMode="MultiLine" style="overflow:auto"/><br /><br />    

        <!-- 확인 버튼 -->
        <asp:Button runat="server" ID="ConfirmButton" text='<%# WebApp.DisplayString.DelAchievementSeason%>' OnClick="ConfirmButton_Click" />
        </asp:Panel>

        <asp:Button runat="server" ID="CloseButton" text="<%# WebApp.DisplayString.Close%>" OnClick="CloseButton_Click" Visible="false"/><br />
    </form>
</asp:Content>
