﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApp.Server.Default" %>
<%@ MasterType VirtualPath="~/AppCode/WebAppPage.Master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form runat="server">
        <asp:Table runat="server" CssClass="outerlineTable">
            <asp:TableRow>
                <asp:TableCell>
                    <strong><asp:Label runat="server" Text='<%# "[ " + WebApp.DisplayString.List + " ]" %>' /></strong>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <table class="lineTable">
                        <asp:Repeater runat="server" ID="ServerInfosRepeator" OnItemCommand="ServerInfosRepeator_OnItemCommand">
                            <HeaderTemplate>
                                <tr>
                                    <th><%# WebApp.DisplayString.ServerNo %></th>
                                    <th><%# WebApp.DisplayString.ServerName %></th>
                                    <th><%# WebApp.DisplayString.Addr %></th>
                                    <th><%# WebApp.DisplayString.Port %></th>
                                    <th><%# WebApp.DisplayString.PlanetId %></th>
                                    <th><%# WebApp.DisplayString.WorldDb %></th>
                                    <th><%# WebApp.DisplayString.SharedWorldDb %></th>
                                    <th><%# WebApp.DisplayString.LogDb %></th>
                                    <th><%# WebApp.DisplayString.ReportDb %></th>
                                    <th><%# WebApp.DisplayString.MergeReportDb %></th>
                                    <th><%# WebApp.DisplayString.IsOnline %></th>
                                    <th><%# WebApp.DisplayString.UserLimit %></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td><%# Eval("ServerNo") %></td>
                                    <td><%# Eval("ServerName") %></td>
                                    <td><%# Eval("Addr") %></td>
                                    <td><%# Eval("Port") %></td>
                                    <td><%# Eval("PlanetId") %></td>
                                    <td><%# Eval("WorldDb") %></td>
                                    <td><%# Eval("SharedDb") %></td>
                                    <td><%# Eval("LogDb") %></td>
                                    <td><%# Eval("ReportDb") %></td>
                                    <td><%# Eval("MergeReportDb") %></td>
                                    <td><%# (bool)Eval("IsOnline") ? WebApp.DisplayString.Online : WebApp.DisplayString.Offline %></td>
                                    <td><asp:TextBox runat="server" ID="UserLimitTextBox" Text='<%# Eval("UserLimit") %>' /></td>
                                    <td>
                                        <asp:Button runat="server" ID="ChangeUserLimitButton" Text="<%# WebApp.DisplayString.UpdateUserLimit %>" OnClientClick='return confirm("인원수를 정말 업데이트 하시겠습니까?")'
                                            CommandName="UpdateUserLimit" CommandArgument='<%# Eval("ServerNo") %>' />
                                    </td>
                                    <td>
                                        <asp:Button runat="server" ID="DeleteServerButton" Text="<%# WebApp.DisplayString.X %>" OnClientClick='return confirm("Delete?")'
                                            CommandName="DeleteServer" CommandArgument='<%# Eval("ServerNo") %>' OnCommand="DeleteServerButton_OnCommand" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>

        <asp:Table runat="server" CssClass="outerlineTable">
            <asp:TableRow>
                <asp:TableCell>
                    <strong><asp:Label runat="server" Text='<%# "[ " + WebApp.DisplayString.ClosedList + " ]" %>' /></strong>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <table class="lineTable">
                        <asp:Repeater runat="server" ID="ClosedServerInfosRepeator">
                            <HeaderTemplate>
                                <tr>
                                    <th><%# WebApp.DisplayString.ServerNo %></th>
                                    <th><%# WebApp.DisplayString.ServerName %></th>
                                    <th><%# WebApp.DisplayString.Addr %></th>
                                    <th><%# WebApp.DisplayString.Port %></th>
                                    <th><%# WebApp.DisplayString.PlanetId %></th>
                                    <th><%# WebApp.DisplayString.WorldDb %></th>
                                    <th><%# WebApp.DisplayString.SharedWorldDb %></th>
                                    <th><%# WebApp.DisplayString.LogDb %></th>
                                    <th><%# WebApp.DisplayString.ReportDb %></th>
                                    <th><%# WebApp.DisplayString.MergeReportDb %></th>
                                    <th><%# WebApp.DisplayString.IsOnline %></th>
                                    <th><%# WebApp.DisplayString.UserLimit %></th>
                                </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td><%# Eval("ServerNo") %></td>
                                    <td><%# Eval("ServerName") %></td>
                                    <td><%# Eval("Addr") %></td>
                                    <td><%# Eval("Port") %></td>
                                    <td><%# Eval("PlanetId") %></td>
                                    <td><%# Eval("WorldDb") %></td>
                                    <td><%# Eval("SharedDb") %></td>
                                    <td><%# Eval("LogDb") %></td>
                                    <td><%# Eval("ReportDb") %></td>
                                    <td><%# Eval("MergeReportDb") %></td>
                                    <td><%# (bool)Eval("IsOnline") ? WebApp.DisplayString.Online : WebApp.DisplayString.Offline %></td>
                                    <td><%# Eval("UserLimit") %></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>

        <asp:Button runat="server" ID="RefreshServerListButton" Text="<%# WebApp.DisplayString.RefreshList %>" OnClick="RefreshServerListButton_OnClick" />
        <br /> <br />

        <asp:Table runat="server" CssClass="outerlineTable">
            <asp:TableRow>
                <asp:TableCell>
                    <strong><asp:Label runat="server" Text='<%# "[ Add New Server ]" %>' /></strong>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell><asp:Label runat="server" Text='<%# "Server Name : " %>' /></asp:TableCell>
                <asp:TableCell><asp:TextBox runat="server" ID="ServerName" /></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell><asp:Label runat="server" Text='<%# "User Name : " %>' /></asp:TableCell>
                <asp:TableCell><asp:TextBox runat="server" ID="UserName" /></asp:TableCell>
                <asp:TableCell><asp:Button runat="server" ID="AddServerButton" Text='<%# "Add Server" %>' OnClick="AddServerButton_OnClick" /></asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </form>

</asp:Content>
