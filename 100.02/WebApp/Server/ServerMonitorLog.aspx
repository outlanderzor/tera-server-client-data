﻿<%@ Page MasterPageFile="~/AppCode/WebAppPage.Master" Language="C#" AutoEventWireup="true" CodeBehind="ServerMonitorLog.aspx.cs" Inherits="WebApp.Server.ServerMonitorLog" %>
<%@ Register TagPrefix="webApp" TagName="SearchForm" Src="~/SearchForm.ascx" %>

<asp:Content ID="Content1" runat="server" 
    contentplaceholderid="mainContentHolder">

    <form runat="server" id ="resultForm" >
    
    <table class="nolineTable">
        <tr>
            <td> <%= WebApp.DisplayString.ServerChoice %> </td>
            <td><asp:DropDownList runat="server" ID="ServNoDDL" DataTextField="Text" DataValueField="Value" /></td>
            <td><asp:Button runat="server" ID="SearchButton" OnClick="SearchButton_Click" /></td>
          </tr>
    </table>
    <br />
    <table class="lineTable">
        <tr>
            <td><%= WebApp.DisplayString.LogWritePeriod%> </td>
            <td><%= WebApp.DatasheetConfig.ServerMonitorLog.refreshMin%></td>
        </tr>
        <tr>
            <td><%= WebApp.DisplayString.ServerMonitorLogKeepPeriod%> </td>
            <td><%= WebApp.DatasheetConfig.ServerMonitorLog.keepingPeriod%> </td>
        </tr>
        <tr>
            <td> Excel Export Start Date </td>
            <td><asp:TextBox runat="server" ID="StartDateText" /></td>
        </tr>
        <tr>
            <td> Excel Export End Date </td>
            <td><asp:TextBox runat="server" ID="EndDateText" /></td>
        </tr>
    </table>
    <br />
    <asp:Button runat="server" ID="ExcelButton" OnClick="ExportExcel" Text="Excel Download"/>
    <br />
    <br />
    <asp:GridView CssClass="lineTable" ID="GVSMLList" runat="server" AutoGenerateColumns="False" ShowHeader="true" ShowHeaderWhenEmpty="true">
    <Columns>
        <asp:TemplateField   HeaderStyle-Width="100">
            <HeaderTemplate><%=WebApp.DisplayString.ServerName%></HeaderTemplate>
            <ItemTemplate><%# Eval("serverNo").ToString() + " " + SetSeverName((int)Eval("serverNo"))%></ItemTemplate>
        </asp:TemplateField>
         <asp:TemplateField >
            <HeaderTemplate><%=WebApp.DisplayString.ConcurrentUser%></HeaderTemplate>
            <ItemTemplate><%# Eval("lastArbiterUserCount").ToString()%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField >
            <HeaderTemplate><%=WebApp.DisplayString.WaitCount%></HeaderTemplate>
            <ItemTemplate><%# Eval("lastWaitingListCount").ToString()%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField >
            <HeaderTemplate><%=WebApp.DisplayString.WorldStatus%></HeaderTemplate>
            <ItemTemplate><%# SetSeverStatus(Convert.ToInt32(Eval("worldServerStatus")))%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField >
            <HeaderTemplate><%=WebApp.DisplayString.LogStatus%></HeaderTemplate>
            <ItemTemplate><%# SetSeverStatus(Convert.ToInt32(Eval("logServerStatus")))%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField >
            <HeaderTemplate><%=WebApp.DisplayString.RestrictionStatus%></HeaderTemplate>
            <ItemTemplate><%# SetSeverStatus(Convert.ToInt32(Eval("petitionServerStatus")))%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField >
            <HeaderTemplate><%=WebApp.DisplayString.BattleFieldServerStatus%></HeaderTemplate>
            <ItemTemplate><%# SetSeverStatus(Convert.ToInt32(Eval("battleFieldServerStatus")))%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField >
            <HeaderTemplate><%=WebApp.DisplayString.InternetCafeConcurrent%></HeaderTemplate>
            <ItemTemplate><%# Eval("lastPcBangUser").ToString()%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField >
            <HeaderTemplate><%=WebApp.DisplayString.DungeonServerStatus%></HeaderTemplate>
            <ItemTemplate><%# SetDungeonServerStatus(Eval("dungeonServerStatusList").ToString())%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField >
            <HeaderTemplate><%=WebApp.DisplayString.PartyMatchServerStatus%></HeaderTemplate>
            <ItemTemplate><%# SetSeverStatus(Convert.ToInt32(Eval("partyMatchServerStatus")))%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField >
            <HeaderTemplate><%=WebApp.DisplayString.NexusServerStatus%></HeaderTemplate>
            <ItemTemplate><%# SetSeverStatus(Convert.ToInt32(Eval("nexusServerStatus")))%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField  HeaderStyle-Width="160">
            <HeaderTemplate><%=WebApp.DisplayString.Date%></HeaderTemplate>
            <ItemTemplate><%# SetUTCTime(Convert.ToDateTime(Eval("regDate")))%></ItemTemplate>
        </asp:TemplateField>
    </Columns>
    </asp:GridView>
    <%=ServerMonitorLogPaging%>
    </form>
</asp:Content>


