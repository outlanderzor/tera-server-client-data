﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="SetKillNpcEvent.aspx.cs" Inherits="WebApp.EventSystem.SetKillNpcEvent" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headerContentHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">
	<form id="Form1" runat="server">
		<div id="controlButtons">
			<asp:Button runat="server" ID="AddEvent" CommandName="AddEvent" OnCommand="AddEvent_Command" Text="<%# WebApp.DisplayString.AddSetting %>"/>
			<asp:Button runat="server" ID="DeleteSelectEvent" CommandName="DeleteSelectEvent" OnCommand="DeleteSelectEvent_Command" Text="<%# WebApp.DisplayString.DeleteSetting %>"/>
			<asp:Button runat="server" ID="DeleteAllEvent" CommandName="DeleteAllEvent" OnCommand="DeleteAllEvent_Command" Text="<%# WebApp.DisplayString.DeleteAllSetting %>"/>
		</div>
        <br />
		<table class="lineTable ">
			<asp:Repeater runat="server" ID="EventList">
				<HeaderTemplate>
					<tr>
						<th></th>
						<th><%# WebApp.DisplayString.EventType %></th>
						<th><%# WebApp.DisplayString.LevelGap %></th>
                        <th><%# WebApp.DisplayString.NpcType %></th>
                        <th><%# WebApp.DisplayString.KillCount %></th>
					</tr>
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
						<td><asp:CheckBox runat="server" ID="EventCheck" /><asp:HiddenField runat="server" ID="EventTypeId" Value='<%# Eval("EventTypeId") %>' /></td>
						<td><%# Eval("EventTypeId") %></td>
						<td><%# Eval("LevelGap") %></td>
						<td><%# Eval("NpcType") %></td>
						<td><%# Eval("KillCount") %></td>
					</tr>
				</ItemTemplate>
			</asp:Repeater>
		</table>
	</form>
</asp:Content>
