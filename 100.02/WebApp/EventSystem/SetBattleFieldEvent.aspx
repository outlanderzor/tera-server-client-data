﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="SetBattleFieldEvent.aspx.cs" Inherits="WebApp.EventSystem.SetBattleFieldEvent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">
     <form runat="server" id="form2">
        <table>
            <tr>
                <td colspan="2">
                    <asp:Panel runat="server" ID="InitPanel"/>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Panel runat="server" ID="ButtonPannel"/>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Panel runat="server" ID="ResultPanel" />
                </td>
            </tr>
        </table>
        <br />
        <br />
    </form>
</asp:Content>