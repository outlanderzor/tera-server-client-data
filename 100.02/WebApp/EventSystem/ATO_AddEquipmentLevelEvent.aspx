﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/CustomAskDialog.Master" AutoEventWireup="true" CodeBehind="ATO_AddEquipmentLevelEvent.aspx.cs" Inherits="WebApp.EventSystem.ATO_AddEquipmentLevelEvent" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHeader" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentBody" runat="server">
	<form id="Form2" runat="server" class="nolineTable">   
		<!-- 잘못 입력 된 값에 대한 안내 메세지  -->
		<p style="color:red; font-weight:bold"><asp:Label ID="ErrorInfo" runat="server" visible="false"/></p><br />

		<asp:Panel id="EditPanel" runat="server" visible="true">
			<ul class="form-style">
				<li><label class="width130"><%# WebApp.DisplayString.EquipmentLevel %></label><asp:TextBox runat="server" ID="EquipmentLevel"/></li>
			</ul>

			<!-- 메모 입력란 -->
			<p style="font-weight:bold">[<%= WebApp.DisplayString.MemoStr %>]</p><br />
			<asp:TextBox runat="server" width="250px" height="100px" ID="MemoBox" TextMode="MultiLine" style="overflow:auto"/><br /><br />    

			<!-- 확인 버튼 -->
			<asp:Button runat="server" ID="ConfirmButton" text='<%# WebApp.DisplayString.AddSetting %>' OnClick="ConfirmButton_Click" />
		</asp:Panel>

		<asp:Button runat="server" ID="CloseButton" text="<%# WebApp.DisplayString.Close%>" Visible="false" OnClick="CloseButton_Click"/><br />
	</form>
</asp:Content>
