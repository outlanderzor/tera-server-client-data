﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="GuildWarehouseLog.aspx.cs" Inherits="WebApp.Guild.GuildWarehouseLog" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form runat="server" id="resultForm" >

        <table class="nolineTable">
            <tr>
                <td> <%= WebApp.DisplayString.ServerChoice %> </td>
                <td> <asp:DropDownList runat="server" ID="ServNoDropDownList" DataTextField="Text" DataValueField="Value" AutoPostBack="true" /> </td>
            </tr>
            <tr>
                <td> <%= WebApp.DisplayString.GuildDBID %> </td>
                <td> <asp:TextBox runat="server" ID="GuildDbIdTextBox" /> </td>
            </tr>
            <tr>
                <td> <%= WebApp.DisplayString.GuildName %> </td>
                <td> <asp:TextBox runat="server" ID="GuildNameTextBox" /> </td>
            </tr>
            <tr>
                <td> <asp:Button runat="server" ID="SubmitButton" OnClick="SubmitButton_Click" /> </td>
                <td> <asp:Button runat="server" ID="ExcelButton" OnClick="ExcelButton_Click" /> </td>
            </tr>       
        </table>

        <asp:GridView CssClass="lineTable" ID="resultGV" runat="server" AutoGenerateColumns="false" ShowHeader="true">
        <Columns>
            <asp:TemplateField>
                <HeaderTemplate> <%= WebApp.DisplayString.Date %> </HeaderTemplate>
                <ItemTemplate> <%# Eval("Date") %> </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate> <%= WebApp.DisplayString.GuildName %> </HeaderTemplate>
                <ItemTemplate> <%# Eval("GuildName") %> </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate> <%= WebApp.DisplayString.GuildDBID %> </HeaderTemplate>
                <ItemTemplate> <a href="<%# Eval("GuildLink") %>"> <%# Eval("GuildDbId") %> </a> </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate> <%= WebApp.DisplayString.UserName %> </HeaderTemplate>
                <ItemTemplate> <%# Eval("UserName") %> </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate> <%= WebApp.DisplayString.UserDBID %> </HeaderTemplate>
                <ItemTemplate> <a href="<%# Eval("UserLink") %>"> <%# Eval("UserDbId") %> </a> </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate> <%= WebApp.DisplayString.ItemName %> </HeaderTemplate>
                <ItemTemplate> <%# Eval("ItemName") %> </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate> <%= WebApp.DisplayString.ItemDBID %> </HeaderTemplate>
                <ItemTemplate> <a href="<%# Eval("ItemLink") %>"> <%# Eval("ItemDbId") %> </a> </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate> <%= WebApp.DisplayString.Amount %> </HeaderTemplate>
                <ItemTemplate> <%# Eval("Amount") %> </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate> <%= WebApp.DisplayString.Action %> </HeaderTemplate>
                <ItemTemplate> <%# Eval("OpAction") %> </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        </asp:GridView>

    </form>

</asp:Content>
