﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="ChangeGuildWarDeclareCost.aspx.cs" Inherits="WebApp.Guild.ChangeGuildWarDeclareCost" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headerContentHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">
    <form id="Form1" runat="server">
            <asp:DropDownList runat="server" ID="ServerDDL" DataSource="<%# WebApp.WebAdminDB.ServerListWithAll.Select(x => new {Text=x.mName, Value=x.mNo}) %>" DataTextField="Text" DataValueField="Value" />
            <asp:Button runat="server" ID="ServerChoice" Text="<%# WebApp.DisplayString.Search %>" OnClick="ServerChoice_Click" />
            <table class="lineTable">
            <asp:Repeater runat="server" ID="StateRepeater">
                <HeaderTemplate>
                    <tr>
                        <th><%# WebApp.DisplayString.ServerName %></th>
                        <th><%# WebApp.DisplayString.GuildSizeSmall %></th>
                        <th><%# WebApp.DisplayString.GuildSizeMedium %></th>
                        <th><%# WebApp.DisplayString.GuildSizeLarge %></th>
                        <th><%# WebApp.DisplayString.GuildSizeXLarge %></th>
                        <th><%# WebApp.DisplayString.Modify %></th>
                    </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><%# Eval("ServerName") %> <asp:HiddenField runat="server" ID="ServerNo" Value='<%# Eval("ServerNo") %>' /></td>
                        <td><asp:Label runat="server" ID="Small" Text='<%# Eval("Small") %>' /> </td>
                        <td><asp:Label runat="server" ID="Medium" Text='<%# Eval("Medium") %>' /> </td>
                        <td><asp:Label runat="server" ID="Large" Text='<%# Eval("Large") %>' /> </td>
                        <td><asp:Label runat="server" ID="XLarge" Text='<%# Eval("XLarge") %>' /> </td>
                        <td><asp:Button runat="server" ID="ChangeInfo" Text="<%# WebApp.DisplayString.ChangeInfo %>" OnClick="ChangeInfo_Click" /></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
            </table>
    </form>
</asp:Content>
