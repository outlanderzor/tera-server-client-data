<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/AppCode/DataSetAskDialog.master"CodeBehind="ATO_DeleteVillagerDespawn.aspx.cs" Inherits="WebApp.Npc.ATO_DeleteVillagerDespawn" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
	<table class="lineTable width-full">
		<asp:Repeater runat="server" ID="DeleteVillagerList">
			<HeaderTemplate>
				<tr>
					<th><%# WebApp.DisplayString.ServerName %></th>
					<th><%# WebApp.DisplayString.HuntingZone %></th>
					<th><%# WebApp.DisplayString.NPC %></th>
				</tr>
			</HeaderTemplate>
			<ItemTemplate>
				<tr>
					<td><%# Eval("ServerName") %></td>
					<td><%# Eval("HuntingZoneIdStr") %></td>
					<td><%# Eval("TemplateIdStr") %></td>
				</tr>
			</ItemTemplate>
		</asp:Repeater>
	</table>
</asp:Content>
