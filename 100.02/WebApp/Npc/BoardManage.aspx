﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="BoardManage.aspx.cs" Inherits="WebApp.Npc.BoardManage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">
    <form id="Form1" runat="server" class="nolineTable">    
        <table class="nolineTable">
            <tr>
                <td> <%= WebApp.DisplayString.ServerChoice %> </td>
                <td> <asp:DropDownList runat="server" ID="ServNoDDL" DataTextField="Text" DataValueField="Value" /> </td>
                
                
            </tr>
            <tr>
                <td> <%= WebApp.DisplayString.BoardChoice%> </td>
                <td> <asp:DropDownList runat="server" ID="BoardDropDownList" DataTextField="Text" DataValueField="Value" /> </td>
            </tr>

            <tr>
                <td> &nbsp; <asp:Button runat="server" ID="SearchButton" OnClick="SearchButton_Click" /> </td>
            </tr>
        </table>

        <asp:Panel runat="server" ID="errorPanel" Visible="false">
                <td> &nbsp;</td>
        </asp:Panel>                

        <asp:Panel runat="server" ID="ResultPanel" Visible="false">
        </asp:Panel>    
    <%=BoardPaging%>
    </form>
</asp:Content>
