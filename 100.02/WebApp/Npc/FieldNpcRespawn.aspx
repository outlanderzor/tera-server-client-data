﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="FieldNpcRespawn.aspx.cs" Inherits="WebApp.Npc.FieldNpcRespawn" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form runat="server" id="resultForm" >

        <table class="outerlineTable">
        <tr>        
            <td> <%= WebApp.DisplayString.ServerChoice %> </td>
            <td> <asp:DropDownList runat="server" ID="ServerChoiceList" DataTextField="Text" DataValueField="Value" /> </td>
        </tr>
        <tr>
            <td> <%= WebApp.DisplayString.UserDBID %> </td>
            <td> <asp:TextBox runat="server" ID="UserDbIdText" /> </td>
        </tr>
        <tr>
            <td> <asp:Button runat="server" ID="SubmitButton" OnClick="SubmitButton_Click" /> </td>
        </tr>
        </table>
        
        <asp:Panel runat="server" ID="ResultPanel" Visible="false">
        </asp:Panel>
    </form>

</asp:Content>
