﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/AppCode/DataSetAskDialog.master" CodeBehind="ATO_AddVillagerDespawn.aspx.cs" Inherits="WebApp.Npc.ATO_AddVillagerDespawn" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
	<div class="ato-serverlist">
		<label><%# WebApp.DisplayString.CheckServer %></label>
		<ul>
			<asp:Repeater runat="server" ID="ServerCheckList">
				<ItemTemplate>
					<li><asp:CheckBox runat="server" ID="ServerName" Text='<%# Eval("ServerName") %>' /><asp:HiddenField runat="server" ID="ServerNo" Value='<%# Eval("ServerNo") %>' /></li>
				</ItemTemplate>
			</asp:Repeater>
		</ul>
		<label><%# WebApp.DisplayString.CheckAllServer %></label>
	</div>
	<div class="ato-main">
		<ul class="form-style">
            <li><label class="width1000"><%# WebApp.DisplayString.VillagerDespawnInfo %></label></li>
			<li><label class="width100"><%# WebApp.DisplayString.HuntingZone %></label><asp:DropDownList runat="server" ID="HuntingZoneList" AutoPostBack="true" OnSelectedIndexChanged="OnHuntingZoneIndexChanged" DataValueField="Key" DataTextField="Value"/></li>
			<li><label class="width100"><%# WebApp.DisplayString.NPC %></label><asp:DropDownList runat="server" ID="VillagerList" DataValueField="Key" DataTextField="Value"/></li>
             <li>
                <asp:Button runat="server" ID="AddItem" Text=<%# WebApp.DisplayString.InsertItem %> OnClick="OnAddItem" />
                <label class="width300"><%# WebApp.DisplayString.InputNpcSpawnEventNpc %></label>
            </li>
		</ul>
	</div>

    <table style="margin-bottom: 10px; border: none;">
        <asp:Repeater runat="server" ID="VillagerDespawnData">
            <HeaderTemplate>
                <tr>
                    <th><%# WebApp.DisplayString.HuntingZone %></th>
                    <th><%# WebApp.DisplayString.NPC %></th>
                    <th><%# WebApp.DisplayString.DeleteItem %></th>
                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><%# Eval("HuntingZoneStr") %></td>
                    <td><%# Eval("TemplateIdStr") %></td>
                    <td><asp:Button runat="server" CommandArgument='<%# Eval("HuntingZone") + " " + Eval("TemplateId") %>' OnCommand="OnDeleteItem" Text="<%# WebApp.DisplayString.DeleteItem %>"/></td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
    
</asp:Content>
