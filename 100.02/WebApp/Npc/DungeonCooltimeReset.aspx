﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="DungeonCooltimeReset.aspx.cs" Inherits="WebApp.Npc.DungeonCooltimeReset" %>
<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form id="form" runat="server">

        <table class="outerlinePaddingTable">
            <tr>
                <th> <%= WebApp.DisplayString.ServerChoice %> </td>
                <td> <asp:DropDownList runat="server" ID="ServNoDropDownList" DataTextField="Text" DataValueField="No" /> </td>
                <td style="text-align:right"> <asp:Button runat="server" ID="SearchButton" OnClick="SearchButton_Click" /></td>
            </tr>
       </table>
       <table class="outerlinePaddingTable">
            <tr>
                <th colspan="2"> [<%= WebApp.DisplayString.ServerControl %>]</th>
            </tr>
            <tr>
                <th> <%= WebApp.DisplayString.Dungeon %> </th>
                <td> <asp:DropDownList runat="server" ID="ServerControlDungeonDropDownList" DataTextField="Text" DataValueField="Id" /> </td>
            </tr>
            <tr>
                <td colspan="2"> <asp:Button runat="server" ID="ServerControlButton" OnClick="ServerControlButton_Click" /> </td>
            </tr>            
        </table>
        <table class="outerlinePaddingTable">
            <tr>
                <th colspan="2"> [<%= WebApp.DisplayString.UserControl %>]</th>
            </tr>
            <tr>
                <th> <%= WebApp.DisplayString.UserDBID %> </th>
                <td> <asp:TextBox runat="server" ID="UserDbIdTextBox" /> </td>
            </tr>
            <tr>
                <th> <%= WebApp.DisplayString.Dungeon %> </th>
                <td> <asp:DropDownList runat="server" ID="UserControlDungeonDropDownList" DataTextField="Text" DataValueField="Id" /> </td>
            </tr>
            <tr>
                <td colspan="2"> <asp:Button runat="server" ID="UserControlButton" OnClick="UserControlButton_Click" /> </td>
            </tr>            
        </table>

    </form>
    
</asp:Content>
