﻿<%@ Page MasterPageFile="~/AppCode/CustomAskDialog.Master" Language="C#" AutoEventWireup="true" CodeBehind="ATO_EditVipToken.aspx.cs" Inherits="WebApp.Users.ATO_EditVipToken" %>

<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentBody" runat="server">
    <form id="Form1" runat="server" class="nolineTable">
        <!-- 캐릭터 공통 정보 -->
        <table class="lineTable">
            <tr><td>
                <b>[<%= WebApp.DisplayString.UserInfo %>]</b> <asp:Label ID="UserInfo" runat="server" Visible="true"/>
            </td></tr>
        </table><br />
        
       <!-- 잘못 입력 된 값에 대한 안내 메세지  -->
       <p style="color:red; font-weight:bold"><asp:Label ID="ErrorInfo" runat="server" visible="false"/></p><br />

       <asp:Panel id="EditPanel" runat="server" visible="true">
           <!-- 토큰 보유량 입력란 -->
            <%= WebApp.DisplayString.VipTokenAmount %> &nbsp;<asp:TextBox runat="server"  ID="TokenAmountBox" /><br /><br />

            <!-- 메모 입력란 -->
            <asp:TextBox runat="server" width="250px" height="100px" ID="MemoBox" TextMode="MultiLine" style="overflow:auto"/><br /><br />    

            <!-- 확인 버튼 -->
            <asp:Button runat="server" ID="ConfirmButton" text="VIP 토큰 보유량 변경" OnClick="ConfirmButton_Click" />
       </asp:Panel>

        <asp:Button runat="server" ID="CloseButton" text="닫기" OnClick="CloseButton_Click" Visible="false"/><br />
    </form>
</asp:Content>